#ifndef __sdc_local_h__
#define __sdc_local_h__ "sdc_local.h"

#include "ff.h"

extern FATFS SDC_FS;
extern SDCDriver SDC_DEV;

void sdcSetup(void);
void sdcUpdate(void);
void sdcMode(bool_t s);

const char *f_errstr(FRESULT r);

#define sdcShellCommands()    \
  shellCommandExtern(mount);  \
  shellCommandExtern(umount); \
  shellCommandExtern(pwd);    \
  shellCommandExtern(ls);     \
  shellCommandExtern(cd);     \
  shellCommandExtern(cat);    \
  shellCommandExtern(source)

#define sdcShellCommandsRef() \
  shellCommandRef(mount)      \
  shellCommandRef(umount)     \
  shellCommandRef(pwd)        \
  shellCommandRef(ls)         \
  shellCommandRef(cd)         \
  shellCommandRef(cat)        \
  shellCommandRef(source)

#endif//__sdc_local_h__

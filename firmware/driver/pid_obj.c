#include "api.h"

#define PID_ALG 2

ObjectDef(PID,
          /* parameters */
          int p; /**< proportional factor */
          int i; /**< integral factor */
          int d; /**< derivative factor */
          int s; /**< scale factor */
          
#if PID_ALG == 1
          int e[3];  /**< errors */
          int c;     /**< control signal */
#endif

#if PID_ALG == 2
          int e;     /**< last error */
          int e_acc; /**< accumulated integral error */
          int e_lim; /**< accumulated integral error limit */
          
          int v;     /**< last value */
#endif
          
          char tuned_st;
          int e_thr; /**< error threshold */
          EventSource tuned;
          EventSource untuned;
          
          int8_t n;  /**< steps */
          
          /* private */
          systime_t dtime;
          systime_t ticks;
          
          /* i/o */
          int t;             /**< target value */
          ObjectLink input;  /**< feedback input */
          ObjectLink output; /**< control output */
          unsigned delay);   /**< polling delay */

static bool_t PIDActive(PID *obj){
  return obj->ticks > 0 && /* in active */
    obj->input.obj && obj->input.par > -1 && /* has input source */
    obj->output.obj && obj->output.par > -1; /* has output sink */
}

#if 0
static inline void PIDUpdateK(PID *obj){
  obj->_i = obj->p * obj->i * obj->delay;
  if(obj->delay > 0){
    obj->_d = obj->p * obj->d / obj->delay;
  }
}
#endif

static inline void PIDStep(PID *obj){
  if(obj->n < 2){
    obj->n ++;
  }
}

static void PIDHandler(PID *obj){
  int v;
  
  if(objectParamGet(obj->input.obj, obj->input.par, &v) != paramSuccess){
    return;
  }
  
#if PID_ALG == 1
  int *e = obj->e;

  e[2] = e[1];
  e[1] = e[0];
  e[0] = obj->t - v;
  
  if(obj->n < 2){
    if(obj->n < 1){
      e[1] = e[0];
    }
    e[2] = e[1];
    obj->n++;
  }
  
  obj->c += ((e[0] - e[1]) * obj->p + e[0] * obj->i + (e[0] - (e[1] << 1) + e[2]) * obj->d) / obj->s;
#endif
  
#if PID_ALG == 2
  obj->e = obj->t - v;
  
  obj->e_acc += obj->e;
  
  if(obj->e_acc > obj->e_lim){
    obj->e_acc = obj->e_lim;
  }else if(obj->e_acc < -obj->e_lim){
    obj->e_acc = -obj->e_lim;
  }
  
  int d = obj->n > 0 ? obj->v - v : 0;
  
  obj->v = v;
  
  int s = (obj->e * obj->p + obj->e_acc * obj->i + d * obj->d) / obj->s;
  
  //tty_printf("e:%9d e_acc:%9d d:%9d s:%9d\r\n", obj->e, obj->e_acc, d, s);
  
  PIDStep(obj);
#endif
  
  if(s < 0){ /* >= 0.00 % */
    s = 0;
  }
  
  if(s > 10000){ /* <= 100.0 % */
    s = 10000;
  }
  
  objectParamSet(obj->output.obj, obj->output.par, &s);
  
  char t = obj->e < obj->e_thr;
  
  if(t != obj->tuned_st){
    obj->tuned_st = t;
    if(obj->tuned_st){
      //chSysLock();
      chEvtBroadcast(&obj->tuned);
      //chSysUnlock();
    }else{
      //chSysLock();
      chEvtBroadcast(&obj->untuned);
      //chSysUnlock();
    }
  }
}

static PID *PIDNext(PID *obj);

static WORKING_AREA(pidWorkArea, 256);
static Thread *pidPollThread = NULL;

static msg_t PIDPollThread(void *arg){
  (void)arg;
  
  chRegSetThreadName("pid-poll");

  systime_t stime = 0;
  
  for(; !chThdShouldTerminate(); ){
    systime_t dtime = 0;
    PID *obj = NULL;
    int n = 0;
    
    for(; (obj = PIDNext(obj)); n++){
      obj->dtime -= stime;
      
      if(obj->dtime == 0){
        PIDHandler(obj);
        obj->dtime = obj->ticks;
      }
      
      if(dtime == 0 || obj->dtime < dtime){
        dtime = obj->dtime;
      }
    }
    
    if(n){
      chThdSleep(dtime);
      stime = dtime;
    }else{
      break;
    }
  }
  
  pidPollThread = NULL;

  return 0;
}

static inline void PIDUpdate(PID *obj){
  obj->n = 0;
  
  obj->e = 0;
  obj->e_acc = 0;
  
  obj->ticks = MS2ST(obj->delay);
  obj->dtime = obj->ticks;
  
  if(PIDActive(obj)){
    if(!pidPollThread){ /* initialize control thread */
      pidPollThread = chThdCreateStatic(pidWorkArea, sizeof(pidWorkArea), NORMALPRIO, PIDPollThread, NULL);
    }
  }else{
    if(obj->output.obj && obj->output.par > -1){
      /* reset output to 0 */
      int s = 0;
      objectParamSet(obj->output.obj, obj->output.par, &s);
    }
  }
}

static PID *PIDCreate(void){
  PID *obj = objectNew(PID);
  
  obj->delay = 250; /* 250 mS */
  
  obj->input = nullLink;
  obj->output = nullLink;
  
  obj->p = 8192;
  obj->i = 512;
  obj->d = 24576;
  obj->s = 1024;
  
  obj->e_lim = 384;
  
  obj->e_thr = 200;
  
  chEvtInit(&obj->tuned);
  chEvtInit(&obj->untuned);
  
  PIDUpdate(obj);
  
  return obj;
}

static void PIDDelete(PID *obj){
  objectDie(obj);
}

ObjectGetValue(PID, Delay, Uint, delay);

static int PIDDelaySet(PID *obj, unsigned *val){
  if(obj->delay != *val){
    obj->delay = *val;
    
    PIDUpdate(obj);
  }
  
  return paramSuccess;
}

ObjectGetSetValue(PID, Prop,  Uint, p);
ObjectGetSetValue(PID, Int,   Uint, i);
ObjectGetSetValue(PID, Der,   Uint, d);
ObjectGetSetValue(PID, Scale, Uint, s);

ObjectGetSetValue(PID, Err,    Sint, e);
ObjectGetSetValue(PID, ErrAcc, Sint, e_acc);
ObjectGetSetValue(PID, ErrLim, Uint, e_lim);

ObjectGetSetValue(PID, ErrThr, Uint, e_thr);
ObjectGetSetValue(PID, Target, Sint, t);

static int PIDActualGet(const PID *obj, unsigned *val){
  *val = obj->t - obj->e;
  
  return paramSuccess;
}

ObjectGetValue(PID, Input, Link, input);

static int PIDInputSet(PID *obj, ObjectLink *val){
  obj->input = *val;
  
  PIDUpdate(obj);
  
  return paramSuccess;
}

ObjectGetValue(PID, Output, Link, output);

static int PIDOutputSet(PID *obj, ObjectLink *val){
  obj->output = *val;
  
  PIDUpdate(obj);
  
  return paramSuccess;
}

ObjectTypeDef(PID, "driver:pid",
              "Proportional Integral Derivative (PID) regulator",
              ObjectParamDefRW(PIDProp,   Uint, "p", "Proportional factor"),
              ObjectParamDefRW(PIDInt,    Uint, "i", "Integral factor"),
              ObjectParamDefRW(PIDDer,    Uint, "d", "Derivative factor"),
              ObjectParamDefRW(PIDScale,  Uint, "s", "Scale factor"),
              
              ObjectParamDefRW(PIDErr,    Sint, "e", "Current error"),
              ObjectParamDefRW(PIDErrAcc, Sint, "e:acc", "Accumulated error"),
              ObjectParamDefRW(PIDErrLim, Uint, "e:lim", "Accumulated error limit"),
              
              ObjectParamDefRW(PIDErrThr, Uint, "e:thr", "Maximal acceptable error"),
              //ObjectParamDefRW(PIDOkTrig,  Evnt, "good", "Trigger"),
              //ObjectParamDefRW(PIDOkTrig,  Evnt, "bad", "Trigger"),
              
              ObjectParamDefRW(PIDDelay,  Uint, "delay", "Control delay (in milliseconds, 0-off state)"),
              ObjectParamDefRW(PIDInput,  Link, "in", "Target feedback input"),
              ObjectParamDefRW(PIDOutput, Link, "out", "Target control output"),
              ObjectParamDefRO(PIDActual, Sint, "actual", "Actual value"),
              ObjectParamDefRW(PIDTarget, Sint, "target", "Target value"));

/* find next active PID driver */
static PID *PIDNext(PID *obj){
  for(; (obj = (PID*)objectNext((BaseObject*)obj)); ){
    if(objectType((BaseObject*)obj) == &PIDVMT && /* required type */
       PIDActive(obj)){ /* is active */
      return obj;
    }
  }
  return NULL;
}

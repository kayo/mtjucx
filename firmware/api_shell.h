#ifndef __api_shell_h__
#define __api_shell_h__ "api_shell.h" 

#define apiShellCommands()     \
  shellCommandExtern(list);    \
  shellCommandExtern(add);     \
  shellCommandExtern(del);     \
  shellCommandExtern(set);     \
  shellCommandExtern(get);
  
#define apiShellCommandsRef() \
  shellCommandRef(list)       \
  shellCommandRef(add)        \
  shellCommandRef(del)        \
  shellCommandRef(set)        \
  shellCommandRef(get)

#endif//__api_shell_h__

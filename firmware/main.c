/*
    ChibiOS/RT - Copyright (C) 2006-2013 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

//#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ch.h"
#include "hal.h"

#include "shell.h"
#include "shell_local.h"
#include "chprintf.h"

#include "api.h"
#include "api_shell.h"

#include "local.h"

/*===========================================================================*/
/* Command line related.                                                     */
/*===========================================================================*/

shellStandardCommands();
shellAdvancedCommands();

sdcShellCommands();
apiShellCommands();

shellCommandExtern(bench);

#define APP(name) shellCommandExtern(name)
APPS
#undef APP

static const ShellCommand *shell_cmd[] = {
  shellStandardCommandsRef()
  shellAdvancedCommandsRef()
  
  sdcShellCommandsRef()
  apiShellCommandsRef()

  shellCommandRef(bench)
  
#define APP(name) shellCommandRef(name)
  APPS
#undef APP

  NULL
};

static const ShellConfig shell_cfg = {
  "supervisor",
  "MTJUCF Shell (" API_VERSION ")",
  "mtjucx> ",
  shell_cmd,
  (BaseSequentialStream*)&SDU1
};

static ShellObject shell;

static WORKING_AREA(waBlinker, 512);
static msg_t Blinker(void *arg) {
  (void)arg;
  chRegSetThreadName("blinker");

  tty_init();
  
  for(;;){
    systime_t time = usbActive() ? 100 : 500;
    ledSet(LED_1, 1);
    chThdSleepMilliseconds(time);
    ledSet(LED_1, 0);
    chThdSleepMilliseconds(time);
    
    tty_swap();
    sdcUpdate();
  }
  return 0;
}

static WORKING_AREA(waShell, 2048+1024);

ObjectTypeExt(Scale);
ObjectTypeExt(ThermoCouple);
ObjectTypeExt(Thermistor);
ObjectTypeExt(PID);

ObjectTypes(
            spiTypesRef()
            pwmTypesRef()
            adcTypesRef()
            stpTypesRef()
            gptTypesRef()
            ctlTypesRef()
            
            ObjectTypeRef(Scale)
            ObjectTypeRef(ThermoCouple)
            ObjectTypeRef(Thermistor)
            ObjectTypeRef(PID)
            );

/*
 * Application entry point.
 */
int main(void) {
  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();
  
  usbSetup();
  sdcSetup();
  
  spiObjectsAdd();
  pwmObjectsAdd();
  adcObjectsAdd();
  ctlObjectsAdd();
  stpObjectsAdd();
  gptObjectsAdd();
  
  chThdCreateStatic(waBlinker, sizeof(waBlinker), NORMALPRIO, Blinker, NULL);
  
  /*
   * Shell manager initialization.
   */
  shellInit(&shell, &shell_cfg);
  
  /*
   * Normal main() thread activity, in this demo it does nothing except
   * sleeping in a loop and check the button state.
   */
  Thread *shelltp = NULL;
  
  while (TRUE) {
    if (!shelltp && (SDU1.config->usbp->state == USB_ACTIVE))
      shelltp = shellCreateStatic(&shell, waShell, sizeof(waShell), NORMALPRIO);
    else if (chThdTerminated(shelltp)) {
      chThdRelease(shelltp);    /* Recovers memory of the previous shell.   */
      shelltp = NULL;           /* Triggers spawning of a new shell.        */
    }
    chThdSleepMilliseconds(1000);
  }
}

#ifndef __ringbuf_h__
#define __ringbuf_h__ "ringbuf.h"

#define RINGBUF_IMPLEMENTATION 1

#define ringbuf_lenof(array) (sizeof(array)/sizeof(array[0]))
#define ringbuf_size(self) ringbuf_lenof((self)->data)

#if RINGBUF_IMPLEMENTATION == 1

#define ringbuf(_type_, _size_) struct { \
    _type_ data[_size_];                 \
    _type_ edge[0];                      \
    _type_* rptr;                        \
    _type_* wptr;                        \
    int size;                            \
  }

#define ringbuf_static() { {}, {}, NULL, NULL, 0 }

#define ringbuf_init(self) ({                         \
      (self)->wptr = (self)->rptr = (self)->data;     \
      (self)->size = 0;                               \
    })

#define ringbuf_used(self) ((self)->size)
#define ringbuf_free(self) (ringbuf_size(self) - ringbuf_used(self))

#define ringbuf_que(self)                                 \
  if(++(self)->wptr == (self)->edge){ /* edge detected */ \
    (self)->wptr = (self)->data;      /* restart */       \
  }

#define ringbuf_put(self, _data_, _size_) ({                    \
      int size = ringbuf_free(self);                            \
      int dpos = 0;                                             \
      if(size > 0){ /* we have a free space */                  \
        if((_size_) < size) size = (_size_);                    \
        for(; size; (self)->size++, size--){                    \
          *(self)->wptr = (_data_)[dpos++];                     \
          ringbuf_que(self);                                    \
        }                                                       \
      }                                                         \
      dpos;                                                     \
    })

#define ringbuf_deq(self)                                 \
  if(++(self)->rptr == (self)->edge){ /* edge detected */ \
    (self)->rptr = (self)->data;      /* restart */       \
  }

#define ringbuf_get(self, _data_, _size_) ({                    \
      int size = ringbuf_used(self);                            \
      int dpos = 0;                                             \
      if(size > 0){ /* we have a data */                        \
        if((_size_) < size) size = (_size_);                    \
        for(; size; (self)->size--, size--){                    \
          (_data_)[dpos++] = *(self)->rptr;                     \
          ringbuf_deq(self);                                    \
        }                                                       \
      }                                                         \
      dpos;                                                     \
    })

#define ringbuf_end(self) (ringbuf_free(self) > 0 ? (self)->wptr : NULL)
#define ringbuf_beg(self) (ringbuf_used(self) > 0 ? (self)->rptr : NULL)

#define ringbuf_prev(self) (ringbuf_used(self) > 1 ? ((self)->wptr - 1 < (self)->data ? (self)->edge - 1 : (self)->wptr - 1) : NULL)
#define ringbuf_next(self) (ringbuf_used(self) > 1 ? ((self)->rptr + 1 >= (self)->edge ? (self)->data : (self)->rptr + 1) : NULL)

#endif

#if RINGBUF_IMPLEMENTATION == 2

#define ringbuf(_type_, _size_) struct { \
    int size;                            \
    _type_* rptr;                        \
    _type_* wptr;                        \
    _type_* edge;                        \
    _type_ data[_size_];                 \
  }

typedef struct {
  int size;                              \
  _type_* rptr;                          \
  _type_* wptr;                          \
  _type_* edge;                          \
  _type_* data;                          \
} ringbuf_t;

#define ringbuf_init(self) ({                           \
      (self)->wptr = (self)->rptr = (self)->data;       \
      (self)->edge = (self)->data + ringbuf_size(self); \
      (self)->size = 0;                                 \
    })

#endif

#endif//__ringbuf_h__

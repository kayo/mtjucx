#ifndef __usb_msc_h__
#define __usb_msc_h__ "usb_msc.h"

#include "scsi_def.h"

#define MSU_REQ_RESET                0xFF
#define MSU_GET_MAX_LUN              0xFE
#define MSU_CBW_SIGNATURE            0x43425355
#define MSU_CSW_SIGNATURE            0x53425355
#define MSU_COMMAND_DIR_DATA_OUT     (0 << 7)
#define MSU_COMMAND_DIR_DATA_IN      (1 << 7)

#define MSU_SETUP_WORD(setup, index) (uint16_t)(((uint16_t)setup[index+1] << 8) | (setup[index] & 0x00FF))

#define MSU_SETUP_VALUE(setup)   MSU_SETUP_WORD(setup, 2)
#define MSU_SETUP_INDEX(setup)   MSU_SETUP_WORD(setup, 4)
#define MSU_SETUP_LENGTH(setup)  MSU_SETUP_WORD(setup, 6)

#define MSU_COMMAND_PASSED 0x00
#define MSU_COMMAND_FAILED 0x01
#define MSU_COMMAND_PHASE_ERROR 0x02

/**
 * @brief Command Block Wrapper
 */
PACK_STRUCT_BEGIN typedef struct {
  uint32_t signature;
  uint32_t tag;
  uint32_t data_len;
  uint8_t flags;
  uint8_t lun;
  uint8_t cmdLength;
  union {
    SCSIRequest command;
    SCSIInquiryRequest inquiry;
    SCSIReadWrite10Request rw10;
    SCSIStartStopUnitRequest ssu;
    SCSISendDiagnosticRequest diag;
  };
} PACK_STRUCT_STRUCT msu_cbw_t PACK_STRUCT_END;

/**
 * @brief Command Status Wrapper
 */
PACK_STRUCT_BEGIN typedef struct {
  uint32_t signature;
  uint32_t tag;
  uint32_t data_residue;
  uint8_t status;
} PACK_STRUCT_STRUCT msu_csw_t PACK_STRUCT_END;

typedef struct MassStorageUSBConfig MassStorageUSBConfig;
typedef struct MassStorageUSBDriver MassStorageUSBDriver;

typedef enum {
  msuIdle,
  msuReadCmd,
  //msuEjected
} msu_state_t;

struct MassStorageUSBConfig {
  /**
   * @brief   USB driver to use.
   */
  USBDriver *usbp;
  /**
   * @brief   Bulk IN endpoint used for outgoing data transfer.
   */
  usbep_t bulk_in;
  /**
   * @brief   Bulk OUT endpoint used for incoming data transfer.
   */
  usbep_t bulk_out;
  /**
   * @brief   BlockDevice driver to use.
   */
  BaseBlockDevice *bbdp;
  /**
   * @brief   ReadWrite buffer to use.
   */
  uint8_t *rw_buf[2];
  /**
   * @brief   SCSI Inquiry Response (use MCU_INQUIRY macro).
   */
  SCSIInquiryResponse inquiry;
  /**
   * @brief   Thread workarea pointer to use.
   */
  void *twsp;
  /**
   * @brief   Size of thread workarea.
   */
  size_t twas;
  /**
   * @brief   Thread priority.
   */
  size_t tprio;
  /**
   * @brief   Thread name.
   */
  const char *tname;
};

#define MSU_INQUIRY(vendor, product, revision) {{ \
      0x00, /* direct access block device */      \
      0x80, /* removable device */                \
      0x04, /* SPC-2 compilant */                 \
      0x02, /* response data format */            \
      0x20, /* response has 0x20 + 4 bytes */     \
      0x00,                                       \
      0x00,                                       \
      0x00,                                       \
      vendor,                                     \
      product,                                    \
      revision                                    \
    }}

struct MassStorageUSBDriver {
  const MassStorageUSBConfig *config;
  BinarySemaphore sem;
  
  Thread *thread;
  
  EventSource connected; /**< Drive connected to USB */
  EventSource ejected;   /**< Block Device ejected from Drive */
  EventSource loaded;    /**< Block Device loaded to Drive */
  
  msu_cbw_t cbw;
  msu_csw_t csw;
  
  BlockDeviceInfo bdi;
  SCSIReadCapacity10Response capacity;
  SCSISenseResponse sense;
  SCSIModeSense6Response mode_sense;
};

#define MSU_CONNECTED 0
#define MSU_EJECTED   1

#ifdef __cplusplus
extern "C" {
#endif
  bool_t msuRequestsHook(USBDriver *usbp);
  void msuDataTransfered(USBDriver *usbp, usbep_t ep);
#define msuDataTransmitted msuDataTransfered
#define msuDataReceived msuDataTransfered
  void msuConfigureHookI(MassStorageUSBDriver *msup);
  
  void msuObjectInit(MassStorageUSBDriver *msup);
  void msuStart(MassStorageUSBDriver *msup, const MassStorageUSBConfig *cfgp);
  void msuStop(MassStorageUSBDriver *msup);

  bool_t msuDeviceLoad(MassStorageUSBDriver *msup);
#define  msuDeviceLoaded(msup) ((msup)->bdi.blk_size > 0 && (msup)->bdi.blk_num > 0)
#ifdef __cplusplus
}
#endif

#endif//__usb_msc_h__

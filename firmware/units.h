#ifndef __units_h__
#define __units_h__ "units.h"

#include "ch.h"
#include "decimal.h"

typedef enum {
  UM = 0,
  MM,
  IN,
  UnitsCount,
} Units;

decimal unitsConv(decimal d, Units f, Units t);

#endif//__units_h__

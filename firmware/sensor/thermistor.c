#include "api.h"

#include <math.h>

typedef float real_t;

#ifndef THERMISTOR_WITH_BENCH
#define THERMISTOR_WITH_BENCH TRUE
#endif

#ifndef THERMISTOR_USE_LOOKUP_TABLE
#define THERMISTOR_USE_LOOKUP_TABLE FALSE
#endif

#define celsius2kelvins(x) ((x) + 27315)
#define kelvins2celsius(x) ((x) - 27315)

#if THERMISTOR_USE_LOOKUP_TABLE == TRUE
typedef struct {
  uint16_t i;
  int16_t o;
} tabent_t;
#endif

ObjectDef(Thermistor,
          /* parameter fields */
          signed t0;      /**< temperature at stated resistance (e.g. 2500 for 25°C) */
          uint16_t r0;    /**< stated resistance (e.g. 10000 for 10K Ohm) */
          uint16_t beta;  /**< stated beta (e.g. 3947, 3977) */
          uint16_t r1;    /**< parallel with thermistor resistance (e.g. 1600 for 1.6K Ohm) 0 means is missing */
          uint16_t r2;    /**< series with thermistor resistance (e.g. 680 for 680 Ohm) */
          
          /* private fields */
          real_t k;       /**< K-coeff */
          real_t vs;      /**< effective bias relative voltage (value / top) */
          uint16_t rs;    /**< effective bias impedance */

#if THERMISTOR_WITH_BENCH == TRUE
          TimeMeasurement conv_bench;
#endif
          
#if THERMISTOR_USE_LOOKUP_TABLE == TRUE
          tabent_t *tab;  /**< lookup table */
          uint16_t len;   /**< lookup table length */
#endif

          /* I/O fields */
          unsigned aiv;   /**< analog input value */
          uint16_t top;   /**< maximum input value */
          ObjectLink src); /**< source for value */

static void ThermistorUpdateK(Thermistor *obj){
  obj->k = exp(((real_t)-(((int32_t)obj->beta) * 100)) / celsius2kelvins(obj->t0)) * obj->r0;
}

static void ThermistorUpdateS(Thermistor *obj){
  if(obj->r1 > 0){
    unsigned r = obj->r1 + obj->r2;
    obj->vs = ((real_t)obj->r1) / r;
    obj->rs = obj->r1 * obj->r2 / r;
  }else{
    obj->vs = 1.0;
    obj->rs = obj->r2;
  }
}

static Thermistor *ThermistorCreate(void){
  Thermistor *obj = objectNew(Thermistor);
  
  obj->t0 = 2500;
  obj->r0 = 10000;
  obj->beta = 2500;

  ThermistorUpdateK(obj);
  
  obj->r1 = 0;
  obj->r2 = 4700;
  
  ThermistorUpdateS(obj);
  
  obj->top = (1 << 12) - 1; /* 12-bit ADC by default */
  obj->aiv = 0;
  
  obj->src = nullLink;

#if THERMISTOR_WITH_BENCH == TRUE
  tmObjectInit(&obj->conv_bench);
#endif
  
  return obj;
}

static void ThermistorDelete(Thermistor *obj){
  objectDie(obj);
}

/* convert ADC reading into a temperature in 0.01 Celcius */
static int16_t ThermistorConvert(Thermistor *obj, unsigned adc){
  
#if THERMISTOR_WITH_BENCH == TRUE
  tmStartMeasurement(&obj->conv_bench);
#endif
  
  real_t v = ((real_t)adc) / obj->top;      /* convert the ADC value to a relative voltage */
  real_t r = v * obj->rs / (obj->vs - v); /* resistance of thermistor */
  
  int16_t o = kelvins2celsius(obj->beta * 100 / log(r / obj->k));
  
#if THERMISTOR_WITH_BENCH == TRUE
  tmStopMeasurement(&obj->conv_bench);
#endif
  
  return o;
}

static int ThermistorValueGet(Thermistor *obj, signed *val){
  if(obj->src.obj && obj->src.par != -1){
    if(objectParamGet(obj->src.obj, obj->src.par, &obj->aiv) != paramSuccess){
      return paramConfigError;
    }
  }
  
  *val = ThermistorConvert(obj, obj->aiv);
  
  return paramSuccess;
}

ObjectGetValue(Thermistor, T0,    Sint, t0);

static int ThermistorT0Set(Thermistor *obj, const signed *val){
  obj->t0 = *val;
  ThermistorUpdateK(obj);
  return paramSuccess;
}

ObjectGetValue(Thermistor, R0,    Uint, r0);

static int ThermistorR0Set(Thermistor *obj, const unsigned *val){
  obj->r0 = *val;
  ThermistorUpdateK(obj);
  return paramSuccess;
}

ObjectGetValue(Thermistor, Beta,  Uint, beta);

static int ThermistorBetaSet(Thermistor *obj, const unsigned *val){
  obj->beta = *val;
  ThermistorUpdateK(obj);
  return paramSuccess;
}

ObjectGetValue(Thermistor, R1,    Uint, r1);

static int ThermistorR1Set(Thermistor *obj, const unsigned *val){
  obj->r1 = *val;
  ThermistorUpdateS(obj);
  return paramSuccess;
}

ObjectGetValue(Thermistor, R2,    Uint, r2);

static int ThermistorR2Set(Thermistor *obj, const unsigned *val){
  obj->r2 = *val;
  ThermistorUpdateS(obj);
  return paramSuccess;
}

#if THERMISTOR_WITH_BENCH == TRUE
ObjectGetValue(Thermistor, CSLast,  Uint, conv_bench.last);
ObjectGetValue(Thermistor, CSWorst, Uint, conv_bench.worst);
ObjectGetValue(Thermistor, CSBest,  Uint, conv_bench.best);
#endif

ObjectGetSetValue(Thermistor, Src,   Link, src);
ObjectGetSetValue(Thermistor, Top,   Uint, top);
ObjectGetSetValue(Thermistor, Input, Uint, aiv);

ObjectTypeDef(Thermistor, "sensor:thermistor", "Thermistor value convertor",
              ObjectParamDefRW(ThermistorT0,    Sint, "t0",    "Temperature at stated resistance (e.g. 2500 for 25*C)"),
              ObjectParamDefRW(ThermistorR0,    Uint, "r0",    "Stated resistance (e.g. 10000 for 10K Ohm)"),
              ObjectParamDefRW(ThermistorBeta,  Uint, "beta",  "Stated beta (e.g. 3947, 3977)"),
              ObjectParamDefRW(ThermistorR1,    Uint, "r1",    "Stated resistance (e.g. 10000 for 10K Ohm, 0 if not present)"),
              ObjectParamDefRW(ThermistorR2,    Uint, "r2",    "Series with thermistor resistance (e.g. 680 for 680 Ohm)"),
#if THERMISTOR_WITH_BENCH == TRUE
              ObjectParamDefRO(ThermistorCSLast,  Uint, "cs:last",  "Conversion stats: last time (cycles)"),
              ObjectParamDefRO(ThermistorCSWorst, Uint, "cs:worst", "Conversion stats: worst time (cycles)"),
              ObjectParamDefRO(ThermistorCSBest,  Uint, "cs:best",  "Conversion stats: best time (cycles)"),
#endif
              ObjectParamDefRW(ThermistorSrc,   Link, "src",   "ADC Input source"),
              ObjectParamDefRW(ThermistorTop,   Uint, "top",   "Maximum input value"),
              ObjectParamDefRW(ThermistorInput, Uint, "input", "Analog input for testing purpose"),
              ObjectParamDefRO(ThermistorValue, Sint, "value", "Output themperature value in 0.01*C"));

#include <string.h>

#include "api.h"
#include "device/spi_obj.h"

#define THERMOCOUPLE_USE_POLL_THREAD TRUE

ObjectDef(ThermoCouple,
          ObjectLink spi;
#if THERMOCOUPLE_USE_POLL_THREAD == TRUE
          int error;
          int value;
#endif
);

/* convert raw data to actual temperature */
static int tcRequest(ThermoCouple *obj, int* val){
  if(!obj->spi.obj){
    return paramConfigError;
  }
  
  SPIDriver *spi = spiDriver(obj->spi);
  SPIConfig *cfg = spiConfig(obj->spi);
  
  uint8_t data[2] = {0, 0};
  
  spiAcquireBus(spi);
  spiStart(spi, cfg);
  spiSelect(spi);
  
  chThdSleepMicroseconds(1);
  spiReceive(spi, sizeof(data), &data);
  
  spiUnselect(spi);
  spiReleaseBus(spi);

  uint16_t temp = (data[0] << 8) | data[1];
  
  if((temp & 0x8002) != 0){
    /* device isn't present */
    return paramDeviceError;
  }
  
  if(temp & 4){
    /* thermocouple open */
    return paramDeviceError;
  }
  
  temp >>= 3;
  
  *val = (temp >> 2) * 100 + (temp & 0b11) * 25;

  return paramSuccess;
}

#if THERMOCOUPLE_USE_POLL_THREAD == TRUE

static const unsigned tcPollDelay = 250; /* milliseconds (according to max conversion time 0.22s) */

static uint8_t tcCount = 0;
static ThermoCouple *tcActive = NULL;
static systime_t tcDelay = 0;

static ThermoCouple *tcNext(ThermoCouple *obj);

static msg_t tcPollThread(void *arg){
  (void)arg;
  
  chRegSetThreadName("thermocouple-poll");
  systime_t time = chTimeNow();
  
  for(; tcCount; ){
    chSysLock();
    if(!tcActive){
      tcActive = tcNext(NULL);
    }
    chSysUnlock();
    
    tcActive->error = tcRequest(tcActive, &tcActive->value);
    
    chSysLock();
    tcActive = tcNext(tcActive);
    chSysUnlock();
    
    time += tcDelay;
    chThdSleepUntil(time);
  }

  return 0;
}

static WORKING_AREA(tcWorkArea, 128);

/* add sensor to polling queue */
static void tcAdd(ThermoCouple *obj){
  tcCount ++;
  tcDelay = MS2ST(tcPollDelay / tcCount);
  
  if(tcCount == 1){
    tcActive = obj;
    chThdCreateStatic(tcWorkArea, sizeof(tcWorkArea), NORMALPRIO, tcPollThread, NULL);
  }
}

/* delete sensor from polling queue */
static void tcDel(ThermoCouple *obj){
  tcCount --;
  tcDelay = MS2ST(tcPollDelay / tcCount);
  
  chSysLock();
  if(tcActive == obj){
    tcActive = NULL;
  }
  chSysUnlock();
}

#endif

static ThermoCouple *ThermoCoupleCreate(void){
  ThermoCouple *obj = objectNew(ThermoCouple);
  
  obj->spi = nullLink;
  
#if THERMOCOUPLE_USE_POLL_THREAD == TRUE
  obj->error = paramDeviceError;
#endif
  
  return obj;
}

static void ThermoCoupleDelete(ThermoCouple *obj){
#if THERMOCOUPLE_USE_POLL_THREAD == TRUE
  if(obj->spi.obj){
    tcDel(obj);
  }
#endif
  objectDie(obj);
}

static int ThermoCoupleBindSet(ThermoCouple *obj, const ObjectLink* val){
  if(val->obj){
    if(strncmp(objectType(val->obj)->name, spiPrefix, sizeof(spiPrefix)-1) != 0){ /* Object type is not SPI interface */
      return paramTypeError;
    }
    const BaseObjectPDT *pdt = objectParam(val->obj, val->par);
    if(!pdt || (pdt->name[0] < '1' || pdt->name[0] > '9')){ /* Chip select parameter is not specified */
      return paramValueError;
    }
  }
  
  obj->spi = *val;
  
#if THERMOCOUPLE_USE_POLL_THREAD == TRUE
  if(obj->spi.obj){
    tcAdd(obj);
  }else{
    tcDel(obj);
  }
#endif
  
  return paramSuccess;
}

ObjectGetValue(ThermoCouple, Bind, Link, spi);

static int ThermoCoupleValueGet(ThermoCouple *obj, int *val){
  if(!obj->spi.obj){
    return paramConfigError;
  }
  
#if THERMOCOUPLE_USE_POLL_THREAD == TRUE
  if(obj->error == paramSuccess){
    *val = obj->value;
  }
  
  return obj->error;
#else
  
  return tcRequest(obj, val);
#endif
}

ObjectTypeDef(ThermoCouple, "sensor:thermocouple",
              "Thermocouple sensor (MAX6675-based, SPI-connected)",
              ObjectParamDefRW(ThermoCoupleBind, Link, "bind", "Associated hardware interface"),
              ObjectParamDefRO(ThermoCoupleValue, Sint, "value", "Themperature in milli Celsius degrees"));

#if THERMOCOUPLE_USE_POLL_THREAD == TRUE
/* find next active (attached to SPI) sensor */
static ThermoCouple *tcNext(ThermoCouple *obj){
  for(; (obj = (ThermoCouple*)objectNext((BaseObject*)obj)); ){
    if(objectType((BaseObject*)obj) == &ThermoCoupleVMT &&
       obj->spi.obj){
      return obj;
    }
  }
  return NULL;
}
#endif

#ifndef __genlist_h__
#define __genlist_h__ "genlist.h" 

typedef struct GenList GenList;

#define genListAdd(list, node) GenListAdd((GenList**)&(list), (GenList*)(node))
void GenListAdd(GenList **list, GenList *node);

#define genListDel(list, node) GenListDel((GenList**)&(list), (GenList*)(node))
void GenListDel(GenList **list, GenList *node);
GenList *genListNext(GenList **list, GenList *node);

struct GenList {
  GenList *next;
};

#define genListNext(list, node) ((__typeof__(node))((node) ? (((GenList*)(node))->next) : (GenList*)(list)))

#define genListFor(list, node) __typeof__(list) node = NULL; for(; (node = genListNext(list, node)); )

#endif//__genlist_h__

#include "ch.h"

#include "decimal.h"

const decimal dnil = dcon(0, 0, 0);
const decimal dmin = dcon(1, DECIMAL_MANTISSA_MAX, 0);
const decimal dmax = dcon(0, DECIMAL_MANTISSA_MAX, 0);

const char *stod(const char *s, decimal *d){
#if DECIMAL_OPTIM == DECIMAL_FAST
  unsigned n = 0, m = 0, e = 0;
  
  /* negative decimal */
  if(*s == '-'){
    n = 1;
    s++;
  }
  
  bool p = false; /* decimal point sign has captured */
  
  for(; ; s++){
    /* this case is first because more frequently happens */
    if(*s >= '0' && *s <= '9'){
#if DECIMAL_HWMUL
      m = m * 10 + (*s - '0');
#else /* this is useless for MCUs with hardware multiplication (I don't know, how check it) */
      if(m > 0){
        m *= 10;
      }
      m += *s - '0';
#endif
      if(p){
        e ++;
      }
    }else if(*s == '.' && !p){
      p = true;
    }else{
      break;
    }
  }
  
  *d = dcon(n, m, e);
#else /* DECIMAL_LESS */
  *d = dnil;
  
  /* negative decimal */
  if(*s == '-'){
    d->s = 1;
    s++;
  }
  
  bool p = false; /* decimal point sign has captured */
  
  for(; ; s++){
    /* this case is first because more frequently happens */
    if(*s >= '0' && *s <= '9'){
#if DECIMAL_HWMUL
      d->m = d->m * 10 + (*s - '0');
#else /* this is useless for MCUs with hardware multiplication (I don't know, how check it) */
      if(d->m > 0){
        d->m *= 10;
      }
      d->m += *s - '0';
#endif
      if(p){
        d->e ++;
      }
    }else if(*s == '.' && !p){
      p = true;
    }else{
      break;
    }
  }
#endif
  
  return s;
}

char *dtos(char *s, decimal d){
  if(d.s){
    *s++ = '-';
  }
  
  /* save pointer to first char for reverse */
  char *q = s;
  
#if DECIMAL_OPTIM == DECIMAL_FAST
  unsigned m = d.m, e = d.e;
  
  /* excuse me for this c kung-fu */
  do{
    *s++ = (m % 10) + '0';
    m /= 10;
    
    if(e > 0){
      e --;
      if(e == 0){
        *s++ = '.';
      }
    }
  }while(m > 0 || e > 0);
#else /* DECIMAL_LESS */
  /* excuse me for this c kung-fu */
  do{
    *s++ = (d.m % 10) + '0';
    d.m /= 10;
    
    if(d.e > 0){
      d.e --;
      if(d.e == 0){
        *s++ = '.';
      }
    }
  }while(d.m > 0 || d.e > 0);
#endif
  
  /* add leading 0 if missing */
  if(s[-1] == '.'){
    *s++ = '0';
  }
  
  /* save pointer to last char */
  char *r = s;
  *s-- = '\0';
  
  char t;
  
  for(; q < s; ){ /* reverse digits */
    t = *q;
    *q++ = *s;
    *s-- = t;
  }
  
  return r;
}

signed dtoi(decimal d){
  return d.s ? -dtou(d) : dtou(d);
}

#if DECIMAL_OPTIM != DECIMAL_FAST
decimal itod(signed i){
  return dcon(i < 0, i >= 0 ? i : -i, 0);
}
#endif

unsigned dtou(decimal d){
#if DECIMAL_OPTIM == DECIMAL_FAST
  unsigned m = d.m, e = d.e;
  
  for(; e > 1; e--){
    m /= 10;
  }
  
  if(e == 1){
    if(m % 10 >= 5){
      m += 10;
    }
    m /= 10;
  }
  
  return m;
#else /* DECIMAL_LESS */
  for(; d.e; d.e--){
    if(d.e == 1 && d.m % 10 >= 5){
      d.m += 10;
    }
    d.m /= 10;
  }
  
  return d.m;
#endif
}

#if DECIMAL_OPTIM != DECIMAL_FAST
decimal utod(unsigned u){
  return dcon(0, u, 0);
}
#endif

float dtof(decimal d){
#if DECIMAL_OPTIM == DECIMAL_FAST
  unsigned e = d.e, p = 1;
  
  for(; e--; ){
    p *= 10;
  }
  
  return (float)(d.s ? -d.m : d.m) / p;
#else /* DECIMAL_LESS */
  float f = (float)d.m;
  unsigned e = d.e;
  
  for(; d.e; d.e--){
    f /= 10;
  }
  
  return d.s ? -f : f;
#endif
}

decimal ftod(float f){
  unsigned s = 0, e = 0;

  if(f < 0){
    s = 1;
    f = -f;
  }
  
  for(; f < DECIMAL_MANTISSA_MAX / 10; e++){
    f *= 10;
  }
  
  return dcon(s, f, e);
}

#include <string.h>
#include <stdlib.h>
#include <limits.h>

#include "ch.h"
#include "hal.h"

#define USE_CHIBI_PRINTF TRUE

#if USE_CHIBI_PRINTF==TRUE
#include "chprintf.h"
#else
#include "stdio.h"
#include "stdarg.h"
#endif

#include "api.h"

#include "local.h"

#include "genlist.h"
#include "strconv.h"

/*
 * Common API Calls
 */

void tty_printf(const char *fmt, ...){
  va_list ap;
  
  va_start(ap, fmt);
#if USE_CHIBI_PRINTF==TRUE
  chvprintf((BaseSequentialStream*)&SDU2, fmt, ap);
#else
  static char sbuf[256];
  vsnprintf(sbuf, sizeof(sbuf), fmt, ap);
  const char *sptr = sbuf;
  for(; *sptr != '\0' &&
        chSequentialStreamPut(&SDU2, *sptr) == Q_OK;
      sptr++);
#endif
  va_end(ap);
}

int tty_getchar(char *c){
  return chSequentialStreamRead(&SDU2, (uint8_t *)&c, 1) == 1 ? 1 : 0;
}

int tty_getc(void){
  int c = chSequentialStreamGet(&SDU2);
  
  if(c < 0){
    return tty_eof;
  }
  
  return c;
}

int tty_putchar(char c){
  return chSequentialStreamPut(&SDU2, c) == Q_OK ? 1 : 0;
}

#include "ringbuf.h"

ringbuf(char, 512) tty_output;

void tty_init(void){
  ringbuf_init(&tty_output);
}

void tty_swap(void){
  char c;
  for(; ringbuf_get(&tty_output, &c, 1); ){
    tty_putchar(c);
  }
}

int tty_putchar_b(char c){
  return ringbuf_put(&tty_output, &c, 1);
}

void delay_us(int us){
  chThdSleepMicroseconds(us);
}

void delay_ms(int ms){
  chThdSleepMilliseconds(ms);
}

/* objects api */

const BaseObjectVMT *objectTypeFind(const char* name){
  apiDbgCheck(name != NULL, "objectTypeFind(NULL)");
  
  const BaseObjectVMT **vmtp;
  
  objectTypeEach(vmtp){
    if(strcmp(vmtp[0]->name, name) == 0){
      return vmtp[0];
    }
  }
  
  return NULL;
}

const BaseObjectPDT *objectTypeParam(const BaseObjectVMT *vmt, int idx){
  apiDbgCheck(vmt != NULL, "objectTypeParam(NULL, idx)");
  
  if(idx < 0 || idx >= (int)vmt->pcnt){
    return NULL;
  }
  
  return &vmt->pdt[idx];
}

int objectParamFind(const BaseObject *obj, const char *name){
  apiDbgCheck(obj != NULL, "objectParamFind(NULL, name)");
  apiDbgCheck(name != NULL, "objectParamFind(obj, NULL)");
  
  unsigned i = 0;
  
  for(; i < objectType(obj)->pcnt; i++){
    if(strcmp(objectParam(obj, i)->name, name) == 0){
      return i;
    }
  }
  
  return paramIndexError;
}

const ObjectLink nullLink = {
  NULL,
  -1,
};

BaseObject *object_pool = NULL;

void objectAdd(BaseObject *obj){
  apiDbgCheck(obj != NULL, "objectAdd(NULL)");
  
  genListAdd(object_pool, obj);
}

void objectDel(BaseObject *obj){
  apiDbgCheck(obj != NULL, "objectDel(NULL)");
  
  genListDel(object_pool, obj);
}

BaseObject *objectNext(BaseObject *obj){
  return genListNext(object_pool, obj);
}

BaseObject *objectFind(const char *name){
  apiDbgCheck(name != NULL, "objectFind(NULL)");
  
  BaseObject *obj = object_pool;

  for(; obj; obj = obj->next){
    if(strcmp(obj->name, name) == 0){
      return obj;
    }
  }
  
  return NULL;
}

BaseObject *objectCreate(const BaseObjectVMT *vmt, const char *name){
  apiDbgCheck(vmt != NULL, "objectCreate(NULL, name)");
  apiDbgCheck(name != NULL, "objectCreate(vmt, NULL)");
  
  if(!vmt->crt){
    return NULL;
  }
  
  BaseObject* obj = vmt->crt();
  
  if(!obj){
    return NULL;
  }
  
  objectType(obj) = vmt;
  strncpy(obj->name, name, sizeof(obj->name)-1);
  objectAdd(obj);
  
  return obj;
}

void objectRemove(BaseObject *obj){
  apiDbgCheck(obj != NULL, "objectRemove(NULL)");
  
  if(!objectType(obj)->del){
    return;
  }
  
  objectDel(obj);
  
  objectType(obj)->del(obj);
}

int objectParamSet(BaseObject *obj, int idx, const void* raw){
  apiDbgCheck(obj != NULL, "objectParamSet(NULL, idx, raw)");
  apiDbgCheck(idx > -1, "objectParamSet(obj, -1, raw)");
  apiDbgCheck(raw != NULL, "objectParamSet(obj, idx, NULL)");
  
  if(idx < 0 || idx >= (int)objectType(obj)->pcnt){
    return paramIndexError;
  }
  
  return objectParam(obj, idx)->set(obj, raw);
}

int objectParamGet(const BaseObject *obj, int idx, void* raw){
  apiDbgCheck(obj != NULL, "objectParamGet(NULL, idx, raw)");
  apiDbgCheck(idx > -1, "objectParamGet(obj, -1, raw)");
  apiDbgCheck(raw != NULL, "objectParamGet(obj, idx, NULL)");
  
  if(idx < 0 || idx >= (int)objectType(obj)->pcnt){
    return paramIndexError;
  }
  
  return objectParam(obj, idx)->get(obj, raw);
}

int objectParamSetStr(BaseObject *obj, int idx, const char* str){
  apiDbgCheck(obj != NULL, "objectParamSetStr(NULL, idx, str)");
  apiDbgCheck(idx > -1, "objectParamSetStr(obj, -1, str)");
  apiDbgCheck(str != NULL, "objectParamSetStr(obj, idx, NULL)");
  
  if(idx < 0 || idx >= (int)objectType(obj)->pcnt){
    return paramIndexError;
  }
  
  switch(objectParam(obj, idx)->type){
    
#define caseType(ptype, stype)                          \
    case paramType##ptype: {                            \
      paramCType##ptype val;                            \
      if(*sto##stype(str, &val) != '\0'){               \
        return paramValueError;                         \
      }                                                 \
      return objectParam(obj, idx)->set(obj, &val);     \
    }
    
    caseType(Uint, u);
    caseType(Sint, i);
    caseType(Real, f);
    
#undef caseType
    
  case paramTypeCstr:
    return objectParam(obj, idx)->set(obj, (char*)str);
    
  case paramTypeLink: {
    ObjectLink link = nullLink; /* set null link */
    
    if(!(str[0] == '#' && str[1] == '\0')){
      const char *ptr = str;
      /* find object and parameter separator */
      for(; *ptr != '.' && *ptr != '\0'; ptr++);

      if(*ptr == '.'){ /* parameter present */
        char tmp[ptr - str], *p = tmp + sizeof(tmp);
        str = ptr++ - 1;
        *p-- = '\0';
        for(; p >= tmp; *p-- = *str--);
        
        link.obj = objectFind(tmp);
      }else{
        link.obj = objectFind(str);
      }
      
      if(!link.obj){ /* object not found */
        return paramValueError;
      }
      
      if(*ptr != '\0'){ /* parameter present */
        link.par = objectParamFind(link.obj, ptr);
        if(link.par < 0){ /* parameter not found */
          return paramValueError;
        }
      }
    }
    
    return objectParam(obj, idx)->set(obj, &link);
  }
    
  default:
    return paramTypeError;
  }
}

int objectParamGetStr(const BaseObject *obj, int idx, char* str){
  apiDbgCheck(obj != NULL, "objectParamGetStr(NULL, idx, str)");
  apiDbgCheck(idx > -1, "objectParamGetStr(obj, -1, str)");
  apiDbgCheck(str != NULL, "objectParamGetStr(obj, idx, NULL)");
  
  *str = '\0';
  
  if(idx < 0 || idx >= (int)objectType(obj)->pcnt){
    return paramIndexError;
  }
  
  switch(objectParam(obj, idx)->type){
    
#define caseType(ptype, stype)                              \
    case paramType##ptype: {                                \
      paramCType##ptype val;                                \
        int res = objectParam(obj, idx)->get(obj, &val);    \
        if(res == paramSuccess){                            \
          stype##tos(str, val);                             \
        }                                                   \
        return res;                                         \
    }
    
    caseType(Uint, u);
    caseType(Sint, i);
    caseType(Real, f);

#undef caseType

  case paramTypeCstr:
    return objectParam(obj, idx)->get(obj, str);

  case paramTypeLink: {
    ObjectLink link = nullLink; /* set null link */
    int res = objectParam(obj, idx)->get(obj, &link);
    
    if(link.obj){ /* object present */
      const char *ptr = link.obj->name;
      for(; (*str++ = *ptr++) != '\0'; ); /* put object name */
      if(link.par >= 0){ /* parameter present */
        const BaseObjectPDT *pdt = objectParam(link.obj, link.par);
        if(pdt){ /* parameter exists */
          ptr = pdt->name;
          for(str[-1] = '.'; (*str++ = *ptr++) != '\0'; ); /* put parameter name */
        }
      }
    }else{ /* put null link */
      *str++ = '#';
      *str = '\0';
    }
    
    return res;
  }
  default:
    return paramTypeError;
  }
}

ObjectDef(Scale,
          ObjectLink out;
          int in_min;
          int in_max;
          int out_min;
          int out_max;
          int value);

static Scale *ScaleCreate(void){
  Scale *obj = objectNew(Scale);
  
  obj->in_min = obj->out_min = INT_MIN;
  obj->in_max = obj->out_max = INT_MAX;
  obj->value = 0;
  
  return obj;
}

static void ScaleDelete(Scale *obj){
  objectDie(obj);
}

static int ScaleValueSet(Scale* obj, const int *val){
  int in_width = obj->in_max - obj->in_min;
  int out_width = obj->out_max - obj->out_min;
  
  obj->value = (*val - obj->in_min) * out_width / in_width + obj->out_min;
  
  if(obj->out.obj && obj->out.par > -1){
    objectParamSet(obj->out.obj, obj->out.par, &obj->value);
  }
  
  return paramSuccess;
}

ObjectGetValue(Scale, Value, Sint, value);

ObjectGetSetValue(Scale, InMin, Sint, in_min);
ObjectGetSetValue(Scale, InMax, Sint, in_max);

ObjectGetSetValue(Scale, OutMin, Sint, out_min);
ObjectGetSetValue(Scale, OutMax, Sint, out_max);

ObjectGetSetValue(Scale, Out, Link, out);

ObjectTypeDef(Scale, "convert:scale", "Simple linear convertor",
              ObjectParamDefRW(ScaleValue, Sint, "value", "Value"),
              ObjectParamDefRW(ScaleInMin, Sint, "in:min", "Input min"),
              ObjectParamDefRW(ScaleInMax, Sint, "in:max", "Input max"),
              ObjectParamDefRW(ScaleOutMin, Sint, "out:min", "Output min"),
              ObjectParamDefRW(ScaleOutMax, Sint, "out:max", "Output max"),
              ObjectParamDefRW(ScaleOut, Link, "out", "Output"));

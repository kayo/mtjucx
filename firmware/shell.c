/*
    ChibiOS/RT - Copyright (C) 2006-2013 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

/**
 * @file    shell.c
 * @brief   Simple CLI shell code.
 *
 * @addtogroup SHELL
 * @{
 */

#include <string.h>

#include "ch.h"
#include "hal.h"
#include "shell.h"
#include "chprintf.h"

#define LINE_FEED "\r\n"
#define ARG_SEP " \t"

static char *_strtok(char *str, const char *delim, char **saveptr) {
  char *token;
  if (str)
    *saveptr = str;
  token = *saveptr;

  if (!token)
    return NULL;

  token += strspn(token, delim);
  *saveptr = strpbrk(token, delim);
  if (*saveptr)
    *(*saveptr)++ = '\0';

  return *token ? token : NULL;
}

static inline void usage(BaseSequentialStream *chp, char *p) {

  chprintf(chp, "Usage: %s" LINE_FEED, p);
}

static inline void list_commands(BaseSequentialStream *chp, const ShellCommand **cmd) {
  for (; cmd[0] != NULL; cmd ++) {
    chprintf(chp, "%s ", cmd[0]->sc_name);
  }
}

shellCommandDefine(help){
  (void)argv;
  shellStdio(chp);
  
  if (argc != 0) {
    usage(chp, "help");
    return -1;
  }
  
  chprintf(chp, "Commands: ");
  list_commands(chp, shellSelf()->so_config->sc_commands);
  chprintf(chp, LINE_FEED);

  return 0;
}

shellCommandDefine(exit){
  (void)argv;
  shellStdio(chp);
  
  if (argc != 0) {
    usage(chp, "exit");
    return -1;
  }
  
  shellExit(shellSelf(), RDY_OK);

  return 0;
}

shellCommandDefine(info){
  (void)argv;
  shellStdio(chp);
  
  if (argc > 0) {
    usage(chp, "info");
    return -1;
  }
  
  chprintf(chp, "Kernel:       %s\r\n", CH_KERNEL_VERSION);
#ifdef CH_COMPILER_NAME
  chprintf(chp, "Compiler:     %s\r\n", CH_COMPILER_NAME);
#endif
  chprintf(chp, "Architecture: %s\r\n", CH_ARCHITECTURE_NAME);
#ifdef CH_CORE_VARIANT_NAME
  chprintf(chp, "Core Variant: %s\r\n", CH_CORE_VARIANT_NAME);
#endif
#ifdef CH_PORT_INFO
  chprintf(chp, "Port Info:    %s\r\n", CH_PORT_INFO);
#endif
#ifdef PLATFORM_NAME
  chprintf(chp, "Platform:     %s\r\n", PLATFORM_NAME);
#endif
#ifdef BOARD_NAME
  chprintf(chp, "Board:        %s\r\n", BOARD_NAME);
#endif
#ifdef __DATE__
#ifdef __TIME__
  chprintf(chp, "Build time:   %s%s%s\r\n", __DATE__, " - ", __TIME__);
#endif
#endif

  return 0;
}

shellCommandDefine(systime){
  (void)argv;
  shellStdio(chp);
  
  if (argc > 0) {
    usage(chp, "systime");
    return -1;
  }
  
  chprintf(chp, "%lu\r\n", (unsigned long)chTimeNow());

  return 0;
}

const ShellCommand *shellFindCommand(ShellObject *sop, const char *name){
  const ShellCommand **cmd = sop->so_config->sc_commands;
  
  for (; cmd[0] != NULL; cmd ++) {
    if (strcasecmp(cmd[0]->sc_name, name) == 0) {
      break;
    }
  }
  
  return cmd[0];
}

/**
 * @brief   Shell thread function.
 *
 * @param[in] p         pointer to a @p BaseSequentialStream object
 * @return              Termination reason.
 * @retval RDY_OK       terminated by command.
 * @retval RDY_RESET    terminated by reset condition on the I/O channel.
 *
 * @notapi
 */
static msg_t shell_thread(void *arg) {
  ShellObject *sop = (ShellObject*)arg;
  const ShellConfig *scp = sop->so_config;
  BaseSequentialStream *chp = scp->sc_channel;
  
  chRegSetThreadName(scp->sc_name);
  chprintf(chp, LINE_FEED "%s" LINE_FEED, scp->sc_header);
  
#if CH_DBG_ENABLE_STACK_CHECK
  chThdSelf()->p_stklimit = (stkalign_t*)((void*)chThdSelf()->p_stklimit + sizeof(ShellLocal));
#endif
  
  ShellLocal *local = chThdLS();
  
  local->sl_shell = sop;
  local->sl_command = NULL;
  
  while (TRUE) {
    chprintf(chp, scp->sc_prompt);
    if (shellReadLine(sop)) {
      chprintf(chp, LINE_FEED "logout");
      break;
    }
    shellExecLine(sop);
    local->sl_command = NULL;
  }
  shellExit(sop, RDY_OK);
  /* Never executed, silencing a warning.*/
  return 0;
}

msg_t shellExecLine(ShellObject *sop){
  BaseSequentialStream *chp = sop->so_config->sc_channel;
  
  ShellLocal *local = chThdLS();
  local->sl_argc = 0;
  
  char *lp, *tokp;
  lp = _strtok(local->sl_line, ARG_SEP, &tokp);
  
  if (!lp) {
    return 0;
  }
  
  local->sl_command = shellFindCommand(sop, lp);
  
  if (!local->sl_command) {
    chprintf(chp, "unexpected command: %s" LINE_FEED, lp);
    return -1;
  }
  
  for (; (lp = _strtok(NULL, ARG_SEP, &tokp)) != NULL; ) {
    if (local->sl_argc >= SHELL_MAX_ARGUMENTS) {
      chprintf(chp, "too many arguments" LINE_FEED);
      return -1;
    }
    local->sl_argv[local->sl_argc++] = lp;
  }
  local->sl_argv[local->sl_argc] = NULL;
  
  return local->sl_command->sc_function(local->sl_argc, local->sl_argv);
}

/**
 * @brief   Shell object initialization.
 *
 * @api
 */
void shellInit(ShellObject *sop, const ShellConfig *scp) {
  sop->so_config = scp;
  chEvtInit(&sop->so_terminated);
}

/**
 * @brief   Terminates the shell.
 * @note    Must be invoked from the command handlers.
 * @note    Does not return.
 *
 * @param[in] msg       shell exit code
 *
 * @api
 */
void shellExit(ShellObject *sop, msg_t msg) {

  /* Atomically broadcasting the event source and terminating the thread,
     there is not a chSysUnlock() because the thread terminates upon return.*/
  chSysLock();
  chEvtBroadcastI(&sop->so_terminated);
  chThdExitS(msg);
}

/**
 * @brief   Spawns a new shell.
 * @pre     @p CH_USE_HEAP and @p CH_USE_DYNAMIC must be enabled.
 *
 * @param[in] scp       pointer to a @p ShellConfig object
 * @param[in] size      size of the shell working area to be allocated
 * @param[in] prio      priority level for the new shell
 * @return              A pointer to the shell thread.
 * @retval NULL         thread creation failed because memory allocation.
 *
 * @api
 */
#if CH_USE_HEAP && CH_USE_DYNAMIC
Thread *shellCreate(ShellObject *sop, size_t size, tprio_t prio) {

  return chThdCreateFromHeap(NULL, size, prio, shell_thread, (void *)sop);
}
#endif

/**
 * @brief   Create statically allocated shell thread.
 *
 * @param[in] scp       pointer to a @p ShellConfig object
 * @param[in] wsp       pointer to a working area dedicated to the shell thread stack
 * @param[in] size      size of the shell working area
 * @param[in] prio      priority level for the new shell
 * @return              A pointer to the shell thread.
 *
 * @api
 */
Thread *shellCreateStatic(ShellObject *sop, void *wsp,
                          size_t size, tprio_t prio) {

  return chThdCreateStatic(wsp, size, prio, shell_thread, (void *)sop);
}

/**
 * @brief   Reads a whole line from the input channel.
 *
 * @param[in] sop       pointer to a @p ShellObject object
 * @param[in] line      pointer to the line buffer
 * @param[in] size      buffer maximum length
 * @return              The operation status.
 * @retval TRUE         the channel was reset or CTRL-D pressed.
 * @retval FALSE        operation successful.
 *
 * @api
 */
bool_t shellReadLine(ShellObject *sop) {
  BaseSequentialStream *chp = sop->so_config->sc_channel;
  char *line = shellLinePtr(), *end = shellLineEnd();
  char *p = line;
  
  while (TRUE) {
    char c;
    
    if (chSequentialStreamRead(chp, (uint8_t *)&c, 1) == 0)
      return TRUE;
    if (c == '\4') { /*  */
      chprintf(chp, "^D");
      return TRUE;
    }
    if (c == '\b') { /* backspace */
      if (p != line) { /* to beginning of line ( abcd| ) */
        chSequentialStreamPut(chp, c);    /* < ( abc|d ) */
        chSequentialStreamPut(chp, 0x20); /* _ ( abc_| ) */
        chSequentialStreamPut(chp, c);    /* < ( abc|_ ) */
        p--;
      }
      continue;
    }
    if (c == '\r') { /* carret return */
      chprintf(chp, LINE_FEED);
      *p = 0;
      return FALSE;
    }
    if (c < 0x20)
      continue;
    if (p < end) {
      chSequentialStreamPut(chp, c);
      *p++ = (char)c;
    }
  }
}

/** @} */

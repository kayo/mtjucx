#ifndef __strconv_h__
#define __strconv_h__ "strconv.h"

/* returns pointer to invalid character or NULL on success */
const char* stou(const char *str, unsigned *val);
const char* stoi(const char *str, signed *val);
const char* stof(const char *str, float *val);

/* returns pointer to end of converted value characters */
char *utos(char *str, unsigned val);
char *itos(char *str, signed val);
char *ftos(char *str, float val);

#endif//__strconv_h__

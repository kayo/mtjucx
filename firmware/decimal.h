#ifndef __decimal_h__
#define __decimal_h__ "decimal.h"

#include <stdbool.h>

#define DECIMAL_MES 1
#define DECIMAL_SEM 2
#define DECIMAL_MSE 3
#define DECIMAL_SME 4

#ifndef DECIMAL_ORDER
#define DECIMAL_ORDER DECIMAL_MSE
#endif

#define DECIMAL_LESS 1
#define DECIMAL_FAST 2

#ifndef DECIMAL_OPTIM
#define DECIMAL_OPTIM DECIMAL_FAST
#endif

#ifndef DECIMAL_HWMUL
#define DECIMAL_HWMUL 1
#endif

#ifndef DECIMAL_MANTISSA_BITS
#define DECIMAL_MANTISSA_BITS 24
#endif

#define DECIMAL_MANTISSA_MAX ((1 << DECIMAL_MANTISSA_BITS) - 1)

#ifndef DECIMAL_EXPONENT_BITS
#define DECIMAL_EXPONENT_BITS 7
#endif

#define DECIMAL_EXPONENT_MAX ((1 << DECIMAL_EXPONENT_BITS) - 1)

typedef struct PACK_STRUCT_BEGIN {
#if DECIMAL_ORDER == DECIMAL_MES
  unsigned m: DECIMAL_MANTISSA_BITS; /**< mantissa */
  unsigned e: DECIMAL_EXPONENT_BITS;  /**< exponent */
  unsigned s: 1;  /**< sign */
#endif
#if DECIMAL_ORDER == DECIMAL_SEM
  unsigned s: 1;  /**< sign */
  unsigned e: DECIMAL_EXPONENT_BITS;  /**< exponent */
  unsigned m: DECIMAL_MANTISSA_BITS; /**< mantissa */
#endif
#if DECIMAL_ORDER == DECIMAL_MSE
  unsigned m: DECIMAL_MANTISSA_BITS; /**< mantissa */
  unsigned s: 1;  /**< sign */
  unsigned e: DECIMAL_EXPONENT_BITS;  /**< exponent */
#endif
#if DECIMAL_ORDER == DECIMAL_SME
  unsigned s: 1; /**< mantissa */
  unsigned m: DECIMAL_MANTISSA_BITS;  /**< sign */
  unsigned e: DECIMAL_EXPONENT_BITS;  /**< exponent */
#endif
} PACK_STRUCT_STRUCT PACK_STRUCT_END decimal;

/**
   @brief Decimal null value.
*/
extern const decimal dnil;

/**
   @brief Decimal minimal value.
*/
extern const decimal dmin;

/**
   @brief Decimal maximal value.
*/
extern const decimal dmax;

#if 0
static inline decimal dcon(unsigned s,
                           unsigned m,
                           unsigned e){
#if DECIMAL_ORDER == DECIMAL_MES
  return (decimal){ m, e, s };
#endif
#if DECIMAL_ORDER == DECIMAL_SEM
  return (decimal){ s, e, m };
#endif
#if DECIMAL_ORDER == DECIMAL_MSE
  return (decimal){ m, s, e };
#endif
#if DECIMAL_ORDER == DECIMAL_SME
  return (decimal){ s, m, e };
#endif
}
#else

#if DECIMAL_ORDER == DECIMAL_MES
#define dcon(s, m, e) ((decimal){ (m), (e), (s) })
#endif
#if DECIMAL_ORDER == DECIMAL_SEM
#define dcon(s, m, e) ((decimal){ (s), (e), (m) })
#endif
#if DECIMAL_ORDER == DECIMAL_MSE
#define dcon(s, m, e) ((decimal){ (m), (s), (e) })
#endif
#if DECIMAL_ORDER == DECIMAL_SME
#define dcon(s, m, e) ((decimal){ (s), (m), (e) })
#endif

#endif

const char* stod(const char *s, decimal *d);
char *dtos(char *s, decimal d);

/**
   @brief Check decimal is signed integer.
   
   Check decimal @p d has integer part only.
*/
static inline bool disi(decimal d){ return d.e == 0; }

/**
   @brief Convert decimal to signed integer.
   
   Convert decimal @p d to signed integer value.
*/
signed dtoi(decimal d);

/**
   @brief Convert signed integer to decimal.
   
   Convert signed integer value @p i to decimal.
*/
#if DECIMAL_OPTIM != DECIMAL_FAST
decimal itod(signed s);
#else
static inline decimal itod(signed i){
  return dcon(i < 0, i >= 0 ? i : -i, 0);
}
#endif

/**
   @brief Check decimal is unsigned integer.
   
   Check decimal @p d is positive and has integer part only.
*/
static inline bool disu(decimal d){ return d.e == 0 && d.s == 0; }

/**
   @brief Convert decimal to unsigned integer.
   
   Convert decimal @p d to unsigned integer value.
*/
unsigned dtou(decimal d);

/**
   @brief Convert unsigned integer to decimal.
   
   Convert unsigned integer value @p u to decimal.
*/
#if DECIMAL_OPTIM != DECIMAL_FAST
decimal utod(unsigned u);
#else
static inline decimal utod(unsigned u){
  return dcon(0, u, 0);
}
#endif

#define disf(d) 1

/**
   @brief Convert decimal to floating point.
   
   Convert decimal @p d to floating point value.
*/
float dtof(decimal d);

/**
   @brief Convert floating point to decimal.
   
   Convert floating point value @p f to decimal with precision @p p.
*/
decimal ftod(float f);

#define disd(d) 1
#define dtod(d) (d)

#endif//__decimal_h__

#ifndef __api_h__
#define __api_h__ "api.h"

#define API_VERSION "0.0.2"

#include "ch.h"
#include "hal.h"

#ifndef API_ENABLE_DBG_CHECK
#define API_ENABLE_DBG_CHECK TRUE
#endif

#if API_ENABLE_DBG_CHECK == TRUE
#define apiDbgCheck(c, n) chDbgCheck(c, n)
#else
#define apiDbgCheck(c, n)
#endif

#define lengthof(array) (sizeof(array)/sizeof(array[0]))

void delay_us(int us);
void delay_ms(int ms);

/**
 * @group Serial Console API
 */

enum {
  tty_eof = -1
};

void tty_init(void);
void tty_swap(void);

/**
 * @brief Formatted print to serial console
 * @param format Printf-like format
 * @param args... Substitution arguments
 */
void tty_printf(const char *format, ...); // __attribute__ ((format (printf, 1, 2)));

/**
 * @brief Get character from serial console
 * @param c Pointer to character cell to read to
 * @return 1 - OK, 0 - EOF caught
 */
int tty_getchar(char *c);

/**
 * @brief Get character from serial console
 * @return Character or EOF
 */
int tty_getc(void);

/**
 * @brief Put character to serial console
 * @param c Character to put
 * @return 1 - OK, 0 - EOF caught
 */
int tty_putchar(char c);
int tty_putchar_b(char c);

/**
 * @group Unified Object API
 */
typedef enum BaseParamType BaseParamType;
typedef struct BaseObjectPDT BaseObjectPDT;
typedef struct BaseObjectVMT BaseObjectVMT;
typedef struct BaseObject BaseObject;
typedef struct ObjectLink ObjectLink;

enum BaseParamType {
  paramTypeNone = 0,
  paramTypeUint,
  paramTypeSint,
  paramTypeReal,
  paramTypeCstr,
  paramTypeLink,
};

typedef void            paramCTypeNone;
typedef unsigned        paramCTypeUint;
typedef signed          paramCTypeSint;
typedef float           paramCTypeReal;
typedef char*           paramCTypeCstr;
typedef ObjectLink      paramCTypeLink;

struct ObjectLink {
  BaseObject* obj;
  int par;
};

extern const ObjectLink nullLink;

enum BaseParamStat {
  paramSuccess     =  0,
  paramIndexError  = -1,
  paramTypeError   = -2,
  paramValueError  = -3,
  paramRangeError  = -4,
  paramAccessError = -5,
  paramConfigError = -6,
  paramDeviceError = -7,
};

typedef int ObjectParamSetM(BaseObject* obj, const void* raw);
typedef int ObjectParamGetM(const BaseObject* obj, void* raw);

#define ObjectSetValue(_pref_, _root_, _type_, _name_)            \
  static int _pref_##_root_##Set(_pref_ *obj,                     \
                                 const paramCType##_type_* val){  \
    obj->_name_ = *val;                                           \
    return paramSuccess;                                          \
  }

#define ObjectGetValue(_pref_, _root_, _type_, _name_)        \
  static int _pref_##_root_##Get(const _pref_ *obj,           \
                                 paramCType##_type_* val){    \
    *val = obj->_name_;                                       \
    return paramSuccess;                                      \
  }

#define ObjectSetGetValue(_pref_, _root_, _type_, _name_) \
  ObjectSetValue(_pref_, _root_, _type_, _name_);         \
  ObjectGetValue(_pref_, _root_, _type_, _name_)

#define ObjectGetSetValue(_pref_, _root_, _type_, _name_) \
  ObjectSetGetValue(_pref_, _root_, _type_, _name_)

struct BaseObjectPDT {
  BaseParamType type;
  const char *name;
  const char *info;
  const ObjectParamSetM *set;
  const ObjectParamGetM *get;
};

#define ObjectParamDef(type, name, info, set, get) { \
      paramType##type, name, info,                   \
      (const ObjectParamSetM*)set,                   \
      (const ObjectParamGetM*)get                    \
    }

#define ObjectParamDefNO(pref, type, name, info)  \
  ObjectParamDef(type, name, info, NULL, NULL)

#define ObjectParamDefRW(pref, type, name, info)          \
  ObjectParamDef(type, name, info, pref##Set, pref##Get)

#define ObjectParamDefRO(pref, type, name, info)    \
  ObjectParamDef(type, name, info, NULL, pref##Get)

#define ObjectParamDefWO(pref, type, name, info)    \
  ObjectParamDef(type, name, info, pref##Set, NULL)

typedef BaseObject* ObjectCreateM(void);
typedef void ObjectDeleteM(BaseObject* obj);

struct BaseObjectVMT {
  const char *name;
  const char *info;
  
  const ObjectCreateM *crt;
  const ObjectDeleteM *del;
  
  const unsigned pcnt;
  const BaseObjectPDT *pdt;
};

extern const BaseObjectVMT *object_type[];

#define ObjectTypes(types...)                   \
  const BaseObjectVMT *object_type[] = {        \
    types                                       \
    NULL                                        \
  };

#define ObjectTypeRef(type)                     \
  &(type##VMT),

#define ObjectTypeExt(type)                     \
  extern const BaseObjectVMT type##VMT;

#define ObjectTypeDef(type, name, info, pars...)        \
  static const BaseObjectPDT type##PDT[] = { pars };    \
  const BaseObjectVMT type##VMT = {                     \
    name,                                               \
    info,                                               \
    (const ObjectCreateM*)type##Create,                 \
    (const ObjectDeleteM*)type##Delete,                 \
    lengthof(type##PDT),                                \
    type##PDT                                           \
  }

struct BaseObject {
  BaseObject *next;
  
  const BaseObjectVMT *vmt;
  char name[16];
};

#define ObjectDef(type, fields...)       \
  typedef struct type type;              \
  struct type {                          \
    BaseObject base;                     \
    fields;                              \
  }

#define ObjectExt(decl) extern decl

#define ObjectBuiltin(type, decl, name, fields...)  \
  decl = {                                          \
    {                                               \
      NULL,                                         \
      &type##VMT,                                   \
      name,                                         \
    },                                              \
    fields                                          \
  }

#define objectReg(name, op)                     \
  object##op((BaseObject*)&(name))

#define objectTypeEach(vmtp)                        \
  for(vmtp = object_type; vmtp[0] != NULL; vmtp++)
const BaseObjectVMT *objectTypeFind(const char* name);
const BaseObjectPDT *objectTypeParam(const BaseObjectVMT *vmt, int idx);

#define objectType(obj) ((obj)->vmt)
BaseObject *objectNext(BaseObject *obj);
BaseObject *objectFind(const char *name);
void objectAdd(BaseObject *obj);
void objectDel(BaseObject *obj);
BaseObject *objectCreate(const BaseObjectVMT *vmt, const char *name);
void objectRemove(BaseObject *obj);

#define objectParam(obj, idx) objectTypeParam(objectType(obj), idx)
int objectParamFind(const BaseObject *obj, const char *name);
int objectParamSet(BaseObject *obj, int idx, const void* raw);
int objectParamGet(const BaseObject *obj, int idx, void* raw);
int objectParamSetStr(BaseObject *obj, int idx, const char* str);
int objectParamGetStr(const BaseObject *obj, int idx, char* str);

#define objectNew(type) ((type*)chHeapAlloc(NULL, sizeof(type)))
#define objectDie(obj) chHeapFree((void*)(obj))

/**
 * @group App main function hack
 */
#define api_cat2(a, b) a##b
#define app_main(name) api_cat2(name, _run)
#ifdef APP
#  define main app_main(APP)
#endif

#endif//__api_h__

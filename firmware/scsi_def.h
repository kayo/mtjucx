#ifndef __scsi_h__
#define __scsi_h__ "scsi.h"

#define SCSI_CMD_INQUIRY                                 0x12
#define SCSI_CMD_REQUEST_SENSE                           0x03
#define SCSI_CMD_READ_CAPACITY_10                        0x25
#define SCSI_CMD_READ_10                                 0x28
#define SCSI_CMD_WRITE_10                                0x2A
#define SCSI_CMD_TEST_UNIT_READY                         0x00
#define SCSI_CMD_PREVENT_ALLOW_MEDIUM_REMOVAL            0x1E
#define SCSI_CMD_VERIFY_10                               0x2F
#define SCSI_CMD_SEND_DIAGNOSTIC                         0x1D
#define SCSI_CMD_MODE_SENSE_6                            0x1A
#define SCSI_CMD_START_STOP_UNIT                         0x1B

#define SCSI_SENSE_KEY_NO_SENSE                          0x00
#define SCSI_SENSE_KEY_RECOVERED_ERROR                   0x01
#define SCSI_SENSE_KEY_NOT_READY                         0x02
#define SCSI_SENSE_KEY_MEDIUM_ERROR                      0x03
#define SCSI_SENSE_KEY_HARDWARE_ERROR                    0x04
#define SCSI_SENSE_KEY_ILLEGAL_REQUEST                   0x05
#define SCSI_SENSE_KEY_UNIT_ATTENTION                    0x06
#define SCSI_SENSE_KEY_DATA_PROTECT                      0x07
#define SCSI_SENSE_KEY_BLANK_CHECK                       0x08
#define SCSI_SENSE_KEY_VENDOR_SPECIFIC                   0x09
#define SCSI_SENSE_KEY_COPY_ABORTED                      0x0A
#define SCSI_SENSE_KEY_ABORTED_COMMAND                   0x0B
#define SCSI_SENSE_KEY_VOLUME_OVERFLOW                   0x0D
#define SCSI_SENSE_KEY_MISCOMPARE                        0x0E

#define SCSI_ASENSE_NO_ADDITIONAL_INFORMATION            0x00
#define SCSI_ASENSE_LOGICAL_UNIT_NOT_READY               0x04
#define SCSI_ASENSE_INVALID_FIELD_IN_CDB                 0x24
#define SCSI_ASENSE_NOT_READY_TO_READY_CHANGE            0x28
#define SCSI_ASENSE_WRITE_PROTECTED                      0x27
#define SCSI_ASENSE_FORMAT_ERROR                         0x31
#define SCSI_ASENSE_INVALID_COMMAND                      0x20
#define SCSI_ASENSE_LOGICAL_BLOCK_ADDRESS_OUT_OF_RANGE   0x21
#define SCSI_ASENSE_MEDIUM_NOT_PRESENT                   0x3A

#define SCSI_ASENSEQ_NO_QUALIFIER                        0x00
#define SCSI_ASENSEQ_FORMAT_COMMAND_FAILED               0x01
#define SCSI_ASENSEQ_INITIALIZING_COMMAND_REQUIRED       0x02
#define SCSI_ASENSEQ_OPERATION_IN_PROGRESS               0x07

#define SCSI_INQUIRY_REQUEST_CMDDT                       (1 << 1)
#define SCSI_INQUIRY_REQUEST_EVPD                        (1 << 0)

#define SCSI_DIAGNOSTIC_REQUEST_SENFTEST                 (1 << 2)

#define SCSI_SSU_REQUEST_START                           (1 << 0)
#define SCSI_SSU_REQUEST_LOEJ                            (1 << 1)

typedef union {
  PACK_STRUCT_BEGIN struct {
    uint8_t opCode;
  } PACK_STRUCT_STRUCT PACK_STRUCT_END;
  uint8_t byte[16];
} SCSIRequest;

typedef union {
  PACK_STRUCT_BEGIN struct {
    uint8_t ResponseCode;
    uint8_t Obsolete;
    uint8_t SenseKey;
    uint32_t Information;
    uint8_t AddSenseLen;
    uint32_t CmdSpecificInfo;
    uint8_t ASC;
    uint8_t ASCQ;
    uint8_t FRUC;
    uint8_t SenseKeySpecific[3];
  } PACK_STRUCT_STRUCT PACK_STRUCT_END;
  uint8_t byte[18];
} SCSISenseResponse;

typedef union {
  PACK_STRUCT_BEGIN struct {
    uint8_t opCode;
    uint8_t obsolete;
    uint8_t pageCode;
    uint16_t acLength;
    uint8_t control;
  } PACK_STRUCT_STRUCT PACK_STRUCT_END;
  uint8_t byte[6];
} SCSIInquiryRequest;

typedef union {
  PACK_STRUCT_BEGIN struct {
    uint8_t Peripheral;
    uint8_t Removable;
    uint8_t Version;
    uint8_t ResponseDataFormat;
    uint8_t AdditionalLength;
    uint8_t Sccstp;
    uint8_t bqueetc;
    uint8_t CmdQue;
    char vendorID[8];
    char productID[16];
    char productRev[4];
  } PACK_STRUCT_STRUCT PACK_STRUCT_END;
  uint8_t byte[36];
} SCSIInquiryResponse;

typedef union {
  PACK_STRUCT_BEGIN struct {
    uint8_t opCode;
    uint8_t obsolete;
    uint8_t __reserved;
    uint16_t paramListLength;
    uint8_t control;
  } PACK_STRUCT_STRUCT PACK_STRUCT_END;
  uint8_t byte[6];
} SCSISendDiagnosticRequest;

typedef union {
  PACK_STRUCT_BEGIN struct {
    uint8_t modeDataLen;
    uint8_t mediumType;
    uint8_t writeProtect;
    uint8_t blockDescLen;
  } PACK_STRUCT_STRUCT PACK_STRUCT_END;
  uint8_t byte[4];
} SCSIModeSense6Response;

typedef union {
  PACK_STRUCT_BEGIN struct {
    uint32_t lastBlockAddr;
    uint32_t blockSize;
  } PACK_STRUCT_STRUCT PACK_STRUCT_END;
  uint8_t byte[8];
} SCSIReadCapacity10Response;

typedef union {
  PACK_STRUCT_BEGIN struct {
    uint8_t opCode;
    uint8_t lunImmed;
    uint8_t __reserved1;
    uint8_t __reserved2;
    uint8_t loejStart;
    uint8_t control;
  } PACK_STRUCT_STRUCT PACK_STRUCT_END;
  uint8_t byte[6];
} SCSIStartStopUnitRequest;

typedef union {
  PACK_STRUCT_BEGIN struct {
    uint8_t opCode;
    uint8_t obsolete;
    uint32_t blockAddr;
    uint8_t groupNum;
    uint16_t trLength;
    uint8_t control;
  } PACK_STRUCT_STRUCT PACK_STRUCT_END;
  uint8_t byte[10];
} SCSIReadWrite10Request;

#endif//__scsi_h__

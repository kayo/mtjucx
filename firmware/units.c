#include "units.h"

decimal unitsConv(decimal d, Units f, Units t){
  switch(f){
  case IN:
    switch(t){
    case MM:
      /* 1 in = 25.4 mm */
      d.m *= 254; /* d * 254 */
      d.e += 1;   /* d / 10 */
      break;
    case UM:
      /* 1 in = 25400 um */
      if(d.e >= 2){
        d.e -= 2;   /* d * 100 */
      }else{
        d.m *= 100;
      }
      d.m *= 254; /* d * 254 */
      break;
    default:
      break;
    }
    break;
  case MM:
    switch(t){
    case IN:
      /* 1 mm = 3937e-5 in */
      d.m *= 3937; /* d * 3937 */
      d.e += 5;    /* d / 100000 */
      break;
    case UM:
      /* 1 mm = 1000 um */
      if(d.e >= 3){
        d.e -= 3;   /* d * 1000 */
      }else{
        d.m *= 1000;
      }
      break;
    default:
      break;
    }
    break;
  case UM:
    switch(t){
    case IN:
      /* 1 um = 3937e-8 in */
      d.m *= 3937; /* d * 3937 */
      d.e += 8;   /* d / 100000000 */
      break;
    case MM:
      /* 1 um = 1e-3 mm */
      d.e += 3;   /* d / 1000 */
      break;
    default:
      break;
    }
    break;
  default:
    break;
  }
  
  return d;
}

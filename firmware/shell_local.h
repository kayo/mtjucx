#ifndef __shell_local_h__
#define __shell_local_h__ "shell_local.h" 

#define shellAdvancedCommands()    \
  shellCommandExtern(echo);        \
  shellCommandExtern(free);        \
  shellCommandExtern(ps);          \
  shellCommandExtern(kill);        \
  shellCommandExtern(hwclock)
  
#define shellAdvancedCommandsRef() \
  shellCommandRef(echo)            \
  shellCommandRef(free)            \
  shellCommandRef(ps)              \
  shellCommandRef(kill)            \
  shellCommandRef(hwclock)

#endif//__shell_local_h__

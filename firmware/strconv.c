#include "strconv.h"

char sisf(const char *str){
  if(*str == '-'){ /* skip sign */
    str ++;
  }
  for(; *str != '\0'; str ++){
    if((*str < '0' || *str > '9') && *str != '.'){ /* isn't digit and point */
      return 0;
    }
  }
  return 1;
}

const char* stou(const char *str, unsigned *val){
  for(*val = 0; *str >= '0' && *str <= '9'; str ++){
    *val = *val * 10 + (*str - '0');
  }
  return str;
}

const char* stoi(const char *str, signed *val){
  char neg = 0;
  
  if(*str == '-'){ /* skip sign */
    neg = 1;
    str ++;
  }
  
  str = stou(str, (unsigned*)val);
  
  if(neg){
    *val = -*val;
  }
  
  return str;
}

const char* stof(const char *str, float *val){
  signed decim_;
  
  str = stoi(str, &decim_);
  
  if(*str != '.'){
    *val = (float)decim_;
    return str;
  }
  
  unsigned fract_;
  const char *end = stou(++str, &fract_);
  
  if(str == end){
    *val = (float)decim_;
    return str;
  }
  
  unsigned divis_ = 1;
  for(; str < end; str ++){
    divis_ *= 10;
  }
  
  *val = (float)decim_ + (float)fract_/(float)divis_;
  return str;
}

char *utos(char *str, unsigned val){
  char *p = str, *q = str, *e;
  char t;
  
  do {
    *p++ = (val % 10) + '0';
    val /= 10;
  } while (val > 0);
  
  e = p;
  *p-- = '\0';
  
  while (q < p) { /* reverse digits */
    t = *q;
    *q++ = *p;
    *p-- = t;
  }

  return e;
}

char *itos(char *str, signed val){
  if(val < 0){
    *str++ = '-';
    val = -val;
  }
  
  return utos(str, val);
}

char *ftos(char* str, float val){
  signed ipart = val;
  signed fpart = (val - ipart) * 1000000;
  
  if(fpart < 0){
    fpart = -fpart;
  }
  
  str = itos(str, ipart);
  
  *str++ = '.';
  
  str = utos(str, fpart);
  
  for(; *str == '0'; *str-- = '\0');
  
  if(*str == '.'){
    *str = '\0';
  }
  
  return str;
}

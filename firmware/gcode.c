#include "ch.h"

#include "gcode.h"
#include "strconv.h"

#include "chprintf.h"

GCodeField *gcodeFieldFind(GCodeEntry *gcep, char code){
  unsigned i = 0, n = gcodeFields(gcep);

  for(; i < n; i++){
    GCodeField *gcfp = gcodeField(gcep, i);

    if(gcodeFieldCode(gcfp) == code){
      return gcfp;
    }
  }
  
  return NULL;
}

void gcodeEntryPrint(BaseSequentialStream *bssp, GCodeEntry *gcep){
  chprintf(bssp, "%d: ", gcep->lnum);
  
  unsigned i = 0, n = gcodeFields(gcep);
  
  for(; i < n; i++){
    GCodeField *gcfp = gcodeField(gcep, i);
    char buf[16];
    
    dtos(buf, gcodeFieldData(gcfp));
    
    chprintf(bssp, "%c%s ", gcodeFieldCode(gcfp), buf);
  }
  
  chprintf(bssp, "#%d\r\n", gcep->csum);
}

static void gcodeEntryInit(GCodeEntry *gcep, int lnum){
  gcep->lnum = lnum;
  gcep->csum = 0;
  
  gcep->gcfn = 0;
}

static char gcodeEntryCheck(GCodeEntry *gcep){
  unsigned csum = 0;
  
  return gcodeFieldGet(gcep, '*', u, &csum) == gcodeMissing || csum == gcep->csum;
}

void gcodeObjectInit(GCodeObject *gcop){
  gcop->gcep = NULL;
  gcop->lnum = 0;
  gcop->seek = 0;
}

static void gcodeFieldClose(GCodeObject *gcop){
  GCodeEntry *gcep = gcop->gcep;
  
  if(gcep->gcfn < 1){
    return;
  }

  GCodeField *gcfp = &gcep->gcfa[gcep->gcfn-1];
  
  gcop->astr[gcop->alen] = '\0';
  
  stod(gcop->astr, &gcfp->data);
}

static void gcodeFieldOpen(GCodeObject *gcop, char code){
  GCodeEntry *gcep = gcop->gcep;
  
  GCodeField *gcfp = &gcep->gcfa[gcep->gcfn++];
  
  gcfp->code = code;
  
  gcop->alen = 0;
}

static GCodeState gcodeStepParse(GCodeObject *gcop, char chr){
  GCodeEntry *gcep = gcop->gcep;
  
  if(('0' <= chr && chr <= '9')
     || chr == '.' || chr == '-'){
    if(gcep->gcfn < 1){
      return gcodeParseError;
    }
    
    if(gcep->gcfn > sizeof(gcep->gcfa) ||
       gcop->alen >= sizeof(gcop->astr)){
      return gcodeOverflowError;
    }
    
    gcop->astr[gcop->alen++] = chr;
    
    return gcodeContinue;
  }
  
  if(chr == '\0'){
    gcodeFieldClose(gcop);
    
    return gcodeSuccess;
  }
  
  if('a' <= chr && chr <= 'z'){
    chr += 'A' - 'a';
  }
  
  if(('A' <= chr && chr <= 'Z') || chr == '*'){
    gcodeFieldClose(gcop);
    gcodeFieldOpen(gcop, chr);
    
    return gcodeContinue;
  }
  
  return gcodeParseError;
}

static GCodeState gcodePostParse(GCodeObject *gcop){
  /* suppress empty entry */
  gcodeStepParse(gcop, '\0');
  
  /* check transfer */
  if(!gcodeEntryCheck(gcop->gcep)){
    return gcodeTransferError;
  }
  
  /* fix line lumber */
  unsigned lnum;
  if(gcodeFieldGet(gcop->gcep, 'N', u, &lnum) == gcodeSuccess){
    gcop->lnum = gcop->gcep->lnum = lnum;
  }
  
  gcop->lnum++;
  
  return gcodeSuccess;
}

void gcodeStartParse(GCodeObject *gcop, GCodeEntry *gcep){
  gcop->gcep = gcep;
  gcop->csum = 0;
  gcodeEntryInit(gcop->gcep, gcop->lnum);
}

static const char gcodeSkipChar[] = " \t\r";

GCodeState gcodeParseChar(GCodeObject *gcop, char chr){
  if(!gcop->gcep){
    return gcodeMissing;
  }
  
  if(gcop->seek && (chr == '\n' || chr == '\0')){
    gcop->seek = 0;
    return gcodeContinue;
  }
  
  if(chr == ';' || chr == '\r' || chr == '\n' || chr == '\0'){
    if(!(chr == '\n' || chr == '\0')){
      gcop->seek = 1;
    }
    return gcodePostParse(gcop);
  }
  
  if(chr == '*'){
    gcop->gcep->csum = gcop->csum;
  }
  
  gcop->csum ^= chr;
  
  unsigned int i = 0;
  
  for(; i < sizeof(gcodeSkipChar); i++){
    if(chr == gcodeSkipChar[i]){
      return gcodeContinue;
    }
  }
  
  return gcodeStepParse(gcop, chr);
}

GCodeState gcodeParseStream(GCodeObject *gcop, BaseSequentialStream *bssp){
  GCodeState gcs;
  char chr;
  
  for(; ; ){
    if(chSequentialStreamRead(bssp, (uint8_t*)&chr, 1) != 1){
      return gcodeMissing;
    }
    if((gcs = gcodeParseChar(gcop, chr)) != gcodeContinue){
      return gcs;
    }
  }
  
  return gcs;
}

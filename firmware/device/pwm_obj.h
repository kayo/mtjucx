#ifndef __pwm_obj_h__
#define __pwm_obj_h__ "pwm_obj.h"

typedef struct {
  const ioportid_t ptr;
  const int8_t bit;
  const uint8_t idx;
} PWMChConfig;

typedef struct {
  unsigned wid;
} PWMChState;

ObjectDef(PWMObject,
          PWMDriver *pwm;
          PWMConfig *cfg;
          unsigned freq;
          const unsigned n_channels;
          const PWMChConfig *ch_config;
          PWMChState *ch_state);

void PWMInitialize(PWMObject* obj);

#ifdef PWM_1_DEV
ObjectTypeExt(PWM1Object);
#define PWM_1_TR ObjectTypeRef(PWM1Object)
ObjectExt(PWMObject pwm_1);
#define PWM_1_OA PWMInitialize(&pwm_1); objectReg(pwm_1, Add);
#else
#define PWM_1_TR
#define PWM_1_OA
#endif

#define pwmTypesRef() PWM_1_TR
#define pwmObjectsAdd() PWM_1_OA

#define pwmPrefix "device:pwm"

#endif//__pwm_obj_h__

#ifndef __stp_obj_h__
#define __stp_obj_h__ "stp_obj.h"

#ifndef STP_WITH_STEP_SELECT
#define STP_WITH_STEP_SELECT TRUE
#endif

#ifndef STP_USE_A4988
#define STP_USE_A4988 TRUE
#endif

#ifndef STP_USE_DRV8824
#define STP_USE_DRV8824 FALSE
#endif

typedef struct {
  const ioportid_t ptr;
  const int8_t bit;
} STPPad;

typedef struct {
  const STPPad dir;
  const STPPad stp;
  const STPPad enb;
#if STP_WITH_STEP_SELECT == TRUE
  const STPPad mss[3];
#endif
  const STPPad min;
  const STPPad max;
} STPConfig;

ObjectDef(STPObject,
          const STPConfig *cfg;
          /* stepper config */
          unsigned div; /**< current steps div */
          unsigned spm; /**< steps per millimeter */
          /* endstops */
          signed min;
          signed max;
          );

void STPDir(STPObject *obj, bool_t dir);
void STPStp(STPObject *obj, bool_t stp);
void STPEnb(STPObject *obj, bool_t enb);
bool_t STPMin(const STPObject *obj);
bool_t STPMax(const STPObject *obj);

static inline void stpDir(BaseObject *obj, bool_t dir){
  STPDir((STPObject*)obj, dir);
}

static inline void stpStp(BaseObject *obj, bool_t stp){
  STPStp((STPObject*)obj, stp);
}

static inline void stpEnb(BaseObject *obj, bool_t enb){
  STPEnb((STPObject*)obj, enb);
}

static inline bool_t stpMin(BaseObject *obj){
  return STPMin((STPObject*)obj);
}

static inline bool_t stpMax(BaseObject *obj){
  return STPMax((STPObject*)obj);
}

ObjectTypeExt(STPObject);

#ifdef STP_1_STP
ObjectExt(STPObject stp_1);
#define STP_1_OA objectReg(stp_1, Add);
#else
#define STP_1_OA
#endif

#ifdef STP_2_STP
ObjectExt(STPObject stp_2);
#define STP_2_OA objectReg(stp_2, Add);
#else
#define STP_2_OA
#endif

#ifdef STP_3_STP
ObjectExt(STPObject stp_3);
#define STP_3_OA objectReg(stp_3, Add);
#else
#define STP_3_OA
#endif

#ifdef STP_4_STP
ObjectExt(STPObject stp_4);
#define STP_4_OA objectReg(stp_4, Add);
#else
#define STP_4_OA
#endif

#ifdef STP_5_STP
ObjectExt(STPObject stp_5);
#define STP_5_OA objectReg(stp_5, Add);
#else
#define STP_5_OA
#endif

#define stpTypesRef() ObjectTypeRef(STPObject)
#define stpObjectsAdd() STP_1_OA STP_2_OA STP_3_OA STP_4_OA STP_5_OA

#define stpPrefix "device:stp"

#endif//__stp_obj_h__

#ifndef __adc_obj_h__
#define __adc_obj_h__ "adc_obj.h" 

typedef struct {
  const ioportid_t ptr;  /**< Input port */
  const int8_t bit;      /**< Input pad */
  const uint8_t channel; /**< analog channel number */
} ADCInConfig;

typedef struct {
  uint8_t rate; /**< Channel Conversion Rate in Cycles (0 - disabled) */
  int8_t  slot; /**< Value location in samples buffer (-1 - disabled) */
} ADCInState;

ObjectDef(ADCObject,
          ADCDriver *adc;               /**< ADC Driver */
          ADCConversionGroup *conv_grp; /**< Conversion Group */
          const unsigned n_inputs;      /**< Number of Inputs */
          const ADCInConfig *in_config; /**< ADC Input Configs */
          ADCInState *in_state;         /**< ADC Input States */
          adcsample_t *in_data);        /**< Samples Data */

#ifdef ADC_1_DEV
ObjectTypeExt(ADC1Object);
#define ADC_1_TR ObjectTypeRef(ADC1Object)
ObjectExt(ADCObject adc_1);
#define ADC_1_OA objectReg(adc_1, Add);
#else
#define ADC_1_TR
#define ADC_1_OA
#endif

#define adcTypesRef() ADC_1_TR
#define adcObjectsAdd() ADC_1_OA

#define adcPrefix "device:adc"

#endif//__adc_obj_h__

#ifndef __gpt_obj_h__
#define __gpt_obj_h__ "gpt_obj.h"

typedef void GPTFunction(void *ud);

ObjectDef(GPTObject,
          GPTDriver *gpt;
          GPTConfig cfg;
          GPTFunction *fn;
          void *ud);

void GPTInitialize(GPTObject* obj);

ObjectTypeExt(GPTObject);

#ifdef GPT_1_DEV
ObjectExt(GPTObject gpt_1);
#define GPT_1_OA GPTInitialize(&gpt_1); objectReg(gpt_1, Add);
#else
#define GPT_1_TR
#define GPT_1_OA
#endif

#define gptTypesRef() ObjectTypeRef(GPTObject)
#define gptObjectsAdd() GPT_1_OA

#define gptPrefix "device:gpt"

#define gptCallback(obj, _fn_, _ud_) ({           \
      ((GPTObject*)obj)->fn = (GPTFunction*)_fn_; \
      ((GPTObject*)obj)->ud = _ud_;               \
    })
#define gptDriver(obj) (((GPTObject*)obj)->gpt)

#define gptFreq(obj) (((GPTObject*)obj)->cfg.frequency)

#define gptGetCounter(gpt) gpt_lld_get_counter(gpt)
#define gpt_lld_get_counter(gpt) ((gpt)->tim->CNT)

#define gptGetInterval(gpt) gpt_lld_get_interval(gpt)
#define gpt_lld_get_interval(gpt) ((gpt)->tim->ARR)

#endif//__gpt_obj_h__

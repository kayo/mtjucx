#include "ch.h"
#include "hal.h"

#include "api.h"
#include "gpt_obj.h"

void GPTInitialize(GPTObject* obj){
  gptStart(obj->gpt, &obj->cfg);
  gptStop(obj->gpt);
}

static void GPTUpdate(GPTObject* obj){
  if(obj->gpt->state == GPT_READY){
    gptStop(obj->gpt);
  }
  
  if(obj->cfg.frequency > 0){
    gptStart(obj->gpt, &obj->cfg);
  }
}

static int GPTFrequencySet(GPTObject* obj, const unsigned *fhz){
  const GPTDriver *gpt = obj->gpt;
  GPTConfig *cfg = &obj->cfg;
  
  if(*fhz > gpt->clock){ /* frequency is too high */
    return paramValueError;
  }
  
  int div = (gpt->clock / *fhz) + 1; /* we need +1 to preserve period value from overflow */
  
  if(div >= 0xffff){ /* frequency is too low */
    return paramValueError;
  }
  
  if(div < 1){ /* we need to reduce period to reach required frequency */
    div = 1;
  }
  
  cfg->frequency = gpt->clock / div;
  
  GPTUpdate(obj);
  
  return paramSuccess;
}

static int GPTFrequencyGet(GPTObject* obj, unsigned *fhz){
  const GPTConfig *cfg = &obj->cfg;
  
  *fhz = cfg->frequency;
  
  return paramSuccess;
}

#define GPTObjectCreate NULL
#define GPTObjectDelete NULL

ObjectTypeDef(GPTObject, gptPrefix, "General Purpose Timer",
              ObjectParamDefRW(GPTFrequency, Uint, "freq", "Activation frequency in Hz"),
              );

static void GPTHandler(GPTDriver *gpt);

#ifdef GPT_1_DEV

ObjectBuiltin(GPTObject, GPTObject gpt_1, "gpt:1",
              &GPT_1_DEV, { 100000, GPTHandler, 0 }, NULL, NULL);

#endif//GPT_1_DEV

static const GPTObject* all[] = {
#ifdef GPT_1_DEV
  &gpt_1,
#endif
  NULL
};

static void GPTHandler(GPTDriver *gpt){
  const GPTObject **obj = all;

  for(; *obj; obj++){
    if((*obj)->gpt == gpt){
      if((*obj)->fn){
        (*obj)->fn((*obj)->ud);
      }
      break;
    }
  }
}

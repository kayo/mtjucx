#include "ch.h"
#include "hal.h"

#include "api.h"
#include "pwm_obj.h"

void PWMInitialize(PWMObject* obj){
  obj->cfg->frequency = 10000;
  obj->cfg->period = 10000;
  pwmStart(obj->pwm, obj->cfg);
  pwmStop(obj->pwm);
}

static void PWMChannelUpdate(PWMObject* obj, unsigned ch){
  const PWMChConfig *chc = &obj->ch_config[ch];
  unsigned wid = obj->ch_state[ch].wid;
  
  if(wid > 0){
    pwmEnableChannel(obj->pwm, chc->idx, PWM_PERCENTAGE_TO_WIDTH(obj->pwm, wid));
    palSetPadMode(chc->ptr, chc->bit, PAL_MODE_STM32_ALTERNATE_PUSHPULL);
  }else{
    palSetPadMode(chc->ptr, chc->bit, PAL_MODE_RESET);
    pwmDisableChannel(obj->pwm, chc->idx);
  }
}

static void PWMUpdate(PWMObject* obj){
  if(obj->pwm->state == PWM_READY){
    pwmStop(obj->pwm);
  }
  
  if(obj->cfg->frequency > 0 && obj->cfg->period > 0){
    pwmStart(obj->pwm, obj->cfg);
    
    unsigned ch = 0;
    for(; ch < obj->n_channels; ch++){
      PWMChannelUpdate(obj, ch);
    }
  }
}

static int PWMFrequencySet(PWMObject* obj, const unsigned *fhz){
  const PWMDriver *pwm = obj->pwm;
  PWMConfig *cfg = obj->cfg;
  
  if(*fhz > (pwm->clock >> 1)){ /* frequency is too high */
    return paramValueError;
  }
  
  int64_t freq = (int64_t)*fhz << 16; /* 16-bit is a timer counter depth */
  int div = (pwm->clock / freq) + 1; /* we need +1 to preserve period value from overflow */
  
  if(div >= 0xffff){ /* frequency is too low */
    return paramValueError;
  }
  
  if(div < 1){ /* we need to reduce period to reach required frequency */
    div = 1;
  }
  
  for(; pwm->clock % div; div++); /* seek to find optimal divider */
  
  cfg->frequency = pwm->clock / div;
  cfg->period = cfg->frequency / *fhz;
  
  obj->freq = *fhz;
  
  PWMUpdate(obj);
  
  return paramSuccess;
}

static int PWMFrequencyGet(PWMObject* obj, unsigned *fhz){
  *fhz = obj->freq;
  
  return paramSuccess;
}

static void PWMChannelModeSet(PWMObject *obj, unsigned ch, signed pol){
  const PWMChConfig *chc = &obj->ch_config[ch];
  
  obj->cfg->channels[chc->idx].mode = pol ? PWM_OUTPUT_ACTIVE_HIGH : PWM_OUTPUT_ACTIVE_LOW;
  
  PWMUpdate(obj);
}

static signed PWMChannelModeGet(PWMObject* obj, unsigned ch){
  const PWMChConfig *chc = &obj->ch_config[ch];
  
  return obj->cfg->channels[chc->idx].mode == PWM_OUTPUT_ACTIVE_HIGH ? 1 : 0;
}

static void PWMChannelPulseSet(PWMObject *obj, unsigned ch, unsigned wid){
  obj->ch_state[ch].wid = wid;
  
  if(obj->pwm->state == PWM_READY){
    PWMChannelUpdate(obj, ch);
  }
}

static unsigned PWMChannelPulseGet(PWMObject *obj, unsigned ch){
  return obj->ch_state[ch].wid;
}

#define PWMChannelFunctions(N)                                          \
  static int PWMChannel##N##PolarSet(PWMObject* obj, const signed *pol){ \
    PWMChannelModeSet(obj, N-1, *pol);                                  \
    return paramSuccess;                                                \
  }                                                                     \
  static int PWMChannel##N##PolarGet(PWMObject* obj, signed *pol){      \
    *pol = PWMChannelModeGet(obj, N-1);                                 \
    return paramSuccess;                                                \
  }                                                                     \
  static int PWMChannel##N##WidthSet(PWMObject* obj, const unsigned *wid){ \
    PWMChannelPulseSet(obj, N-1, *wid);                                 \
    return paramSuccess;                                                \
  }                                                                     \
  static int PWMChannel##N##WidthGet(PWMObject* obj, unsigned *wid){    \
    *wid = PWMChannelPulseGet(obj, N-1);                                \
    return paramSuccess;                                                \
  }

PWMChannelFunctions(1);
PWMChannelFunctions(2);
PWMChannelFunctions(3);
PWMChannelFunctions(4);

#define PWM_CH(c) { PORT(c##_OUT), PAD(c##_OUT), c }

#ifdef PWM_1_DEV
static PWMConfig pwm_1_config = {
  0,
  0,
  NULL,
  {
    {PWM_OUTPUT_ACTIVE_LOW, NULL},
    {PWM_OUTPUT_ACTIVE_LOW, NULL},
    {PWM_OUTPUT_ACTIVE_LOW, NULL},
    {PWM_OUTPUT_ACTIVE_LOW, NULL}
  },
  0,
  0,
#if STM32_PWM_USE_ADVANCED
  0
#endif
};

static const PWMChConfig pwm_1_ch_config[] = {
#ifdef PWM_1_CH_1
  PWM_CH(PWM_1_CH_1),
#endif
#ifdef PWM_1_CH_2
  PWM_CH(PWM_1_CH_2),
#endif
#ifdef PWM_1_CH_3
  PWM_CH(PWM_1_CH_3),
#endif
#ifdef PWM_1_CH_4
  PWM_CH(PWM_1_CH_4),
#endif
};

#define PWM_1_CH_COUNT lengthof(pwm_1_ch_config)

static PWMChState pwm_1_ch_state[PWM_1_CH_COUNT];

#define PWM1ObjectCreate NULL
#define PWM1ObjectDelete NULL

ObjectTypeDef(PWM1Object, pwmPrefix "1", "Pulse-Width Modulation output",
              ObjectParamDefRW(PWMFrequency, Uint, "freq", "Modulation frequency in Hz"),
#ifdef PWM_1_CH_1
              ObjectParamDefRW(PWMChannel1Width, Uint, "1", "Channel 1 Width (in 0.01%)"),
              ObjectParamDefRW(PWMChannel1Polar, Sint, "1:mode", "Channel 1 Mode (1-active high, 0-active low)"),
#endif
#ifdef PWM_1_CH_2
              ObjectParamDefRW(PWMChannel2Width, Uint, "2", "Channel 2 Width (in 0.01%)"),
              ObjectParamDefRW(PWMChannel2Polar, Sint, "2:mode", "Channel 2 Mode (1-active high, 0-active low)"),
#endif
#ifdef PWM_1_CH_3
              ObjectParamDefRW(PWMChannel3Width, Uint, "3", "Channel 3 Width (in 0.01%)"),
              ObjectParamDefRW(PWMChannel3Polar, Sint, "3:mode", "Channel 3 Mode (1-active high, 0-active low)"),
#endif
#ifdef PWM_1_CH_4
              ObjectParamDefRW(PWMChannel4Width, Uint, "4", "Channel 4 Width (in 0.01%)"),
              ObjectParamDefRW(PWMChannel4Polar, Sint, "4:mode", "Channel 4 Mode (1-active high, 0-active low)"),
#endif
              );

ObjectBuiltin(PWM1Object, PWMObject pwm_1, "pwm:1",
              &PWM_1_DEV, &pwm_1_config, 0, PWM_1_CH_COUNT,
              pwm_1_ch_config, pwm_1_ch_state);

#endif//PWM_1_DEV

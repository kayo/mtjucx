#ifndef __spi_obj_h__
#define __spi_obj_h__ "spi_obj.h"

ObjectDef(SPIObject,
          SPIDriver *spi;
          SPIConfig *cfg);

#ifdef SPI_1_DEV
ObjectTypeExt(SPI1Object);
#define SPI_1_TR ObjectTypeRef(SPI1Object)
ObjectExt(SPIObject spi_1);
#define SPI_1_OA objectReg(spi_1, Add);
#else
#define SPI_1_TR
#define SPI_1_OA
#endif

#define spiTypesRef() SPI_1_TR
#define spiObjectsAdd() SPI_1_OA

#define spiPrefix "device:spi"
#define spiDriver(link) (((SPIObject*)((link).obj))->spi)
#define spiConfig(link) (&(((SPIObject*)((link).obj))->cfg[(link).par]))

#endif//__spi_obj_h__

#ifndef __ctl_obj_h__
#define __ctl_obj_h__ "ctl_obj.h"

typedef struct {
  const ioportid_t ptr;   /**< port pointer */
  const int8_t bit;       /**< pad bit */
  const int8_t pol;       /**< polarity (active level) */
  const iomode_t cfg;     /**< pad config */
} CTLOutConfig;

typedef struct {
  uint8_t val;
} CTLOutState;

typedef struct {
  const ioportid_t ptr;   /**< port pointer */
  const int8_t bit;       /**< pad bit */
  const int8_t pol;       /**< polarity (active level) */
  const iomode_t cfg;     /**< pad config */
} CTLInConfig;

ObjectDef(CTLObject,
          const CTLOutConfig *out_config;
          CTLOutState *out_state;
          const uint8_t n_outs;
          const CTLInConfig *in_config;
          const uint8_t n_ins);

void CTLInit(CTLObject *obj);
void CTLDone(CTLObject *obj);

ObjectTypeExt(CTLObject);
ObjectExt(CTLObject ctl);

#define ctlTypesRef() ObjectTypeRef(CTLObject)
#define ctlObjectsAdd() CTLInit(&ctl); objectReg(ctl, Add);

#define ctlPrefix "device:ctl"

#endif//__ctl_obj_h__

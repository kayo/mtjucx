#include "ch.h"
#include "hal.h"

#include "api.h"
#include "spi_obj.h"

#ifdef SPI_1_DEV
static SPIConfig spi_1_cfg[] = {
#ifdef SPI_1_CS_1
	{
		NULL,
		PORT(SPI_1_CS_1),
		PAD(SPI_1_CS_1),
		SPI_CR1_BR_1 /* SPI frequency 2.25 Mhz (36/16) */
	},
#endif
#ifdef SPI_1_CS_2
	{
		NULL,
		PORT(SPI_1_CS_2),
		PAD(SPI_1_CS_2),
		SPI_CR1_BR_1 /* SPI frequency 2.25 Mhz (36/16) */
	},
#endif
#ifdef SPI_1_CS_3
	{
		NULL,
		PORT(SPI_1_CS_3),
		PAD(SPI_1_CS_3),
		SPI_CR1_BR_1 /* SPI frequency 2.25 Mhz (36/16) */
	},
#endif
#ifdef SPI_1_CS_4
	{
		NULL,
		PORT(SPI_1_CS_4),
		PAD(SPI_1_CS_4),
		SPI_CR1_BR_1 /* SPI frequency 2.25 Mhz (36/16) */
	},
#endif
};

#define SPI_1_CS_COUNT lengthof(spi_1_cfg)

#define SPI1ObjectCreate NULL
#define SPI1ObjectDelete NULL

ObjectTypeDef(SPI1Object, spiPrefix "1", "SPI Hardware Interface",
#ifdef SPI_1_CS_1
			  ObjectParamDefNO(SPI1ChipSelect1, Uint, "1", "Chip select 1"),
#endif
#ifdef SPI_1_CS_2
			  ObjectParamDefNO(SPI1ChipSelect2, Uint, "2", "Chip select 2"),
#endif
#ifdef SPI_1_CS_3
			  ObjectParamDefNO(SPI1ChipSelect3, Uint, "3", "Chip select 3"),
#endif
#ifdef SPI_1_CS_4
			  ObjectParamDefNO(SPI1ChipSelect4, Uint, "4", "Chip select 4"),
#endif
			  );

ObjectBuiltin(SPI1Object, SPIObject spi_1, "spi:1", &SPI_1_DEV, spi_1_cfg);
#endif//SPI_1_DEV

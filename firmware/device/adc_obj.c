#include "ch.h"
#include "hal.h"

#include "api.h"
#include "adc_obj.h"

static const unsigned adcRates[] = {
  0,    /* off */
  2,    /* 1.5 (cycles) */
  8,    /* 7.5 */
  14,   /* 13.5 */
  29,   /* 28.5 */
  42,   /* 41.5 */
  56,   /* 55.5 */
  72,   /* 71.5 */
  240,  /* 239.5 */
};

static unsigned ADCRateNorm(unsigned rate){
  unsigned i = 0;
  for(; i < lengthof(adcRates); i++){
    if(rate <= adcRates[i]){
      return adcRates[i];
    }
  }
  return adcRates[i - 1];
}

static int8_t ADCRateWord(unsigned rate){
  unsigned i = 0;
  for(; i < lengthof(adcRates); i++){
    if(rate <= adcRates[i]){
      return i - 1;
    }
  }
  return i - 1;
}

static void ADCUpdate(ADCObject *obj){
  /* stop active conversion */
  if(obj->adc->state == ADC_ACTIVE){
    adcStopConversion(obj->adc);
  }
  
  if(obj->adc->state == ADC_READY){
    adcStop(obj->adc);
  }
  
  ADCConversionGroup *cg = obj->conv_grp;
  const ADCInConfig *ic = obj->in_config;
  ADCInState *is = obj->in_state;
  unsigned i = 0; /* channel iterator */
  unsigned c = 0; /* active channel number */
  unsigned t;
  
  /* reset conversion sequence */
  cg->sqr3 = cg->sqr2 = cg->sqr1 = 0;
  
  /* setup conversion sequence */
  for(; i < obj->n_inputs; i++){
    if(is[i].rate > 0){
      t = ic[i].channel & 0b11111;
      
      if(c < 6){
        cg->sqr3 |= t << (5 * (c - 0));
      }else if(c < 12){
        cg->sqr2 |= t << (5 * (c - 6));
      }else{
        cg->sqr1 |= t << (5 * (c - 12));
      }
      
      /* reset conversion value */
      obj->in_data[c] = 0;
      
      /* setup data location */
      is[i].slot = c;
      
      c++;
    }else{
      /* reset data location */
      is[i].slot = -1;
    }
  }
  
  /* set number of active channels */
  cg->num_channels = c;
  cg->sqr1 |= ADC_SQR1_NUM_CH(c);
  
  if(c > 0){
    /* start new conversion */
    adcStart(obj->adc, NULL);
    
    if(obj->adc->state == ADC_READY){
      adcStartConversion(obj->adc, obj->conv_grp, obj->in_data, 1);
    }
  }
}

#if ADC_OBJ_EVT == TRUE
static void ADCCallback(ADCDriver *adcp, adcsample_t *buffer, size_t n){
  (void)adcp;
  (void)buffer;
  (void)n;
}

static void ADCErrorCallback(ADCDriver *adcp, adcerror_t err){
  (void)adcp;
  (void)err;
}
#else
#define ADCCallback NULL
#define ADCErrorCallback NULL
#endif

static void ADCRateSet(ADCObject *obj, unsigned ch, unsigned rate){
  obj->in_state[ch].rate = rate = ADCRateNorm(rate);
  
  ADCConversionGroup *cg = obj->conv_grp;
  const ADCInConfig *ic = &obj->in_config[ch];
  uint32_t *smprp;
  unsigned shift;

  /* calc sample time location for channel */
  if(ic->channel < 10){
    shift = 3 * ic->channel;
    smprp = &cg->smpr2;
  }else{
    shift = 3 * (ic->channel - 10);
    smprp = &cg->smpr1;
  }
  
  /* reset sample time for channel */
  *smprp &= ~(0b111 << shift);

  if(rate > 0){
    /* set sample time for channel */
    *smprp |= ADCRateWord(rate) << shift;
  }
  
  ADCUpdate(obj);
  
  if(ic->ptr != NULL){ /* Configure I/O Channel */
    palSetPadMode(ic->ptr, ic->bit, rate > 0 ? PAL_MODE_INPUT_ANALOG : PAL_MODE_RESET);
  }
}

static unsigned ADCRateGet(ADCObject *obj, unsigned ch){
  return obj->in_state[ch].rate;
}

#define ADCInFunctions(N)                                             \
  static int ADCIn##N##StateSet(ADCObject *obj, const unsigned *val){ \
    ADCRateSet(obj, N-1, *val);                                       \
    return paramSuccess;                                              \
  }                                                                   \
  static int ADCIn##N##StateGet(ADCObject *obj, unsigned *val){       \
    *val = ADCRateGet(obj, N-1);                                      \
    return paramSuccess;                                              \
  }                                                                   \
  static int ADCIn##N##ValueGet(ADCObject *obj, signed *val){         \
    ADCInState *is = &obj->in_state[N-1];                             \
    if(is->slot < 0){                                                 \
      *val = -1;                                                      \
      return paramConfigError;                                        \
    }                                                                 \
    *val = obj->in_data[is->slot];                                    \
    return paramSuccess;                                              \
  }

ADCInFunctions(1);
ADCInFunctions(2);
ADCInFunctions(3);
ADCInFunctions(4);
ADCInFunctions(5);
ADCInFunctions(6);

#define ADC_IN(a) { PORT(a##_IN), PAD(a##_IN), CAT2(ADC_CHANNEL_, a) }

#ifdef ADC_1_DEV

/* I/O */
static const ADCInConfig adc_1_in_config[] = {
#ifdef ADC_1_CH_1
  ADC_IN(ADC_1_CH_1),
#endif
#ifdef ADC_1_CH_2
  ADC_IN(ADC_1_CH_2),
#endif
#ifdef ADC_1_CH_3
  ADC_IN(ADC_1_CH_3),
#endif
#ifdef ADC_1_CH_4
  ADC_IN(ADC_1_CH_4),
#endif
#ifdef ADC_1_CH_R
  ADC_IN(ADC_1_CH_R),
#endif
#ifdef ADC_1_CH_T
  ADC_IN(ADC_1_CH_T),
#endif
};

#define ADC_1_CH_COUNT lengthof(adc_1_in_config)

/* sample data */
static adcsample_t adc_1_in_data[ADC_1_CH_COUNT] = {
  [0 ... ADC_1_CH_COUNT-1] = 0
};

static ADCConversionGroup adc_1_conv_grp = {
  TRUE,
  0,
  ADCCallback,
  ADCErrorCallback,
  0, ADC_CR2_TSVREFE, /* CR1, CR2 */
  0,      /* SMPR1 (sample times for channels 10...17) */
  0,      /* SMPR2 (sample times for channels 0...9) */
  0, 0, 0 /* SQR1, SQR2, SQR3 */
};

static ADCInState adc_1_in_state[ADC_1_CH_COUNT] = {
  [0 ... ADC_1_CH_COUNT-1] = { 0, -1 }
};

#define ADC1ObjectCreate NULL
#define ADC1ObjectDelete NULL

ObjectTypeDef(ADC1Object, adcPrefix "1", "Analog to Digital Conversion input",
#ifdef ADC_1_CH_1
              ObjectParamDefRW(ADCIn1State, Uint, "1:time", "Input 1 Sampling Time In Cycles (0-off, 240-max)"),
              ObjectParamDefRO(ADCIn1Value, Sint, "1", "Input 1 Value"),
#endif
#ifdef ADC_1_CH_2
              ObjectParamDefRW(ADCIn2State, Uint, "2:time", "Input 2 Sampling Time In Cycles (0-off, 240-max)"),
              ObjectParamDefRO(ADCIn2Value, Sint, "2", "Input 2 Value"),
#endif
#ifdef ADC_1_CH_3
              ObjectParamDefRW(ADCIn3State, Uint, "3:time", "Input 3 Sampling Time In Cycles (0-off, 240-max)"),
              ObjectParamDefRO(ADCIn3Value, Sint, "3", "Input 3 Value"),
#endif
#ifdef ADC_1_CH_4
              ObjectParamDefRW(ADCIn4State, Uint, "4:time", "Input 4 Sampling Time In Cycles (0-off, 240-max)"),
              ObjectParamDefRO(ADCIn4Value, Sint, "4", "Input 4 Value"),
#endif
#ifdef ADC_1_CH_R
              ObjectParamDefRW(ADCIn5State, Uint, "r:time", "Input Vrefint Sampling Time In Cycles (0-off, 240-max)"),
              ObjectParamDefRO(ADCIn5Value, Sint, "r", "Input Vrefint Value"),
#endif
#ifdef ADC_1_CH_T
              ObjectParamDefRW(ADCIn6State, Uint, "t:time", "Input Tsensor Sampling Time In Cycles (0-off, 240-max)"),
              ObjectParamDefRO(ADCIn6Value, Sint, "t", "Input Tsensor Value"),
#endif
              );

ObjectBuiltin(ADC1Object, ADCObject adc_1, "adc:1",
              &ADC_1_DEV, &adc_1_conv_grp, ADC_1_CH_COUNT,
              adc_1_in_config, adc_1_in_state, adc_1_in_data);

#endif//ADC_1_DEV

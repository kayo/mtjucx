#include "ch.h"
#include "hal.h"

#include "api.h"
#include "ctl_obj.h"

void CTLInit(CTLObject *obj){
  const CTLOutConfig *out_config = obj->out_config;
  const CTLOutConfig *out_config_edge = out_config + obj->n_outs;
  CTLOutState *out_state = obj->out_state;
  
  for(; out_config < out_config_edge; out_config++, out_state++){
    out_state->val = 0;
    palSetPadMode(out_config->ptr, out_config->bit, out_config->cfg);
  }

  const CTLInConfig *in_config = obj->in_config;
  const CTLInConfig *in_config_edge = in_config + obj->n_ins;
  
  for(; in_config < in_config_edge; in_config++){
    palSetPadMode(in_config->ptr, in_config->bit, in_config->cfg);
  }
}

void CTLDone(CTLObject *obj){
  const CTLOutConfig *out_config = obj->out_config;
  const CTLOutConfig *out_config_edge = out_config + obj->n_outs;
  
  for(; out_config < out_config_edge; out_config++){
    palSetPadMode(out_config->ptr, out_config->bit, PAL_MODE_RESET);
  }

  const CTLInConfig *in_config = obj->in_config;
  const CTLInConfig *in_config_edge = in_config + obj->n_ins;
  
  for(; in_config < in_config_edge; in_config++){
    palSetPadMode(in_config->ptr, in_config->bit, PAL_MODE_RESET);
  }
}

static void CTLOutSet(CTLObject *obj, unsigned out, unsigned val){
  const CTLOutConfig *out_config = obj->out_config + out;
  CTLOutState *out_state = obj->out_state + out;
  
  out_state->val = val ? 1 : 0;
  
  if(!out_config->pol){
    val = !val;
  }
  
  if(val){
    palSetPad(out_config->ptr, out_config->bit);
  }else{
    palClearPad(out_config->ptr, out_config->bit);
  }
}

static unsigned CTLOutGet(CTLObject *obj, unsigned out){
  CTLOutState *out_state = obj->out_state + out;
  
  return out_state->val;
}

static unsigned CTLInGet(CTLObject *obj, unsigned ch){
  const CTLInConfig *in_config = obj->in_config + ch;
  
  unsigned val = palReadPad(in_config->ptr, in_config->bit);
  
  if(!in_config->pol){
    val = !val;
  }
  
  return val;
}

static int CTLAtxSet(CTLObject *obj, const unsigned *val){
  CTLOutSet(obj, 0, *val);
  return paramSuccess;
}

static int CTLAtxGet(CTLObject *obj, unsigned *val){
#ifdef ATX_PWOK
  *val = CTLInGet(obj, 0);
#else
  (void)obj;
  *val = 1;
#endif
  return paramSuccess;
}

#define CTLGetSet(name, ch)                                         \
  static int CTL##name##Set(CTLObject *obj, const unsigned *val){   \
    CTLOutSet(obj, ch, *val);                                       \
    return paramSuccess;                                            \
  }                                                                 \
  static int CTL##name##Get(CTLObject *obj, unsigned *val){         \
    *val = CTLOutGet(obj, ch);                                      \
    return paramSuccess;                                            \
  }

CTLGetSet(Led2, 1);
CTLGetSet(Led3, 2);

#define CTL_OUT(p, s, m) { PORT(p), PAD(p), s, PAL_MODE_##m }

static const CTLOutConfig ctl_out_config[] = {
#ifdef ATX_PSON
  CTL_OUT(ATX_PSON, 0, OUTPUT_OPENDRAIN),
#endif
#ifdef LED_2
  CTL_OUT(LED_2, 1, OUTPUT_PUSHPULL),
#endif
#ifdef LED_3
  CTL_OUT(LED_3, 1, OUTPUT_PUSHPULL),
#endif
};

#define CTL_OUT_COUNT lengthof(ctl_out_config)

static CTLOutState ctl_out_state[CTL_OUT_COUNT];

static const CTLInConfig ctl_in_config[] = {
#ifdef ATX_PWOK
  CTL_OUT(ATX_PWOK, 0, INPUT_PULLUP),
#endif
};

#define CTL_IN_COUNT lengthof(ctl_in_config)

#define CTLObjectCreate NULL
#define CTLObjectDelete NULL

ObjectTypeDef(CTLObject, ctlPrefix, "Board Control interface",
#ifdef ATX_PSON
              ObjectParamDefRW(CTLAtx, Uint, "atx:power", "ATX Power switch (set PSON, get PWOK)"),
#endif
#ifdef LED_2
              ObjectParamDefRW(CTLLed2, Uint, "led:2", "Board LED 2"),
#endif
#ifdef LED_3
              ObjectParamDefRW(CTLLed3, Uint, "led:3", "Board LED 3"),
#endif
              );

ObjectBuiltin(CTLObject, CTLObject ctl, "ctl",
              ctl_out_config, ctl_out_state, CTL_OUT_COUNT,
              ctl_in_config, CTL_IN_COUNT);

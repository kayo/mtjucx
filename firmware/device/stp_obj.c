#include "ch.h"
#include "hal.h"

#include "api.h"
#include "stp_obj.h"

void STPDir(STPObject *obj, bool_t dir){
  const STPConfig *cfg = obj->cfg;
  
  if(dir){
    palSetPad(cfg->dir.ptr, cfg->dir.bit);
  }else{
    palClearPad(cfg->dir.ptr, cfg->dir.bit);
  }
}

void STPStp(STPObject *obj, bool_t stp){
  const STPConfig *cfg = obj->cfg;
  
  if(stp){
    palSetPad(cfg->stp.ptr, cfg->stp.bit);
  }else{
    palClearPad(cfg->stp.ptr, cfg->stp.bit);
  }
}

void STPEnb(STPObject *obj, bool_t enb){
  const STPConfig *cfg = obj->cfg;
  
  if(enb){
    palClearPad(cfg->enb.ptr, cfg->enb.bit);
  }else{
    palSetPad(cfg->enb.ptr, cfg->enb.bit);
  }
}

bool_t STPMin(const STPObject *obj){
  const STPConfig *cfg = obj->cfg;
  
  if(cfg->min.ptr && obj->min != 0){
    return palReadPad(cfg->min.ptr, cfg->min.bit) ^ (obj->min == 1 ? 0 : 1);
  }
  
  return FALSE;
}

bool_t STPMax(const STPObject *obj){
  const STPConfig *cfg = obj->cfg;
  
  if(cfg->max.ptr && obj->max != 0){
    return palReadPad(cfg->max.ptr, cfg->max.bit) ^ (obj->max == 1 ? 0 : 1);
  }
  
  return FALSE;
}

static int STPStepPerMSet(STPObject *obj, const unsigned *spm){
  obj->spm = *spm;
  return paramSuccess;
}

static int STPStepPerMGet(const STPObject *obj, unsigned *spm){
  *spm = obj->spm;
  return paramSuccess;
}

static int STPStepDivSet(STPObject *obj, const unsigned *div){
  const STPConfig *cfg = obj->cfg;
  
  if(*div != obj->div){
    if(*div != 0 && *div != 1 && *div != 2 && *div != 4 && *div != 8 && *div != 16
#if STP_USE_DRV8824 == TRUE
       && *div != 32
#endif
       ){
      return paramValueError;
    }
    
    if(*div > 0 && obj->div == 0){ /* enable motor */
      palSetPadMode(cfg->dir.ptr, cfg->dir.bit, PAL_MODE_OUTPUT_PUSHPULL);
      palSetPadMode(cfg->stp.ptr, cfg->stp.bit, PAL_MODE_OUTPUT_PUSHPULL);
      if(cfg->enb.ptr) palSetPadMode(cfg->enb.ptr, cfg->enb.bit, PAL_MODE_OUTPUT_PUSHPULL);
      
#if STP_WITH_STEP_SELECT == TRUE
      const STPPad *mss = cfg->mss;
      for(; mss < cfg->mss + lengthof(cfg->mss); mss++){
        if(mss->ptr) palSetPadMode(mss->ptr, mss->bit, PAL_MODE_OUTPUT_PUSHPULL);
      }
#endif
    }else if(*div == 0 && obj->div > 0){ /* disable motor */
      palSetPadMode(cfg->dir.ptr, cfg->dir.bit, PAL_MODE_RESET);
      palSetPadMode(cfg->stp.ptr, cfg->stp.bit, PAL_MODE_RESET);
      if(cfg->enb.ptr) palSetPadMode(cfg->enb.ptr, cfg->enb.bit, PAL_MODE_RESET);
      
#if STP_WITH_STEP_SELECT == TRUE
      const STPPad *mss = cfg->mss;
      for(; mss < cfg->mss + lengthof(cfg->mss); mss++){
        if(mss->ptr) palSetPadMode(mss->ptr, mss->bit, PAL_MODE_RESET);
      }
#endif
    }

    obj->div = *div;

    if(*div > 0){
      palSetPad(cfg->enb.ptr, cfg->enb.bit);
#if STP_WITH_STEP_SELECT == TRUE
#define STP_MSS_CLR(n) if(cfg->mss[n-1].ptr) palClearPad(cfg->mss[n-1].ptr, cfg->mss[n-1].bit)
      STP_MSS_CLR(1);
      STP_MSS_CLR(2);
      STP_MSS_CLR(3);
      
#define STP_MSS_SET(n) if(cfg->mss[n-1].ptr) palSetPad(cfg->mss[n-1].ptr, cfg->mss[n-1].bit)
      switch(*div){
      case 2:
        STP_MSS_SET(1);
        break;
      case 4:
        STP_MSS_SET(2);
        break;
      case 8:
        STP_MSS_SET(1);
        STP_MSS_SET(2);
        break;
      case 16:
#if STP_USE_A4988 == TRUE
        STP_MSS_SET(1);
        STP_MSS_SET(2);
#endif
        STP_MSS_SET(3);
        break;
#if STP_USE_DRV8824 == TRUE
      case 32:
        STP_MSS_SET(1);
        STP_MSS_SET(2);
        STP_MSS_SET(3);
#endif
      }
#endif
      /* Enable */
      palClearPad(cfg->enb.ptr, cfg->enb.bit);
    }else{
      /* Disable */
      palSetPad(cfg->enb.ptr, cfg->enb.bit);
    }
  }
  return paramSuccess;
}

static int STPStepDivGet(const STPObject *obj, unsigned *div){
  *div = obj->div;
  return paramSuccess;
}

static int STPMinEdgeSet(STPObject *obj, const signed *st){
  const STPConfig *cfg = obj->cfg;
  if(!cfg->min.ptr){
    return paramConfigError;
  }
  if(*st != obj->min){
    if(*st == 0){ /* disable */
      palSetPadMode(cfg->min.ptr, cfg->min.bit, PAL_MODE_RESET);
    }else{ /* enable */
      palSetPadMode(cfg->min.ptr, cfg->min.bit, PAL_MODE_INPUT_PULLUP);
    }
    obj->min = *st;
  }
  return paramSuccess;
}

static int STPMinEdgeGet(const STPObject *obj, signed *st){
  const STPConfig *cfg = obj->cfg;
  if(!cfg->min.ptr){
    return paramConfigError;
  }
  *st = STPMin(obj);
  return paramSuccess;
}

static int STPMaxEdgeSet(STPObject *obj, const signed *st){
  const STPConfig *cfg = obj->cfg;
  if(!cfg->max.ptr){
    return paramConfigError;
  }
  if(*st != obj->max){
    if(*st == 0){ /* disable */
      palSetPadMode(cfg->max.ptr, cfg->max.bit, PAL_MODE_RESET);
    }else{ /* enable */
      palSetPadMode(cfg->max.ptr, cfg->max.bit, PAL_MODE_INPUT_PULLUP);
    }
    obj->max = *st;
  }
  return paramSuccess;
}

static int STPMaxEdgeGet(const STPObject *obj, signed *st){
  const STPConfig *cfg = obj->cfg;
  if(!cfg->min.ptr){
    return paramConfigError;
  }
  *st = STPMax(obj);
  return paramSuccess;
}

#define STPObjectCreate NULL
#define STPObjectDelete NULL

ObjectTypeDef(STPObject, stpPrefix, "Stepper Motor Controller",
              ObjectParamDefRW(STPStepDiv, Uint, "div", "Step size (0-off, 1-full, 2-half, 4-quarter, 8-1/8, 16-1/16, 32-1/32)"),
              ObjectParamDefRW(STPStepPerM, Uint, "spm", "Steps per meter"),
              ObjectParamDefRW(STPMinEdge, Sint, "min", "Min edge activate/reached (0-off, 1-on, -1-inverse on)"),
              ObjectParamDefRW(STPMaxEdge, Sint, "max", "Max edge activate/reached (0-off, 1-on, -1-inverse on)"),
              );

#define STP_DEFAULTS 0, 300, 0, 0

#define STP_PAD(n) { PORT(n), PAD(n) }

#ifdef STP_1_STP
static const STPConfig stp_1_config = {
  STP_PAD(STP_1_DIR),
  STP_PAD(STP_1_STP),
  STP_PAD(STP_1_ENB),
#if STP_WITH_STEP_SELECT == TRUE
  {
    STP_PAD(STP_1_MS1),
    STP_PAD(STP_1_MS2),
    STP_PAD(STP_1_MS3),
  },
#endif
  STP_PAD(STP_1_MIN),
  STP_PAD(STP_1_MAX),
};

ObjectBuiltin(STPObject, STPObject stp_1, "stp:1",
              &stp_1_config, STP_DEFAULTS);
#endif

#ifdef STP_2_STP
static const STPConfig stp_2_config = {
  STP_PAD(STP_2_DIR),
  STP_PAD(STP_2_STP),
  STP_PAD(STP_2_ENB),
#if STP_WITH_STEP_SELECT == TRUE
  {
    STP_PAD(STP_2_MS1),
    STP_PAD(STP_2_MS2),
    STP_PAD(STP_2_MS3),
  },
#endif
  STP_PAD(STP_2_MIN),
  STP_PAD(STP_2_MAX),
};

ObjectBuiltin(STPObject, STPObject stp_2, "stp:2",
              &stp_2_config, STP_DEFAULTS);
#endif

#ifdef STP_3_STP
static const STPConfig stp_3_config = {
  STP_PAD(STP_3_DIR),
  STP_PAD(STP_3_STP),
  STP_PAD(STP_3_ENB),
#if STP_WITH_STEP_SELECT == TRUE
  {
    STP_PAD(STP_3_MS1),
    STP_PAD(STP_3_MS2),
    STP_PAD(STP_3_MS3),
  },
#endif
  STP_PAD(STP_3_MIN),
  STP_PAD(STP_3_MAX),
};

ObjectBuiltin(STPObject, STPObject stp_3, "stp:3",
              &stp_3_config, STP_DEFAULTS);
#endif

#ifdef STP_4_STP
static const STPConfig stp_4_config = {
  STP_PAD(STP_4_DIR),
  STP_PAD(STP_4_STP),
  STP_PAD(STP_4_ENB),
#if STP_WITH_STEP_SELECT == TRUE
  {
    STP_PAD(STP_4_MS1),
    STP_PAD(STP_4_MS2),
    STP_PAD(STP_4_MS3),
  },
#endif
  STP_PAD(STP_4_MIN),
  STP_PAD(STP_4_MAX),
};

ObjectBuiltin(STPObject, STPObject stp_4, "stp:4",
              &stp_4_config, STP_DEFAULTS);
#endif

#ifdef STP_5_STP
static const STPConfig stp_5_config = {
  STP_PAD(STP_5_DIR),
  STP_PAD(STP_5_STP),
  STP_PAD(STP_5_ENB),
#if STP_WITH_STEP_SELECT == TRUE
  {
    STP_PAD(STP_5_MS1),
    STP_PAD(STP_5_MS2),
    STP_PAD(STP_5_MS3),
  },
#endif
  STP_PAD(STP_5_MIN),
  STP_PAD(STP_5_MAX),
};

ObjectBuiltin(STPObject, STPObject stp_5, "stp:5",
              &stp_5_config, STP_DEFAULTS);
#endif

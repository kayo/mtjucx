#include "stddef.h"

#include "genlist.h"

void GenListAdd(GenList **list, GenList *node){
  GenList **next = list;
  
  for(; *next; next = &(*next)->next); /* seek to end of list */
  
  *next = node;
  node->next = NULL;
}

void GenListDel(GenList **list, GenList *node){
  GenList **next = list;
  
  for(; *next; next = &(*next)->next){ /* iterate through list */
    if(*next == node){
      *next = node->next;
      node->next = NULL;
      break;
    }
  }
}

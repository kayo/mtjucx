#include "ch.h"
#include "hal.h"
#include "shell.h"
#include "chprintf.h"

#include <math.h>
#include <string.h>

#define NOINLINE __attribute__((noinline))

#define BenchObject TimeMeasurement

#define bench_start(b)    \
  tmObjectInit(&b);       \
  chSysLock();            \
  tmStartMeasurement(&b);

#define bench_stop(b)     \
  tmStopMeasurement(&b);  \
  chSysUnlock();

#define bench_report(b, t, a...)                                    \
  chprintf(chp, "[%u] " t "\r\n", b.last, ##a);

#define bench_report_range(b, t, a...)                              \
  chprintf(chp, "[%u...%u] " t "\r\n",                              \
           b.worst, b.best != (unsigned)-1 ? b.best : b.last, ##a);

static void NOINLINE bench_float_(BaseSequentialStream *chp, float *f){
  BenchObject b;
  
  long n;
  
  bench_start(b);
  f[0] = f[1] * f[2];
  bench_stop(b);
  bench_report(b, "Float mul");
  
  bench_start(b);
  f[0] = f[1] + f[2];
  bench_stop(b);
  bench_report(b, "Float add");
  
  bench_start(b);
  f[0] = -f[1];
  bench_stop(b);
  bench_report(b, "Float neg");
  
  bench_start(b);
  f[0] = f[2] / f[3];
  bench_stop(b);
  bench_report(b, "Float div");
  
  f[0] = f[1] + f[4];
  bench_start(b);
  n = f[0];
  bench_stop(b);
  bench_report(b, "Float to long");
  
  n += 2;
  bench_start(b);
  f[0] = n;
  bench_stop(b);
  bench_report(b, "Long to float");
}

static void bench_float(BaseSequentialStream *chp){
  float f[5] = {
    0.f,
    M_PI,
    M_E,
    .1f,
    2.f,
  };
  
  bench_float_(chp, f);
}

static void NOINLINE bench_signed_(BaseSequentialStream *chp, int *n){
  BenchObject b;
  
  bench_start(b);
  n[0] = n[1] * n[2];
  bench_stop(b);
  bench_report(b, "Signed mul");
  
  bench_start(b);
  n[0] = n[1] + n[2];
  bench_stop(b);
  bench_report(b, "Signed add");
  
  bench_start(b);
  n[0] = -n[1];
  bench_stop(b);
  bench_report(b, "Signed neg");
  
  bench_start(b);
  n[0] = n[2] / n[3];
  bench_stop(b);
  bench_report(b, "Signed div");
}

static void bench_signed(BaseSequentialStream *chp){
  int n[4] = {
    0,
    300,
    200,
    11,
  };
  
  bench_signed_(chp, n);
}

#include "strconv.h"

static void bench_strconv(BaseSequentialStream *chp){
  BenchObject b;
  char r[16];
  const char *c;

  signed i;
  unsigned u;
  float f;

#define test_stox_and_xtos(s, x)                                        \
  bench_start(b);                                                       \
  c = sto##x(s, &x);                                                    \
  bench_stop(b);                                                        \
  bench_report(b, "sto" #x "(" s ") c(%s) x:%" #x, c, x);               \
  bench_start(b);                                                       \
  c = x##tos(r, x);                                                     \
  bench_stop(b);                                                        \
  bench_report(b, #x "tos(" s ") c(%s) r(%s)", c, r);

  test_stox_and_xtos("12345678", i);
  test_stox_and_xtos("-12345678", i);
  test_stox_and_xtos("12345678", u);
  test_stox_and_xtos("12345.678", f);
  test_stox_and_xtos("-12345.678", f);
}

#include "decimal.h"

static void bench_decimal(BaseSequentialStream *chp){
  BenchObject b;
  char r[16];
  const char *c;

  chprintf(chp, "sizeof decimal: %u\r\n", sizeof(decimal));
  
  decimal d;
  signed i;
  unsigned u;
  float f;

#define test_stod_and_dtos(a)                                           \
  bench_start(b);                                                       \
  c = stod(a, &d);                                                      \
  bench_stop(b);                                                        \
  bench_report(b, "stod(" a ") c(%s) m:%u e:%u s:%u", c, d.m, d.e, d.s); \
  bench_start(b);                                                       \
  c = dtos(r, d);                                                       \
  bench_stop(b);                                                        \
  bench_report(b, "dtos(" a ") c(%s) r(%s)", c, r);
  
  test_stod_and_dtos("12345678");
  test_stod_and_dtos("-12345678");
  
  test_stod_and_dtos("12345.678");
  test_stod_and_dtos("-12345.678");
  
  test_stod_and_dtos("12345.000");
  
  test_stod_and_dtos("0.7654321");
  test_stod_and_dtos("-.7654321");

  test_stod_and_dtos("0.0007");
  test_stod_and_dtos("-.005");

  test_stod_and_dtos("100");
  test_stod_and_dtos("-1.0001");
  
#define test_xtod_and_dtox(x, v)                                        \
  x = v;                                                                \
  bench_start(b);                                                       \
  d = x##tod(x);                                                        \
  bench_stop(b);                                                        \
  bench_report(b, #x "tod(" #v ") m:%u e:%u s:%u", d.m, d.e, d.s);  \
  bench_start(b);                                                       \
  x = dto##x(d);                                                        \
  bench_stop(b);                                                        \
  bench_report(b, "dto" #x "(%" #x ")", x);
  
  test_xtod_and_dtox(i, 1234);
  test_xtod_and_dtox(i, -1234);
  
  test_xtod_and_dtox(u, 56789);
  
  test_xtod_and_dtox(f, 1234.5678);
  test_xtod_and_dtox(f, -1234.5678);
  
  test_xtod_and_dtox(f, -.5678);
  test_xtod_and_dtox(f, 12345.00);

  test_xtod_and_dtox(f, -.005);
  test_xtod_and_dtox(f, 1000.00);
  
  test_xtod_and_dtox(f, 0.0000001);
  test_xtod_and_dtox(f, -0.0000001);
}

/* Main, run the benchmarks.  */
shellCommandDefine(bench){
  shellStdio(chp);

  if(argc < 1){
    chprintf(chp,
             "Usage: bench <benchmark1>[ ... <benchmarkN>]\r\n"
             "  Available benchmarks: float signed strconv decimal\r\n");
    return -1;
  }
  
  int argn = 0;
  
  for(; argn < argc; argn++){
    if(strcmp(argv[argn], "float") == 0){
      bench_float(chp);
      continue;
    }
    if(strcmp(argv[argn], "signed") == 0){
      bench_signed(chp);
      continue;
    }
    if(strcmp(argv[argn], "strconv") == 0){
      bench_strconv(chp);
      continue;
    }
    if(strcmp(argv[argn], "decimal") == 0){
      bench_decimal(chp);
      continue;
    }
    chprintf(chp, "Benchmark %s is not available.\r\n", argv[argn]);
  }
  
  return 0;
}

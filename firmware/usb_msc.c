#include "ch.h"
#include "hal.h"

#include "usb_msc.h"

static inline uint32_t swap_uint32(uint32_t val) {
  val = ((val << 8) & 0xFF00FF00 ) | ((val >> 8) & 0x00FF00FF);
  return ((val << 16) & 0xFFFF0000) | ((val >> 16) & 0x0000FFFF);
}

static inline uint16_t swap_uint16(uint16_t val) {
  return ((val >> 8) & 0xff) | ((val & 0xff) << 8);
}

/**
 * @brief   Default requests hook.
 *
 * @param[in] usbp      pointer to the @p USBDriver object
 * @return              The hook status.
 * @retval TRUE         Message handled internally.
 * @retval FALSE        Message not handled.
 */
bool_t msuRequestsHook(USBDriver *usbp) {
  if (((usbp->setup[0] & USB_RTYPE_TYPE_MASK) == USB_RTYPE_TYPE_CLASS) &&
      ((usbp->setup[0] & USB_RTYPE_RECIPIENT_MASK) == USB_RTYPE_RECIPIENT_INTERFACE)) {
    /* check that the request is for interface 0.*/
    if(MSU_SETUP_INDEX(usbp->setup) != 0)
      return FALSE;
    
    /* act depending on bRequest = setup[1] */
    switch(usbp->setup[1]) {
    case MSU_REQ_RESET:
      /* check that it is a HOST2DEV request */
      if(((usbp->setup[0] & USB_RTYPE_DIR_MASK) != USB_RTYPE_DIR_HOST2DEV) ||
         (MSU_SETUP_LENGTH(usbp->setup) != 0) ||
         (MSU_SETUP_VALUE(usbp->setup) != 0))
        return FALSE;
      
      /* reset all endpoints */
      /* TODO!*/
      /* The device shall NAK the status stage of the device request until
       * the Bulk-Only Mass Storage Reset is complete.
       */
      return TRUE;
    case MSU_GET_MAX_LUN:
      /* check that it is a DEV2HOST request */
      if(((usbp->setup[0] & USB_RTYPE_DIR_MASK) != USB_RTYPE_DIR_DEV2HOST) ||
         (MSU_SETUP_LENGTH(usbp->setup) != 1) ||
         (MSU_SETUP_VALUE(usbp->setup) != 0))
        return FALSE;
      
      static uint8_t len_buf[1] = {0};
      /* stall to indicate that we don't support LUN */
      usbSetupTransfer(usbp, len_buf, 1, NULL);
      return TRUE;
    }
  }
  return FALSE;
}

void msuDataTransfered(USBDriver *usbp, usbep_t ep) {
  MassStorageUSBDriver *msup = usbp->in_params[ep - 1];
  
  chSysLockFromIsr();
  chBSemSignalI(&msup->sem);
  chSysUnlockFromIsr();
}

/**
 * @brief   USB device configured handler.
 *
 * @param[in] mcup      pointer to a @p MassStorageUSBDriver object
 *
 * @iclass
 */
void msuConfigureHookI(MassStorageUSBDriver *msup){
  /* initialise the thread */
  chBSemSignalI(&msup->sem);
  
  /* signal that the device is connected */
  chEvtBroadcastI(&msup->connected);
}

bool_t msuDeviceLoad(MassStorageUSBDriver *msup){
  const MassStorageUSBConfig *cfgp = msup->config;
  
  if(blkGetDriverState(cfgp->bbdp) == BLK_READY){
    //chSysLock();
    blkGetInfo(cfgp->bbdp, &msup->bdi);
    
    msup->capacity.blockSize = swap_uint32(msup->bdi.blk_size);
    msup->capacity.lastBlockAddr = swap_uint32(msup->bdi.blk_num-1);
    //chSysUnlock();
    
    chEvtBroadcast(&msup->loaded);
    
    return TRUE;
  }
  
  return FALSE;
}

static void msuDeviceEject(MassStorageUSBDriver *msup){
  msup->bdi.blk_size = 0;
  msup->bdi.blk_num = 0;
  
  msup->capacity.blockSize = swap_uint32(msup->bdi.blk_size);
  msup->capacity.lastBlockAddr = swap_uint32(msup->bdi.blk_num-1);
  
  chEvtBroadcast(&msup->ejected);
}

static bool_t msuDeviceReady(MassStorageUSBDriver *msup){
  return msuDeviceLoaded(msup) &&
    blkGetDriverState(msup->config->bbdp) == BLK_READY;
}

static inline void msuWaitForTransfer(MassStorageUSBDriver *msup) {
  chBSemWait(&msup->sem);
}

#define msuWaitForTransmit msuWaitForTransfer
#define msuWaitForReceive msuWaitForTransfer

static void msuSendStatus(MassStorageUSBDriver *msup, uint8_t status){
  const MassStorageUSBConfig *cfgp = msup->config;
  const msu_cbw_t *cbw = &msup->cbw;
  msu_csw_t *csw = &msup->csw;
  
  csw->signature = MSU_CSW_SIGNATURE;
  csw->tag = cbw->tag;
  
  csw->status = status;
  csw->data_residue = status == MSU_COMMAND_PASSED ? 0 : cbw->data_len;
  
  usbPrepareTransmit(cfgp->usbp, cfgp->bulk_in, (uint8_t*)csw, sizeof(msu_csw_t));
  
  chSysLock();
  usbStartTransmitI(cfgp->usbp, cfgp->bulk_in);
  chSysUnlock();
  
  msuWaitForTransmit(msup);
}

static void msuStallInOut(MassStorageUSBDriver *msup, bool_t in, bool_t out){
  const MassStorageUSBConfig *cfgp = msup->config;
  
  chSysLock();
  /* stall IN endpoint */
  if(in) usbStallTransmitI(cfgp->usbp, cfgp->bulk_in);
  /* stall OUT endpoint */
  if(out) usbStallReceiveI(cfgp->usbp, cfgp->bulk_out);
  chSysUnlock();
}

static inline void msuSetSense(MassStorageUSBDriver *msup, uint8_t key, uint8_t acode, uint8_t aqual) {
  msup->sense.SenseKey = key;
  msup->sense.ASC = acode;
  msup->sense.ASCQ = aqual;
}

/* update sense with success status */
static void msuSetSenseNone(MassStorageUSBDriver *msup){
  msuSetSense(msup,
              SCSI_SENSE_KEY_NO_SENSE,
              SCSI_ASENSE_NO_ADDITIONAL_INFORMATION,
              SCSI_ASENSEQ_NO_QUALIFIER);
}

static void msuSetSenseEjected(MassStorageUSBDriver *msup){
  msuSetSense(msup,
              SCSI_SENSE_KEY_NOT_READY/*UNIT_ATTENTION*/,
              SCSI_ASENSE_MEDIUM_NOT_PRESENT,
              SCSI_ASENSEQ_NO_QUALIFIER);
}

static void SCSICommandInquiry(MassStorageUSBDriver *msup) {
  const MassStorageUSBConfig *cfgp = msup->config;
  msu_cbw_t *cbw = &msup->cbw;
  
  if((cbw->inquiry.obsolete & (SCSI_INQUIRY_REQUEST_CMDDT | SCSI_INQUIRY_REQUEST_EVPD)) ||
     cbw->inquiry.pageCode) {
    /* Optional but unsupported bits set - update the SENSE key and fail the request */
    msuSetSense(msup,
                 SCSI_SENSE_KEY_ILLEGAL_REQUEST,
                 SCSI_ASENSE_INVALID_FIELD_IN_CDB,
                 SCSI_ASENSEQ_NO_QUALIFIER);
    
    msuSendStatus(msup, MSU_COMMAND_FAILED);
    
    return;
  }
  
  usbPrepareTransmit(cfgp->usbp, cfgp->bulk_in, cfgp->inquiry.byte, sizeof(cfgp->inquiry));
  
  chSysLock();
  usbStartTransmitI(cfgp->usbp, cfgp->bulk_in);
  chSysUnlock();
  
  msuWaitForTransmit(msup);
  
  msuSetSenseNone(msup);
  
  msuSendStatus(msup, MSU_COMMAND_PASSED);
}

static void SCSICommandRequestSense(MassStorageUSBDriver *msup) {
  const MassStorageUSBConfig *cfgp = msup->config;
  
  usbPrepareTransmit(cfgp->usbp, cfgp->bulk_in, msup->sense.byte, sizeof(msup->sense.byte));
  
  chSysLock();
  usbStartTransmitI(cfgp->usbp, cfgp->bulk_in);
  chSysUnlock();
  
  msuWaitForTransmit(msup);

  msuSetSenseNone(msup);
  
  msuSendStatus(msup, MSU_COMMAND_PASSED);
}

static void SCSICommandReadCapacity10(MassStorageUSBDriver *msup) {
  const MassStorageUSBConfig *cfgp = msup->config;
  
  if(!msuDeviceReady(msup)){
    msuSetSenseEjected(msup);
    
    msuSendStatus(msup, MSU_COMMAND_FAILED);
    
    return;
  }
  
  usbPrepareTransmit(cfgp->usbp, cfgp->bulk_in, msup->capacity.byte, sizeof(msup->capacity));
  
  chSysLock();
  usbStartTransmitI(cfgp->usbp, cfgp->bulk_in);
  chSysUnlock();
  
  msuWaitForTransmit(msup);
  
  msuSetSenseNone(msup);
  
  msuSendStatus(msup, MSU_COMMAND_PASSED);
}

static void SCSICommandSendDiagnostic(MassStorageUSBDriver *msup) {
  msu_cbw_t *cbw = &msup->cbw;
  
  if(!(cbw->diag.obsolete & SCSI_DIAGNOSTIC_REQUEST_SENFTEST)){
    /* Only self-test supported - update SENSE key and fail the command */
    msuSetSense(msup,
                SCSI_SENSE_KEY_ILLEGAL_REQUEST,
                SCSI_ASENSE_INVALID_FIELD_IN_CDB,
                SCSI_ASENSEQ_NO_QUALIFIER);
    
    msuSendStatus(msup, MSU_COMMAND_FAILED);
    
    return;
  }
  
  /* TODO: actually perform the test */
  msuSendStatus(msup, MSU_COMMAND_PASSED);
}

typedef struct {
  uint32_t blockAddr;
  uint16_t trLength;
} msu_rws_t;

static bool_t msuPrepareDataTransfer(MassStorageUSBDriver *msup, msu_rws_t *rwsp){
  const msu_cbw_t *cbw = &msup->cbw;
  
  rwsp->blockAddr = swap_uint32(cbw->rw10.blockAddr);
  rwsp->trLength = swap_uint16(cbw->rw10.trLength);
  
  if(rwsp->blockAddr + rwsp->trLength > msup->bdi.blk_num) {
    /* Block address is invalid, update SENSE key and return command fail */
    msuSetSense(msup,
                SCSI_SENSE_KEY_ILLEGAL_REQUEST,
                SCSI_ASENSE_LOGICAL_BLOCK_ADDRESS_OUT_OF_RANGE,
                SCSI_ASENSEQ_NO_QUALIFIER);
    
    msuStallInOut(msup, TRUE, TRUE);
    msuSendStatus(msup, MSU_COMMAND_FAILED);
    
    return FALSE;
  }
  
  return TRUE;
}

static void msuFinalizeDataTransfer(MassStorageUSBDriver *msup, bool_t complete){
  if(complete){
    msuSetSenseNone(msup);
    
    msuSendStatus(msup, MSU_COMMAND_PASSED);
  }else{
    /* Read/Write error reached, update SENSE key and return command fail */
    msuSetSense(msup,
                SCSI_SENSE_KEY_MEDIUM_ERROR,
                SCSI_ASENSE_NO_ADDITIONAL_INFORMATION,
                SCSI_ASENSEQ_NO_QUALIFIER);
    
    msuStallInOut(msup, TRUE, TRUE);
    msuSendStatus(msup, MSU_COMMAND_FAILED);
  }
}

static void msuCompleteDataTransfer(MassStorageUSBDriver *msup){
  (void)msup;
}

static void SCSICommandWrite10(MassStorageUSBDriver *msup){
  const MassStorageUSBConfig *cfgp = msup->config;
  
  if(blkIsWriteProtected(cfgp->bbdp)) {
    /* device is write protected and a write has been issued */
    msuSetSense(msup,
              SCSI_SENSE_KEY_DATA_PROTECT,
              SCSI_ASENSE_WRITE_PROTECTED,
              SCSI_ASENSEQ_NO_QUALIFIER);
    
    msuStallInOut(msup, TRUE, TRUE);
    msuSendStatus(msup, MSU_COMMAND_FAILED);
    
    return;
  }
  
  msu_rws_t ws;
  int error = 0;
  int i = 0;
  
  if(msuPrepareDataTransfer(msup, &ws)){
    /* loop over each block */
    for(; i <= ws.trLength && !error; i++) {
      if(i < ws.trLength){
        /* receive the data packet */
        usbPrepareReceive(cfgp->usbp, cfgp->bulk_out, cfgp->rw_buf[i % 2], msup->bdi.blk_size);
        
        chSysLock();
        usbStartReceiveI(cfgp->usbp, cfgp->bulk_out);
        chSysUnlock();
      }
      
      if(i > 0){
        /* now write last received block to the block device */
        if(blkWrite(cfgp->bbdp, ws.blockAddr++, cfgp->rw_buf[(i - 1) % 2], 1) == CH_FAILED) {
          error ++;
        }
      }
      
      if(i < ws.trLength) {
        /* now wait for the USB event to complete */
        msuWaitForReceive(msup);
      }
    }
    
    msuFinalizeDataTransfer(msup, !error);
  }
  
  msuCompleteDataTransfer(msup);
}

static void SCSICommandRead10(MassStorageUSBDriver *msup) {
  const MassStorageUSBConfig *cfgp = msup->config;
  
  msu_rws_t rs;
  int error = 0;
  int i = 0;
  
  if(msuPrepareDataTransfer(msup, &rs)){
    /* loop over each block */
    for(; i <= rs.trLength && !error; i++) {
      if(i > 0){
        /* transmit the last readed block */
        usbPrepareTransmit(cfgp->usbp, cfgp->bulk_in, cfgp->rw_buf[(i - 1) % 2], msup->bdi.blk_size);
        
        chSysLock();
        usbStartTransmitI(cfgp->usbp, cfgp->bulk_in);
        chSysUnlock();
      }
      
      if(i < rs.trLength){
        /* read the block from block device */
        if(blkRead(cfgp->bbdp, rs.blockAddr++, cfgp->rw_buf[i % 2], 1) == CH_FAILED) {
          error ++;
        }
      }
      
      if(i > 0){
        /* wait for block transmitting */
        msuWaitForTransmit(msup);
      }
    }
    
    msuFinalizeDataTransfer(msup, !error);
  }
  
  msuCompleteDataTransfer(msup);
}

static void SCSICommandTestUnitReady(MassStorageUSBDriver *msup) {
  if(msuDeviceReady(msup)){
    msuSetSenseNone(msup);
    
    msuSendStatus(msup, MSU_COMMAND_PASSED);
  }else{
    msuSetSenseEjected(msup);
    
    msuSendStatus(msup, MSU_COMMAND_FAILED);
  }
}

static void SCSICommandStartStopUnit(MassStorageUSBDriver *msup) {
  const msu_cbw_t *cbw = &msup->cbw;
  
  if(cbw->ssu.loejStart & SCSI_SSU_REQUEST_LOEJ){
    if(cbw->ssu.loejStart & SCSI_SSU_REQUEST_START) {
      /* media has been loaded */
      //msuDeviceLoad(msup);
    }else{
      /* media has been ejected */
      msuDeviceEject(msup);
    }
  }
  
  msuSetSenseNone(msup);
  
  msuSendStatus(msup, MSU_COMMAND_PASSED);
}

static void SCSICommandModeSense6(MassStorageUSBDriver *msup) {
  const MassStorageUSBConfig *cfgp = msup->config;
  
  /* Send an empty header response with the Write Protect flag status */
  msup->mode_sense.writeProtect = blkIsWriteProtected(cfgp->bbdp) ? 0x80 : 0x00;
  
  usbPrepareTransmit(cfgp->usbp, cfgp->bulk_in, msup->mode_sense.byte, sizeof(msup->mode_sense));
  
  chSysLock();
  usbStartTransmitI(cfgp->usbp, cfgp->bulk_in);
  chSysUnlock();
  
  msuWaitForTransmit(msup);
  
  msuSetSenseNone(msup);
  
  msuSendStatus(msup, MSU_COMMAND_PASSED);
}

static void msuWaitForCommandBlock(MassStorageUSBDriver *msup) {
  const MassStorageUSBConfig *cfgp = msup->config;
  
  usbPrepareReceive(cfgp->usbp, cfgp->bulk_out,
                    (uint8_t*)&msup->cbw, sizeof(msu_cbw_t));
  
  chSysLock();
  usbStartReceiveI(cfgp->usbp, cfgp->bulk_out);
  chSysUnlock();
  
  msuWaitForReceive(msup);
}

/* A command block has been received */
static void msuExecuteCommandBlock(MassStorageUSBDriver *msup) {
  const msu_cbw_t *cbw = &msup->cbw;
  
  /* check the command */
  if((cbw->signature != MSU_CBW_SIGNATURE) ||
     (cbw->lun > 0) ||
     ((cbw->data_len > 0) && (cbw->flags & 0x1F)) ||
     (cbw->cmdLength == 0) ||
     (cbw->cmdLength > 16)) {
    
    msuStallInOut(msup, TRUE, TRUE);
    
    return;
  }
  
  switch(cbw->command.opCode) {
  case SCSI_CMD_INQUIRY:
    SCSICommandInquiry(msup);
    break;
  case SCSI_CMD_REQUEST_SENSE:
    SCSICommandRequestSense(msup);
    break;
  case SCSI_CMD_READ_CAPACITY_10:
    SCSICommandReadCapacity10(msup);
    break;
  case SCSI_CMD_READ_10:
    SCSICommandRead10(msup);
    break;
  case SCSI_CMD_WRITE_10:
    SCSICommandWrite10(msup);
    break;
  case SCSI_CMD_SEND_DIAGNOSTIC:
    SCSICommandSendDiagnostic(msup);
    break;
  case SCSI_CMD_TEST_UNIT_READY:
    SCSICommandTestUnitReady(msup);
    break;
  case SCSI_CMD_PREVENT_ALLOW_MEDIUM_REMOVAL:
  case SCSI_CMD_VERIFY_10:
    /* don't handle */
    msuSetSenseNone(msup);
    msuSendStatus(msup, MSU_COMMAND_PASSED);
    break;
  case SCSI_CMD_MODE_SENSE_6:
    SCSICommandModeSense6(msup);
    break;
  case SCSI_CMD_START_STOP_UNIT:
    SCSICommandStartStopUnit(msup);
    break;
  default:
    msuSetSense(msup,
                 SCSI_SENSE_KEY_ILLEGAL_REQUEST,
                 SCSI_ASENSE_INVALID_COMMAND,
                 SCSI_ASENSEQ_NO_QUALIFIER);
    
    msuSendStatus(msup, MSU_COMMAND_FAILED);
  }
}

static msg_t msuThread(void *arg) {
  MassStorageUSBDriver *msup = (MassStorageUSBDriver *)arg;
  const MassStorageUSBConfig *cfgp = msup->config;
  
  chRegSetThreadName(cfgp->tname ? cfgp->tname : "USB-MSC");
  
  /* wait for the usb to be initialised */
  msuWaitForTransfer(msup);
  
  for (; !chThdShouldTerminate(); ) {
    msuWaitForCommandBlock(msup);
    msuExecuteCommandBlock(msup);
  }
  
  return 0;
}

/**
 * @brief   Initializes a generic mass storage driver object.
 *
 * @param[out] msup     pointer to a @p MassStorageUSBDriver structure
 *
 * @init
 */
void msuObjectInit(MassStorageUSBDriver *msup) {
  chEvtInit(&msup->connected);
  chEvtInit(&msup->ejected);
  chEvtInit(&msup->loaded);
  
  /* initialise binary semaphore as taken */
  chBSemInit(&msup->sem, TRUE);
  
  msup->thread = NULL;
}

/**
 * @brief   Configures and starts the driver.
 *
 * @param[in] sdup      pointer to a @p SerialUSBDriver object
 * @param[in] config    the serial over USB driver configuration
 * 
 * @api
 */
void msuStart(MassStorageUSBDriver *msup, const MassStorageUSBConfig *cfgp){
  uint8_t i;
  
  chDbgCheck(msup != NULL, "msuStart");
  
  msup->config = cfgp;
  
  msup->bdi.blk_size = 0;
  msup->bdi.blk_num = 0;
  
  /* initialise sense values to zero */
  for(i = 0; i < sizeof(msup->sense); msup->sense.byte[i++] = 0x00);
  
  /* Response code = 0x70, additional sense length = 0x0A */
  msup->sense.ResponseCode = 0x70;
  msup->sense.AddSenseLen = 0x0A;
  
  msup->mode_sense.modeDataLen = 3;
  msup->mode_sense.mediumType = 0;
  msup->mode_sense.writeProtect = 0;
  msup->mode_sense.blockDescLen = 0;
  
  cfgp->usbp->in_params[cfgp->bulk_in - 1] = msup;
  cfgp->usbp->in_params[cfgp->bulk_out - 1] = msup;
  
  if(msup->thread == NULL) {
    msup->thread = chThdCreateStatic(cfgp->twsp, cfgp->twas,
                                     cfgp->tprio, msuThread, msup);
  }
}

/**
 * @brief   Stops the driver.
 * @details Any thread waiting on the driver's queues will be awakened with
 *          the message @p Q_RESET.
 *
 * @param[in] msup      pointer to a @p MassStorageUSBDriver object
 *
 * @api
 */
void msuStop(MassStorageUSBDriver *msup) {
  chDbgCheck(msup != NULL, "msuStop");
  
  const MassStorageUSBConfig *cfgp = msup->config;
  
  if(msup->thread){
    chThdTerminate(msup->thread);
  }
  
  /* Driver in stopped state.*/
  cfgp->usbp->in_params[cfgp->bulk_in - 1]   = NULL;
  cfgp->usbp->out_params[cfgp->bulk_out - 1] = NULL;
}

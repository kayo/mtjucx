#ifndef __usb_msc_def_h__
#define __usb_msc_def_h__ "usb_msc_def.h"

#define USB_EP_MODE_TRANSACTION 0x0000

#if !defined(USB_MSC_DATA_PACKET_SIZE)
#define USB_MSC_DATA_PACKET_SIZE 0x40
#endif

#define USB_MSC_INTERFACE(ifData, idData, epDataIn, epDataOut)          \
    /* Interface Descriptor.*/                                          \
    USB_DESC_INTERFACE(ifData,         /* bInterfaceNumber */           \
                       0x00,           /* bAlternateSetting */          \
                       0x02,           /* bNumEndpoints */              \
                       0x08,           /* bInterfaceClass (Mass Stprage)\
                                        */                              \
                       0x06,           /* bInterfaceSubClass (SCSI      \
                                          transparent command set, MSCO \
                                          chapter 2) */                 \
                       0x50,           /* bInterfaceProtocol (Bulk-Only \
                                          Mass Storage, MSCO chapter 3) \
                                       */                               \
                       idData),        /* iInterface */                 \
    /* Endpoint 1 Descriptor */                                         \
    USB_DESC_ENDPOINT(epDataIn|0x80,   /* bEndpointAddress */           \
                      0x02,            /* bmAttributes (Bulk) */        \
                      USB_MSC_DATA_PACKET_SIZE, /* wMaxPacketSize */    \
                      0x00),           /* bInterval (ignored for bulk)  \
                                        */                              \
    /* Endpoint 2 Descriptor */                                         \
    USB_DESC_ENDPOINT(epDataOut,       /* bEndpointAddress */           \
                      0x02,            /* bmAttributes (Bulk) */        \
                      USB_MSC_DATA_PACKET_SIZE, /* wMaxPacketSize */    \
                      0x00)            /* bInterval (ignored for bulk) */

#define USB_MSC_INTERFACE_LENGTH(count) ((count)*(      \
  9/* Interface Descriptor (Mass Storage) */+           \
  7/* Data Input Endpoint */+                           \
  7/* Data Output Endpoint */))

#define USB_MSC_ENDPOINTS(prefix)                                   \
    static USBInEndpointState prefix##_ep_data_in_state;            \
    static USBOutEndpointState prefix##_ep_data_out_state;          \
    static const USBEndpointConfig prefix##_ep_data_config = {      \
      USB_EP_MODE_TYPE_BULK,                                        \
      NULL,                                                         \
      msuDataTransmitted,                                           \
      msuDataReceived,                                              \
      USB_MSC_DATA_PACKET_SIZE,                                     \
      USB_MSC_DATA_PACKET_SIZE,                                     \
      &prefix ## _ep_data_in_state,                                 \
      &prefix ## _ep_data_out_state,                                \
      1,                                                            \
      NULL,                                                         \
    };

#define USB_MSC_CFG(prefix) &prefix##_ep_data_config

#endif//__usb_msc_def_h__

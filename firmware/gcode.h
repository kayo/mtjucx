#ifndef __gcode_h__
#define __gcode_h__ "gcode.h"

#include "decimal.h"
#include "units.h"

typedef enum {
  gcodeSuccess,       /**< success */
  gcodeContinue,      /**< incomplete command */
  gcodeMissing,       /**< field missing */
  gcodeOverflowError, /**< too long command */
  gcodeTransferError, /**< check sum mismatch */
  gcodeParseError,    /**< parse error */
  gcodeCommandError,  /**< command error */
} GCodeState;

typedef struct {
  char  code;
  decimal data;
} GCodeField;

typedef struct {
  GCodeField gcfa[8]; /**< captured command fields */
  unsigned gcfn;      /**< command fields count */
  
  char csum;          /**< calculated check sum */
  unsigned lnum;      /**< command line number */
} GCodeEntry;

typedef struct {
  GCodeEntry *gcep;
  
  char astr[16];      /**< accum string */
  unsigned alen;      /**< accum length */
  
  char seek;
  
  char csum;
  unsigned lnum;
} GCodeObject;

GCodeField *gcodeFieldFind(GCodeEntry *gcep, char code);

#define gcodeFieldGet(gcep, code, type, data) ({      \
      GCodeField *gcfp = gcodeFieldFind(gcep, code);  \
      GCodeState gcst = gcodeSuccess;                 \
      if(gcfp){                                       \
        *(data) = gcodeFieldVal(gcfp, type);          \
      }else{                                          \
        gcst = gcodeMissing;                          \
      }                                               \
      gcst;                                           \
    })

#define gcodeFieldGetConv(gcep, code, from, to, type, data) ({  \
      GCodeField *gcfp = gcodeFieldFind(gcep, code);            \
      GCodeState gcst = gcodeSuccess;                           \
      if(gcfp){                                                 \
        *(data) = gcodeFieldValConv(gcfp, from, to, type);      \
      }else{                                                    \
        gcst = gcodeMissing;                                    \
      }                                                         \
      gcst;                                                     \
    })

#define gcodeFieldVal(gcfp, type) dto##type(gcodeFieldData(gcfp))
#define gcodeFieldValConv(gcfp, from, to, type) dto##type(unitsConv(gcodeFieldData(gcfp), from, to))

static inline char gcodeFieldCode(GCodeField *gcfp){
  return gcfp->code;
}

static inline decimal gcodeFieldData(GCodeField *gcfp){
  return gcfp->data;
}

static inline unsigned gcodeFields(GCodeEntry *gcep){
  return gcep->gcfn;
}

static inline GCodeField *gcodeField(GCodeEntry *gcep, unsigned gcfn){
  return &gcep->gcfa[gcfn];
}

void gcodeObjectInit(GCodeObject *gcop);

void gcodeStartParse(GCodeObject *gcop, GCodeEntry *gcep);
GCodeState gcodeParseChar(GCodeObject *gcop, char chr);

GCodeState gcodeParseStream(GCodeObject *gcop, BaseSequentialStream *bssp);

void gcodeEntryPrint(BaseSequentialStream *bssp, GCodeEntry *gcep);

#endif//__gcode_h__

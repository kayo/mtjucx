#include "ch.h"
#include "hal.h"

#include "shell.h"
#include "chprintf.h"
#include <string.h>

#include "sdc_local.h"
#include "usb_local.h"

#define LINE_FEED "\r\n"
#define SDC_MAX_PATH_LENGTH 128
#define SDC_MAX_LINE_LENGTH 64

FATFS SDC_FS;

#if !defined(SDC_SPI)
#define SDC_SS "sdc1"
/*
 * SDIO configuration.
 */
static const SDCConfig sdc_cfg = {
  0
};

#else//!defined(SDC_SPI)
#define SDC_SS "mmc1"
/*
 * MMC over SPI configuration.
 */

/* Maximum speed SPI configuration (18MHz, CPHA=0, CPOL=0, MSb first).*/
static SPIConfig hs_spi_cfg = {
  NULL,
  PORT(SDC_NSS),
  PAD(SDC_NSS),
  0
};

/* Low speed SPI configuration (281.250kHz, CPHA=0, CPOL=0, MSb first).*/
static SPIConfig ls_spi_cfg = {
  NULL,
  PORT(SDC_NSS),
  PAD(SDC_NSS),
  SPI_CR1_BR_2 | SPI_CR1_BR_1
};

/* MMC/SD over SPI driver configuration.*/
static MMCConfig sdc_cfg = {
  &SDC_SPI,
  &ls_spi_cfg,
  &hs_spi_cfg
};

#endif//!defined(SDC_SPI)

#define SDC_POLLING_INTERVAL 10
#define SDC_POLLING_DELAY 10

static bool sdc_ins = FALSE, sdc_ins_ = FALSE, sdc_mnt = TRUE;
static unsigned sdc_cnt = SDC_POLLING_INTERVAL;
static VirtualTimer sdc_itt;

static bool sdcReady(void){
  blkstate_t state = blkGetDriverState(&SDC_DEV);
  
  return sdc_ins_ && (state == BLK_READY/* || state == BLK_ACTIVE*/) && SDC_FS.fs_type != 0;
}

static void sdcTouch(void *p){
  BaseBlockDevice *bbdp = p;
  
  /* The presence check is performed only while the driver is not in a
     transfer state because it is often performed by changing the mode of
     the pin connected to the CS/D3 contact of the card, this could disturb
     the transfer.*/
  blkstate_t state = blkGetDriverState(bbdp);
  chSysLockFromIsr();
  if (state != BLK_READING &&
      state != BLK_WRITING) {
    /* Safe to perform the check.*/
    if (sdc_cnt > 0) {
      if (blkIsInserted(bbdp)) {
        if (--sdc_cnt == 0) {
          sdc_ins = TRUE;
        }
      } else {
        sdc_cnt = SDC_POLLING_INTERVAL;
      }
    } else {
      if (!blkIsInserted(bbdp)) {
        sdc_cnt = SDC_POLLING_INTERVAL;
        sdc_ins = FALSE;
      }
    }
  }
  chVTSetI(&sdc_itt, MS2ST(SDC_POLLING_DELAY), sdcTouch, &SDC_DEV);
  chSysUnlockFromIsr();
}

void sdcSetup(void){
  sdcStart(&SDC_DEV, &sdc_cfg);
  
  chVTSet(&sdc_itt, MS2ST(SDC_POLLING_DELAY), sdcTouch, &SDC_DEV);
}

FRESULT sdc_mfs = FR_OK;

void sdcUpdate(void){
  if(sdc_ins != sdc_ins_){
    if(sdc_ins){
      if(blkConnect(&SDC_DEV) == CH_FAILED){
        return;
      }
    }else{
      sdc_mfs = f_mount(0, NULL);
      blkDisconnect(&SDC_DEV);
    }
    sdc_ins_ = sdc_ins;
  }
  if(sdc_mnt){
    if(SDC_FS.fs_type == 0 &&
       blkGetDriverState(&SDC_DEV) == BLK_READY){
      sdc_mfs = f_mount(0, &SDC_FS);
      f_chdir("/");
    }
  }else{
    if(SDC_FS.fs_type != 0){
      sdc_mfs = f_mount(0, NULL);
#if USB_MSC_COUNT > 0
      msuDeviceLoad(&MSU1);
#endif
    }else{
#if USB_MSC_COUNT > 0
      if(!sdc_mnt && MSU1.bdi.blk_size == 0){
        sdc_mnt = TRUE;
      }
#endif
    }
  }
}

static const char *_f_errors[] = {
  [FR_OK] = "Succeeded",
	[FR_DISK_ERR] = "A hard error occured in the low level disk I/O layer",
	[FR_INT_ERR] = "Assertion failed",
	[FR_NOT_READY] = "The physical drive cannot work",
	[FR_NO_FILE] = "Could not find the file",
	[FR_NO_PATH] = "Could not find the path",
	[FR_INVALID_NAME] = "The path name format is invalid",
	[FR_DENIED] = "Acces denied due to prohibited access or directory full",
	[FR_EXIST] = "Acces denied due to prohibited access",
	[FR_INVALID_OBJECT] = "The file/directory object is invalid",
	[FR_WRITE_PROTECTED] = "The physical drive is write protected",
	[FR_INVALID_DRIVE] = "The logical drive number is invalid",
	[FR_NOT_ENABLED] = "The volume has no work area",
	[FR_NO_FILESYSTEM] = "There is no valid FAT volume",
	[FR_MKFS_ABORTED] = "The f_mkfs() aborted due to any parameter error",
	[FR_TIMEOUT] = "Could not get a grant to access the volume within defined period",
	[FR_LOCKED] = "The operation is rejected according to the file shareing policy",
	[FR_NOT_ENOUGH_CORE] = "LFN working buffer could not be allocated",
	[FR_TOO_MANY_OPEN_FILES] = "Number of open files > _FS_SHARE",
	[FR_INVALID_PARAMETER] = "Given parameter is invalid"
};

const char *f_errstr(FRESULT r){
  return _f_errors[r]; // (!) Unsafe usage
}

shellCommandDefine(mount){
  (void)argv;
  shellStdio(chp);
  
  if(argc != 0){
    chprintf(chp,
             "Usage: mount" LINE_FEED
             "  Get SD Card status." LINE_FEED);
    return -1;
  }
  
  if(sdc_ins_ && sdc_mnt){
    chprintf(chp, SDC_SS " on / type fatfs (%s)" LINE_FEED, f_errstr(sdc_mfs));
  }else{
    chprintf(chp, SDC_SS " not mounted (may be in host mode)" LINE_FEED);
  }
  
  return 0;
}

shellCommandDefine(umount){
  (void)argv;
  shellStdio(chp);
  
  if(argc != 0){
    chprintf(chp,
             "Usage: umount" LINE_FEED
             "  Unmount SD Card from device (switch to host mode)." LINE_FEED);
    return -1;
  }
  
  if(!sdc_mnt){
    chprintf(chp, "SD Card already in host mode" LINE_FEED);
    return -1;
  }
  
  sdc_mnt = FALSE;
  
  return 0;
}

shellCommandDefine(pwd){
  (void)argv;
  shellStdio(chp);
  
  if(argc != 0){
    chprintf(chp,
             "Usage: pwd" LINE_FEED
             "  Get current working path." LINE_FEED);
    
    return -1;
  }
  
  char ent[SDC_MAX_PATH_LENGTH];
  FRESULT res = f_getcwd(ent, sizeof(ent));
  
  if(res != FR_OK){
    chprintf(chp, "FS getcwd failed: %s" LINE_FEED, f_errstr(res));
    return -1;
  }
  
  char *str = ent;
  for(; *str != '/'; str++); /* skip "<label>:" */
  
  chprintf(chp, "%s" LINE_FEED, str);
  return 0;
}

shellCommandDefine(ls){
  shellStdio(chp);
  
  if((argc != 1 && argc != 0) ||
     (argc == 1 && argv[0][0] == '-')){
    chprintf(chp,
             "Usage: ls [path]" LINE_FEED
             "  Get directory listing." LINE_FEED);
    return -1;
  }

  if(!sdcReady()){
    chprintf(chp, "SD Card not mounted." LINE_FEED);
    return -1;
  }

  FRESULT res;
  char ent[SDC_MAX_PATH_LENGTH];
  const char *path = "";
  
  if(argc == 1){
    path = argv[0];
  }else{
    res = f_getcwd(ent, sizeof(ent));
    
    if(res != FR_OK){
      chprintf(chp, "FS getcwd failed: %s" LINE_FEED, f_errstr(res));
      return -1;
    }
    
    path = ent;
  }
  
  FILINFO fno;
  fno.lfname = ent;
  fno.lfsize = sizeof(ent);
  
  DIR dir;
  
  res = f_opendir(&dir, path);
  
  if(res == FR_OK){
    for(; ; ){
      res = f_readdir(&dir, &fno);
      
      if(res != FR_OK){
        chprintf(chp, "FS readdir failed: %s" LINE_FEED, f_errstr(res));
        return -1;
      }
      
      if(fno.fname[0] == 0){
        break;
      }
      
      chprintf(chp, "%s%s%s%s %8d %s" LINE_FEED,
               fno.fattrib & AM_DIR ? "d" : "-",
               fno.fattrib & AM_RDO ? "r" : "w",
               fno.fattrib & AM_HID ? "h" : "v",
               fno.fattrib & AM_ARC ? "a" : "-",
               fno.fsize,
               ent);
    }
  }else{
    chprintf(chp, "FS opendir failed: %s" LINE_FEED, f_errstr(res));
  }
  
  return 0;
}

shellCommandDefine(cd){
  shellStdio(chp);
  
  if((argc != 1 && argc != 0) ||
     (argc == 1 && argv[0][0] == '-')){
    chprintf(chp,
             "Usage: cd [path]" LINE_FEED
             "  Changing current working directory." LINE_FEED);
    return -1;
  }
  
  if(!sdcReady()){
    chprintf(chp, "SD Card not mounted." LINE_FEED);
    return -1;
  }
  
  const char* path = "/";
  
  if(argc == 1){
    path = argv[0];
  }
  
  FRESULT res = f_chdir(path);
  
  if(res != FR_OK){
    chprintf(chp, "FS chdir failed: %s" LINE_FEED, f_errstr(res));
    
    return -1;
  }
  
  return 0;
}

shellCommandDefine(cat){
  shellStdio(chp);
  
  if(argc != 1){
    chprintf(chp,
             "Usage: cat <file>" LINE_FEED
             "  Printing contents of file." LINE_FEED);
    return -1;
  }

  if(!sdcReady()){
    chprintf(chp, "SD Card not mounted." LINE_FEED);
    return -1;
  }
  
  const char *path = argv[0];
  FIL fob;
  
  FRESULT res = f_open(&fob, path, FA_READ);
  
  if(res != FR_OK){
    chprintf(chp, "FS open failed: %s" LINE_FEED, f_errstr(res));
    
    return -1;
  }
  
  char buf[SDC_MAX_LINE_LENGTH];
  char *ptr, *end;
  UINT rbs, pos;
  
  for(; !f_eof(&fob); ){
    res = f_read(&fob, buf, sizeof(buf), &rbs);
    
    if(res != FR_OK){
      chprintf(chp, "FS read failed: %s" LINE_FEED, f_errstr(res));
      
      return -1;
    }
    
    if(rbs == 0){
      break;
    }
    
    ptr = buf;
    end = ptr + rbs;
    pos = 0;
    
    for(; ptr < end; ptr ++, pos ++){
      switch(*ptr){
      case '\r':
        pos = 0;
        break;
      case '\n':
        if(pos != 0){
          chSequentialStreamPut(chp, '\r');
        }
        pos --;
      default:
        chSequentialStreamPut(chp, *ptr);
      }
    }
  }
  
  return 0;
}

shellCommandDefine(source){
  shellStdio(chp);
  
  if(argc != 1){
    chprintf(chp,
             "Usage: source <file>" LINE_FEED
             "  Running script from file." LINE_FEED);
    return -1;
  }
  
  if(!sdcReady()){
    chprintf(chp, "SD Card not mounted." LINE_FEED);
    return -1;
  }
  
  const char *path = argv[0];
  FIL fob;
  
  FRESULT res = f_open(&fob, path, FA_READ);
  
  if(res != FR_OK){
    chprintf(chp, "FS open failed: %s" LINE_FEED, f_errstr(res));
    
    return -1;
  }
  
  UINT rbs, lnum = 1, skip = 0;
  char *buf = shellLinePtr(), *end = shellLineEnd();
  char *ptr = buf;
  
  /* char by char script reading */
  for(; !f_eof(&fob); ){
    res = f_read(&fob, ptr, 1, &rbs);
    
    if(res != FR_OK){
      chprintf(chp, "FS read failed: %s" LINE_FEED, f_errstr(res));
      
      return -1;
    }
    
    if(rbs == 0){
      break;
    }

    if(*ptr == '\n'){ /* run command */
      if(skip > 0){
        skip --;
      }else{
        *ptr = '\0';
        chprintf(chp, "!%02d:> %s\r\n", lnum, buf);
        shellExecLine(shellSelf());
      }
      ptr = buf;
      lnum ++;
      continue;
    }
    
    if(*ptr < 0x20){ /* skip */
      continue;
    }
    
    ptr ++;

    if(ptr >= end){
      chprintf(chp, "Command line %d too long!", lnum);
      skip ++;
      ptr = buf;
    }
  }

  if(ptr > buf){ /* last non-\n terminated line */
    *ptr = '\0';
    chprintf(chp, "!%02d:> %s\r\n", lnum, buf);
    shellExecLine(shellSelf());
  }
  
  return 0;
}

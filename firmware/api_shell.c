//#include <string.h>

#include "ch.h"
#include "chprintf.h"

#include "api.h"
#include "shell.h"

#define LINE_FEED "\r\n"

#define CHECK(cond, code, text, args...)        \
  if(cond){                                     \
    chprintf(chp, "Error: " text "!" LINE_FEED, \
             ##args);                           \
    return code;                                \
  }

static const char* paramTypeNames[] = {
  [paramTypeNone] = "None",
  [paramTypeUint] = "Uint",
  [paramTypeSint] = "Sint",
  [paramTypeReal] = "Real",
  [paramTypeCstr] = "Cstr",
  [paramTypeLink] = "Link",
};

#define paramTypeName(pdt) (paramTypeNames[(pdt)->type])

static const char* paramAccessCodes[] = {
  "no",
  "ro",
  "wo",
  "rw",
};

#define paramAccessCode(pdt) (paramAccessCodes[((pdt)->get ? 0b01 : 0b00) | ((pdt)->set ? 0b10 : 0b00)])

static const char *paramErrorStrings[] = {
  [-paramSuccess]     = "success",
  [-paramIndexError]  = "index error",
  [-paramTypeError]   = "type error",
  [-paramValueError]  = "value error",
  [-paramRangeError]  = "range error",
  [-paramAccessError] = "access error",
  [-paramConfigError] = "config error",
  [-paramDeviceError] = "device error",
};

#define paramErrorString(ret) (paramErrorStrings[-(ret)])

shellCommandDefine(list){
  shellStdio(chp);
  
  if(argc > 0){
    
#define what argv[0]
    
    if(what[0] == 't' || what[0] == 'T'){
      if(argc != 1){
        chprintf(chp,
                 "Usage: list [t]ypes" LINE_FEED
                 "  Show list of object types." LINE_FEED);
        
        return -1;
      }
      
      const BaseObjectVMT **vmtp;
      
      //chprintf(chp, "Type Name (Number of Params) // Description" LINE_FEED);
      
      objectTypeEach(vmtp){
        chprintf(chp, "%s <%d> // %s" LINE_FEED, vmtp[0]->name, vmtp[0]->pcnt, vmtp[0]->info);
      }
      
      return 0;
    }
    
    if(what[0] == 'o' || what[0] == 'O'){
      if(argc != 1){
        chprintf(chp,
                 "Usage: list [o]bjects" LINE_FEED
                 "  Show list of objects." LINE_FEED);
        
        return -1;
      }
      
      BaseObject* obj = NULL;
      
      //chprintf(chp, "Object Name (Type Name)" LINE_FEED);
      
      for(; (obj = objectNext(obj)); ){
        chprintf(chp, "%s <%s> //%s" LINE_FEED, obj->name, obj->vmt->name, obj->vmt->del ? "" : " built-in");
      }
      
      return 0;
    }
    
    if(what[0] == 'p' || what[0] == 'P'){
      if(argc != 2){
        chprintf(chp,
                 "Usage: list [p]arams <type_name|object_name>" LINE_FEED
                 "  Show list of parameters." LINE_FEED);
        
        return -1;
      }
      
#define type argv[1]
#define name argv[1]
      
      const BaseObjectVMT* vmt = objectTypeFind(type);

      if(!vmt){
        const BaseObject* obj = objectFind(name);

        if(obj){
          vmt = objectType(obj);
        }
      }
      
      CHECK(!vmt, -1, "Object or type <%s> doesn't exists", type);
      
#undef type
#undef name
      
      //chprintf(chp, "Param Type (Access) Name // Description" LINE_FEED);
      
      int idx;
      const BaseObjectPDT* pdt;
      
      for(idx = 0; (pdt = objectTypeParam(vmt, idx)); idx++){
        chprintf(chp, "%s <%s> %s // %s" LINE_FEED, paramTypeName(pdt), paramAccessCode(pdt), pdt->name, pdt->info);
      }
      
      return 0;
    }
    
#undef what
  }
  
  chprintf(chp,
           "Usage: list <[t]ypes|[o]bjects|[p]arams> [<type_name>]" LINE_FEED
           "  Show list of object types, objects or parameters." LINE_FEED);
  
  return -1;
}

shellCommandDefine(add){
  shellStdio(chp);
  
  if(argc != 2){
    chprintf(chp,
             "Usage: add <object_name> <object_type>" LINE_FEED
             "  Add new sensor, driver or controller object." LINE_FEED);
    
    return -1;
  }
  
#define name argv[0]
#define type argv[1]
  
  BaseObject *obj = objectFind(name);
  
  CHECK(obj, -1, "Object <%s> already exists", name);
  
  const BaseObjectVMT *vmt = objectTypeFind(type);
  
  CHECK(!vmt, -1, "Object type <%s> doesn't exists", type);
  
  CHECK(!vmt->crt, -1, "Object type <%s> is built-in and can't be created", type);
  
  obj = objectCreate(vmt, name);
  
  CHECK(!obj, -1, "Unable to create object <%s> with type <%s>", name, type);
  
#undef name
#undef type
  
  return 0;
}

shellCommandDefine(del){
  shellStdio(chp);
  
  if(argc != 1){
    chprintf(chp,
             "Usage: del <object_name>" LINE_FEED
             "  Delete sensor, driver or controller object." LINE_FEED);
    
    return -1;
  }
  
#define name argv[0]

  BaseObject* obj = objectFind(name);
  
  CHECK(!obj, -1, "Object <%s> doesn't exists", name);

  CHECK(!obj->vmt->del, -1, "Object <%s> is built-in and can't be deleted", name);
  
  objectRemove(obj);
  
#undef name
  
  return 0;
}

shellCommandDefine(set){
  shellStdio(chp);
  
  if(argc < 2){
    chprintf(chp,
             "Usage: set <object_name> <param1>=<value1> [... <param%d>=<value%d>]" LINE_FEED
             "  Set object parameter values." LINE_FEED, SHELL_MAX_ARGUMENTS-2, SHELL_MAX_ARGUMENTS-2);
    
    return -1;
  }
  
#define name argv[0]
#define key argv[i]
  
  BaseObject* obj = objectFind(name);
  
  CHECK(!obj, -1, "Object <%s> doesn't exists", name);
  
  char *val;
  int par, ret, i;
  
  for(i = 1; i < argc; i++){
    for(val = key; *val != '\0' && *val != '='; val ++);
    
    CHECK(*val == '\0', paramValueError,
          "Invalid parameter string <%s>",
          key);
    
    *val++ = '\0';
    
    par = objectParamFind(obj, key);
    
    CHECK(par < 0, paramIndexError,
          "Parameter <%s> of object <%s> doesn't exists",
          key, name);
    
    ret = objectParamSetStr(obj, par, val);
    
    CHECK(ret != paramSuccess, ret,
          "Can't set parameter <%s>: %s",
          key, paramErrorString(ret));
  }

#undef key
#undef name
  
  return paramSuccess;
}

shellCommandDefine(get){
  shellStdio(chp);
  
  if(argc < 2){
    chprintf(chp,
             "Usage: get <object_name> <param1> [... <param%d>]" LINE_FEED
             "  Get object parameter values." LINE_FEED, SHELL_MAX_ARGUMENTS-2);
    
    return -1;
  }

#define name argv[0]
#define key argv[i]
  
  BaseObject* obj = objectFind(name);
  
  CHECK(!obj, -1, "Object <%s> doesn't exists", name);
  
  char val[64];
  int par, ret, i;
  
  for(i = 1; i < argc; i ++){
    par = objectParamFind(obj, key);
    
    CHECK(par < 0, paramIndexError,
          "Parameter <%s> of object <%s> doesn't exists",
          key, name);
    
    ret = objectParamGetStr(obj, par, val);

    CHECK(ret != paramSuccess, ret,
          "Can't get parameter <%s>: %s",
          key, paramErrorString(ret));
    
    chprintf(chp, "%s ", val);
  }

#undef key
#undef name
  
  chprintf(chp, LINE_FEED);

  return paramSuccess;
}

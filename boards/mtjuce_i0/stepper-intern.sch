EESchema Schematic File Version 2  date Чт 30 янв 2014 12:18:52
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:stm32
LIBS:comm
LIBS:a4988
LIBS:project-cache
EELAYER 25  0
EELAYER END
$Descr A4 11700 8267
encoding utf-8
Sheet 4 11
Title ""
Date "30 jan 2014"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 6900 2850 0    60   ~ 0
VMM = 12-24 [V]
Text Notes 4100 5950 0    60   ~ 0
I trip max = 3.3 × RVn01 / (8 × Rn05,Rn06 × (RVn01 + Rn01))\n\nI trip max = 2 [A]
Text Notes 8050 3200 0    60   ~ 0
~Charge Pump (CP1,2)\n\nThe charge pump is used to generate\na gate supply greater than that of VBB\nfor driving the source-side FET gates.\n
Text Notes 7600 4900 0    60   ~ 0
~VREG\n\nThis internally-generated voltage\nis used to operate the sink-side\nFET outputs.\n\nVreg = 7 [V]
Text Notes 1750 5000 0    60   ~ 0
~Internal PWM Control\n\nI trip max = Vref / (8 × Rsense)\n
Connection ~ 4100 5500
Wire Wire Line
	4100 4200 4100 5500
Connection ~ 4100 3500
Wire Wire Line
	4100 3700 4100 3300
Wire Wire Line
	7400 3700 7800 3700
Wire Wire Line
	4800 4400 4700 4400
Wire Wire Line
	4800 4200 4700 4200
Wire Wire Line
	4800 3700 4700 3700
Wire Wire Line
	7800 3200 7800 3100
Connection ~ 7000 3100
Wire Wire Line
	7400 3200 7400 3100
Connection ~ 6700 3100
Wire Wire Line
	6700 2700 6700 3500
Wire Wire Line
	7000 3600 7000 3700
Wire Wire Line
	7000 3700 6600 3700
Connection ~ 7400 4700
Wire Wire Line
	7400 4500 7300 4500
Wire Wire Line
	6800 4700 6700 4700
Wire Wire Line
	6700 4700 6700 4600
Wire Wire Line
	6700 4600 6600 4600
Wire Wire Line
	5900 5500 5900 5200
Wire Wire Line
	5500 5200 5500 5500
Wire Wire Line
	5500 2700 3800 2700
Wire Wire Line
	3800 2700 3800 3700
Connection ~ 5500 5500
Connection ~ 4700 2700
Connection ~ 5500 2700
Wire Wire Line
	4700 2800 4700 2700
Wire Wire Line
	6600 4800 6700 4800
Wire Wire Line
	6700 4800 6700 4950
Connection ~ 5900 5500
Wire Wire Line
	5900 2500 5900 2400
Wire Wire Line
	5900 2400 5700 2400
Wire Wire Line
	5700 2400 5700 3000
Wire Wire Line
	6900 4200 6800 4200
Wire Wire Line
	6800 4200 6800 4300
Wire Wire Line
	6800 4300 6600 4300
Wire Wire Line
	6600 4000 6900 4000
Connection ~ 5700 5500
Wire Wire Line
	6900 3900 6600 3900
Wire Wire Line
	6600 4200 6700 4200
Wire Wire Line
	6700 4200 6700 4100
Wire Wire Line
	6700 4100 6900 4100
Wire Wire Line
	5500 3000 5500 2600
Wire Wire Line
	5900 2900 5900 3000
Wire Wire Line
	4800 3400 4700 3400
Wire Wire Line
	4700 3400 4700 3300
Wire Wire Line
	3800 4200 3800 4550
Wire Wire Line
	4800 4800 3950 4800
Wire Wire Line
	3800 5050 3800 5500
Wire Wire Line
	5700 5600 5700 5200
Wire Wire Line
	6700 5500 6700 5350
Wire Wire Line
	6600 4500 6800 4500
Wire Wire Line
	7400 4700 7300 4700
Connection ~ 6700 5500
Wire Wire Line
	6700 3500 6600 3500
Wire Wire Line
	6600 3400 6700 3400
Wire Wire Line
	7000 3100 7000 3200
Connection ~ 6700 3400
Wire Wire Line
	7400 3600 7400 5500
Connection ~ 7400 4500
Wire Wire Line
	7800 3700 7800 3600
Connection ~ 7400 3700
Connection ~ 7400 3100
Wire Wire Line
	4800 4000 4350 4000
Wire Wire Line
	4800 3800 4700 3800
Wire Wire Line
	4800 4300 4700 4300
Wire Wire Line
	4800 4600 4700 4600
Wire Wire Line
	7800 3100 6700 3100
Wire Wire Line
	7400 5500 3800 5500
Wire Wire Line
	4800 3500 4100 3500
Wire Wire Line
	4100 2700 4100 2800
Connection ~ 4100 2700
$Comp
L R R402
U 1 1 52E0EFA1
P 4100 3050
AR Path="/52E0BE52/52E0EFA1" Ref="R402"  Part="1" 
AR Path="/52E0FC36/52E0EFA1" Ref="R502"  Part="1" 
AR Path="/52E10426/52E0EFA1" Ref="R602"  Part="1" 
AR Path="/52E10446/52E0EFA1" Ref="R702"  Part="1" 
AR Path="/52E1046F/52E0EFA1" Ref="R802"  Part="1" 
F 0 "R402" V 4180 3050 50  0000 C CNN
F 1 "NC" V 4100 3050 50  0000 C CNN
F 2 "SM0603" H 4100 3050 60  0001 C CNN
	1    4100 3050
	-1   0    0    -1  
$EndComp
Text Notes 1750 2850 0    60   ~ 0
~Fixed Off-Time Toff [uS]\n\n\nRosc = VDD, Toff = 30,\n\ndecay mode is automatic Mixed decay\nexcept when in full step, where decay\nmode is set to Slow decay.\n\n\nRosc = GND, Toff = 30,\n\ncurrent decay is set to Mixed decay\nfor both increasing and decreasing\ncurrents for all step modes.\n\n\nRosc = R to GND, Toff = R / 825,\n\ndecay mode is automatic Mixed decay\nfor all step modes.\n
Text HLabel 4700 4600 0    60   Input ~ 0
~ENABLE
Text HLabel 4700 4400 0    60   Input ~ 0
MS1
Text HLabel 4700 4300 0    60   Input ~ 0
MS2
Text Label 4350 4000 0    60   ~ 0
~MOT_RST
Text HLabel 4700 4200 0    60   Input ~ 0
MS3
Text HLabel 4700 3800 0    60   Input ~ 0
STEP
Text HLabel 4700 3700 0    60   Input ~ 0
DIR
$Comp
L C C405
U 1 1 52E0EAD9
P 7800 3400
AR Path="/52E0BE52/52E0EAD9" Ref="C405"  Part="1" 
AR Path="/52E0FC36/52E0EAD9" Ref="C505"  Part="1" 
AR Path="/52E10426/52E0EAD9" Ref="C605"  Part="1" 
AR Path="/52E10446/52E0EAD9" Ref="C705"  Part="1" 
AR Path="/52E1046F/52E0EAD9" Ref="C805"  Part="1" 
F 0 "C405" H 7850 3500 50  0000 L CNN
F 1 "220n" H 7850 3300 50  0000 L CNN
F 2 "SM0603" H 7800 3400 60  0001 C CNN
	1    7800 3400
	-1   0    0    -1  
$EndComp
$Comp
L CP1 C404
U 1 1 52E0EA63
P 7400 3400
AR Path="/52E0BE52/52E0EA63" Ref="C404"  Part="1" 
AR Path="/52E0FC36/52E0EA63" Ref="C504"  Part="1" 
AR Path="/52E10426/52E0EA63" Ref="C604"  Part="1" 
AR Path="/52E10446/52E0EA63" Ref="C704"  Part="1" 
AR Path="/52E1046F/52E0EA63" Ref="C804"  Part="1" 
F 0 "C404" H 7450 3500 50  0000 L CNN
F 1 "4.7u 50V" H 7300 3300 50  0000 L CNN
F 2 "SM0805" H 7400 3400 60  0001 C CNN
	1    7400 3400
	-1   0    0    -1  
$EndComp
Text HLabel 6700 2700 1    60   Input ~ 0
VMM
$Comp
L C C403
U 1 1 52E0E9E8
P 7000 3400
AR Path="/52E0BE52/52E0E9E8" Ref="C403"  Part="1" 
AR Path="/52E0FC36/52E0E9E8" Ref="C503"  Part="1" 
AR Path="/52E10426/52E0E9E8" Ref="C603"  Part="1" 
AR Path="/52E10446/52E0E9E8" Ref="C703"  Part="1" 
AR Path="/52E1046F/52E0E9E8" Ref="C803"  Part="1" 
F 0 "C403" H 7050 3500 50  0000 L CNN
F 1 "100n" H 7050 3300 50  0000 L CNN
F 2 "SM0603" H 7000 3400 60  0001 C CNN
	1    7000 3400
	-1   0    0    -1  
$EndComp
$Comp
L R R405
U 1 1 52E0E979
P 7050 4500
AR Path="/52E0BE52/52E0E979" Ref="R405"  Part="1" 
AR Path="/52E0FC36/52E0E979" Ref="R505"  Part="1" 
AR Path="/52E10426/52E0E979" Ref="R605"  Part="1" 
AR Path="/52E10446/52E0E979" Ref="R705"  Part="1" 
AR Path="/52E1046F/52E0E979" Ref="R805"  Part="1" 
F 0 "R405" V 7130 4500 50  0000 C CNN
F 1 "0.05" V 7050 4500 50  0000 C CNN
F 2 "SM0805" H 7050 4500 60  0001 C CNN
	1    7050 4500
	0    1    -1   0   
$EndComp
$Comp
L R R406
U 1 1 52E0E96C
P 7050 4700
AR Path="/52E0BE52/52E0E96C" Ref="R406"  Part="1" 
AR Path="/52E0FC36/52E0E96C" Ref="R506"  Part="1" 
AR Path="/52E10426/52E0E96C" Ref="R606"  Part="1" 
AR Path="/52E10446/52E0E96C" Ref="R706"  Part="1" 
AR Path="/52E1046F/52E0E96C" Ref="R806"  Part="1" 
F 0 "R406" V 7130 4700 50  0000 C CNN
F 1 "0.05" V 7050 4700 50  0000 C CNN
F 2 "SM0805" H 7050 4700 60  0001 C CNN
	1    7050 4700
	0    1    -1   0   
$EndComp
$Comp
L R R401
U 1 1 52E0E82B
P 3800 3950
AR Path="/52E0BE52/52E0E82B" Ref="R401"  Part="1" 
AR Path="/52E0FC36/52E0E82B" Ref="R501"  Part="1" 
AR Path="/52E10426/52E0E82B" Ref="R601"  Part="1" 
AR Path="/52E10446/52E0E82B" Ref="R701"  Part="1" 
AR Path="/52E1046F/52E0E82B" Ref="R801"  Part="1" 
F 0 "R401" V 3880 3950 50  0000 C CNN
F 1 "30K" V 3800 3950 50  0000 C CNN
F 2 "SM0603" H 3800 3950 60  0001 C CNN
	1    3800 3950
	-1   0    0    -1  
$EndComp
$Comp
L POT RV401
U 1 1 52E0E7FE
P 3800 4800
AR Path="/52E0BE52/52E0E7FE" Ref="RV401"  Part="1" 
AR Path="/52E0FC36/52E0E7FE" Ref="RV501"  Part="1" 
AR Path="/52E10426/52E0E7FE" Ref="RV601"  Part="1" 
AR Path="/52E10446/52E0E7FE" Ref="RV701"  Part="1" 
AR Path="/52E1046F/52E0E7FE" Ref="RV801"  Part="1" 
F 0 "RV401" H 3800 4700 50  0000 C CNN
F 1 "0-10K" H 3800 4800 50  0000 C CNN
	1    3800 4800
	0    1    1    0   
$EndComp
$Comp
L R R403
U 1 1 52E0E79D
P 4100 3950
AR Path="/52E0BE52/52E0E79D" Ref="R403"  Part="1" 
AR Path="/52E0FC36/52E0E79D" Ref="R503"  Part="1" 
AR Path="/52E10426/52E0E79D" Ref="R603"  Part="1" 
AR Path="/52E10446/52E0E79D" Ref="R703"  Part="1" 
AR Path="/52E1046F/52E0E79D" Ref="R803"  Part="1" 
F 0 "R403" V 4180 3950 50  0000 C CNN
F 1 "10K" V 4100 3950 50  0000 C CNN
F 2 "SM0603" H 4100 3950 60  0001 C CNN
	1    4100 3950
	-1   0    0    -1  
$EndComp
$Comp
L R R404
U 1 1 52E0E748
P 4700 3050
AR Path="/52E0BE52/52E0E748" Ref="R404"  Part="1" 
AR Path="/52E0FC36/52E0E748" Ref="R504"  Part="1" 
AR Path="/52E10426/52E0E748" Ref="R604"  Part="1" 
AR Path="/52E10446/52E0E748" Ref="R704"  Part="1" 
AR Path="/52E1046F/52E0E748" Ref="R804"  Part="1" 
F 0 "R404" V 4780 3050 50  0000 C CNN
F 1 "10K" V 4700 3050 50  0000 C CNN
F 2 "SM0603" H 4700 3050 60  0001 C CNN
	1    4700 3050
	-1   0    0    -1  
$EndComp
$Comp
L C C402
U 1 1 52E0E6FE
P 6700 5150
AR Path="/52E0BE52/52E0E6FE" Ref="C402"  Part="1" 
AR Path="/52E0FC36/52E0E6FE" Ref="C502"  Part="1" 
AR Path="/52E10426/52E0E6FE" Ref="C602"  Part="1" 
AR Path="/52E10446/52E0E6FE" Ref="C702"  Part="1" 
AR Path="/52E1046F/52E0E6FE" Ref="C802"  Part="1" 
F 0 "C402" H 6750 5250 50  0000 L CNN
F 1 "220n" H 6750 5050 50  0000 L CNN
F 2 "SM0603" H 6700 5150 60  0001 C CNN
	1    6700 5150
	-1   0    0    -1  
$EndComp
$Comp
L C C401
U 1 1 52E0E6D0
P 5900 2700
AR Path="/52E0BE52/52E0E6D0" Ref="C401"  Part="1" 
AR Path="/52E0FC36/52E0E6D0" Ref="C501"  Part="1" 
AR Path="/52E10426/52E0E6D0" Ref="C601"  Part="1" 
AR Path="/52E10446/52E0E6D0" Ref="C701"  Part="1" 
AR Path="/52E1046F/52E0E6D0" Ref="C801"  Part="1" 
F 0 "C401" H 5950 2800 50  0000 L CNN
F 1 "100n" H 5950 2600 50  0000 L CNN
F 2 "SM0603" H 5900 2700 60  0001 C CNN
	1    5900 2700
	-1   0    0    -1  
$EndComp
$Comp
L +3.3V #PWR401
U 1 1 52E0E6C1
P 5500 2600
AR Path="/52E0BE52/52E0E6C1" Ref="#PWR401"  Part="1" 
AR Path="/52E0FC36/52E0E6C1" Ref="#PWR501"  Part="1" 
AR Path="/52E10426/52E0E6C1" Ref="#PWR601"  Part="1" 
AR Path="/52E10446/52E0E6C1" Ref="#PWR701"  Part="1" 
AR Path="/52E1046F/52E0E6C1" Ref="#PWR801"  Part="1" 
F 0 "#PWR401" H 5500 2560 30  0001 C CNN
F 1 "+3.3V" H 5500 2710 30  0000 C CNN
	1    5500 2600
	1    0    0    -1  
$EndComp
$Comp
L CONN_4 P401
U 1 1 52E0E671
P 7250 4050
AR Path="/52E0BE52/52E0E671" Ref="P401"  Part="1" 
AR Path="/52E0FC36/52E0E671" Ref="P501"  Part="1" 
AR Path="/52E10426/52E0E671" Ref="P601"  Part="1" 
AR Path="/52E10446/52E0E671" Ref="P701"  Part="1" 
AR Path="/52E1046F/52E0E671" Ref="P801"  Part="1" 
F 0 "P401" V 7200 4050 50  0000 C CNN
F 1 "MOT" V 7300 4050 50  0000 C CNN
	1    7250 4050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR402
U 1 1 52E0E651
P 5700 5600
AR Path="/52E0BE52/52E0E651" Ref="#PWR402"  Part="1" 
AR Path="/52E0FC36/52E0E651" Ref="#PWR502"  Part="1" 
AR Path="/52E10426/52E0E651" Ref="#PWR602"  Part="1" 
AR Path="/52E10446/52E0E651" Ref="#PWR702"  Part="1" 
AR Path="/52E1046F/52E0E651" Ref="#PWR802"  Part="1" 
F 0 "#PWR402" H 5700 5600 30  0001 C CNN
F 1 "GND" H 5700 5530 30  0001 C CNN
	1    5700 5600
	1    0    0    -1  
$EndComp
$Comp
L A4988 D401
U 1 1 52E0D4B1
P 5700 4100
AR Path="/52E0BE52/52E0D4B1" Ref="D401"  Part="1" 
AR Path="/52E0FC36/52E0D4B1" Ref="D501"  Part="1" 
AR Path="/52E10426/52E0D4B1" Ref="D601"  Part="1" 
AR Path="/52E10446/52E0D4B1" Ref="D701"  Part="1" 
AR Path="/52E1046F/52E0D4B1" Ref="D801"  Part="1" 
F 0 "D401" H 5200 4950 60  0000 C CNN
F 1 "A4988" H 5700 4100 60  0000 C CNN
F 2 "QFN28" H 5700 4100 60  0001 C CNN
	1    5700 4100
	1    0    0    -1  
$EndComp
$EndSCHEMATC

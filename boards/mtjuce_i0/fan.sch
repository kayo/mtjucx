EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:stm32
LIBS:comm
LIBS:a4988
LIBS:n_fet
LIBS:project-cache
EELAYER 24 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 16 17
Title "4-pin fan control"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_4 P1501
U 1 1 5306C00D
P 6950 3550
AR Path="/5306707B/5306C00D" Ref="P1501"  Part="1" 
AR Path="/530673F7/5306C00D" Ref="P1601"  Part="1" 
AR Path="/53066EBB/5306C00D" Ref="P1701"  Part="1" 
F 0 "P1601" V 6900 3550 50  0000 C CNN
F 1 "FAN" V 7000 3550 50  0000 C CNN
F 2 "SIL-4" H 6950 3550 60  0001 C CNN
F 3 "" H 6950 3550 60  0000 C CNN
	1    6950 3550
	1    0    0    1   
$EndComp
$Comp
L +12V #PWR1501
U 1 1 5306C013
P 6300 2600
AR Path="/5306707B/5306C013" Ref="#PWR1501"  Part="1" 
AR Path="/530673F7/5306C013" Ref="#PWR1601"  Part="1" 
AR Path="/53066EBB/5306C013" Ref="#PWR1701"  Part="1" 
F 0 "#PWR1601" H 6300 2550 20  0001 C CNN
F 1 "+12V" H 6300 2700 30  0000 C CNN
F 2 "" H 6300 2600 60  0000 C CNN
F 3 "" H 6300 2600 60  0000 C CNN
	1    6300 2600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR1502
U 1 1 5306C019
P 6300 5000
AR Path="/5306707B/5306C019" Ref="#PWR1502"  Part="1" 
AR Path="/530673F7/5306C019" Ref="#PWR1602"  Part="1" 
AR Path="/53066EBB/5306C019" Ref="#PWR1702"  Part="1" 
F 0 "#PWR1602" H 6300 5000 30  0001 C CNN
F 1 "GND" H 6300 4930 30  0001 C CNN
F 2 "" H 6300 5000 60  0000 C CNN
F 3 "" H 6300 5000 60  0000 C CNN
	1    6300 5000
	1    0    0    -1  
$EndComp
Text HLabel 6000 4200 0    60   Input ~ 0
TACH
Text HLabel 6000 3400 0    60   Input ~ 0
PWM
$Comp
L ZENER D1501
U 1 1 5306C4E9
P 6300 3850
AR Path="/5306707B/5306C4E9" Ref="D1501"  Part="1" 
AR Path="/530673F7/5306C4E9" Ref="D1601"  Part="1" 
AR Path="/53066EBB/5306C4E9" Ref="D1701"  Part="1" 
F 0 "D1601" H 6300 3950 50  0000 C CNN
F 1 "3.3V" H 6300 3750 40  0000 C CNN
F 2 "SOT223" H 6300 3850 60  0001 C CNN
F 3 "" H 6300 3850 60  0000 C CNN
	1    6300 3850
	0    -1   -1   0   
$EndComp
$Comp
L R R1503
U 1 1 5306CA14
P 6100 4550
AR Path="/5306707B/5306CA14" Ref="R1503"  Part="1" 
AR Path="/530673F7/5306CA14" Ref="R1603"  Part="1" 
AR Path="/53066EBB/5306CA14" Ref="R1703"  Part="1" 
F 0 "R1603" V 6180 4550 40  0000 C CNN
F 1 "4.7K" V 6107 4551 40  0000 C CNN
F 2 "SM0603" V 6030 4550 30  0001 C CNN
F 3 "" H 6100 4550 30  0000 C CNN
	1    6100 4550
	-1   0    0    -1  
$EndComp
$Comp
L R R1502
U 1 1 5306E268
P 6100 3850
AR Path="/5306707B/5306E268" Ref="R1502"  Part="1" 
AR Path="/530673F7/5306E268" Ref="R1602"  Part="1" 
AR Path="/53066EBB/5306E268" Ref="R1702"  Part="1" 
F 0 "R1602" V 6180 3850 40  0000 C CNN
F 1 "10K" V 6107 3851 40  0000 C CNN
F 2 "SM0603" V 6030 3850 30  0001 C CNN
F 3 "" H 6100 3850 30  0000 C CNN
	1    6100 3850
	-1   0    0    -1  
$EndComp
$Comp
L R R1501
U 1 1 5306E7B8
P 6100 3050
AR Path="/5306707B/5306E7B8" Ref="R1501"  Part="1" 
AR Path="/530673F7/5306E7B8" Ref="R1601"  Part="1" 
AR Path="/53066EBB/5306E7B8" Ref="R1701"  Part="1" 
F 0 "R1601" V 6180 3050 40  0000 C CNN
F 1 "10K" V 6107 3051 40  0000 C CNN
F 2 "SM0603" V 6030 3050 30  0001 C CNN
F 3 "" H 6100 3050 30  0000 C CNN
	1    6100 3050
	-1   0    0    -1  
$EndComp
Connection ~ 6300 2700
Wire Wire Line
	6300 2600 6300 2700
Wire Wire Line
	6100 2700 6500 2700
Wire Wire Line
	6100 2800 6100 2700
Connection ~ 6100 3500
Wire Wire Line
	6100 3300 6100 3600
Wire Wire Line
	6100 3500 6600 3500
Wire Wire Line
	6500 2700 6500 3600
Wire Wire Line
	6500 3600 6600 3600
Wire Wire Line
	6500 4900 6500 3700
Wire Wire Line
	6500 3700 6600 3700
Wire Wire Line
	6000 3400 6600 3400
Wire Wire Line
	6100 4100 6100 4300
Wire Wire Line
	6000 4200 6100 4200
Connection ~ 6100 4200
Wire Wire Line
	6100 4800 6100 4900
Wire Wire Line
	6100 4900 6500 4900
Wire Wire Line
	6300 4050 6300 5000
Connection ~ 6300 4900
Wire Wire Line
	6300 3650 6300 3400
Connection ~ 6300 3400
Text Notes 4000 4150 0    60   ~ 0
~Tach Signal Divider:\n\n10R+10R+4.7R=24.7R\n\n12V*4.7R/24.7R=2.3V (logic one)\n0V (logic zero)
Text Notes 4000 3300 0    60   ~ 0
~Logic output overvoltage protection\n\nZener diode to ground are required\nbecause PWM input of the fan has\ninternally pullup to +12V
$EndSCHEMATC

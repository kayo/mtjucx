/*
    ChibiOS/RT - Copyright (C) 2006-2013 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include "ch.h"
#include "hal.h"

/**
 * @brief   PAL setup.
 * @details Digital I/O ports static configuration as defined in @p board.h.
 *          This variable is used by the HAL when initializing the PAL driver.
 */
#if HAL_USE_PAL || defined(__DOXYGEN__)
const PALConfig pal_default_config =
{
  {VAL_GPIOAODR, VAL_GPIOACRL, VAL_GPIOACRH},
  {VAL_GPIOBODR, VAL_GPIOBCRL, VAL_GPIOBCRH},
  {VAL_GPIOCODR, VAL_GPIOCCRL, VAL_GPIOCCRH},
  {VAL_GPIODODR, VAL_GPIODCRL, VAL_GPIODCRH},
  {VAL_GPIOEODR, VAL_GPIOECRL, VAL_GPIOECRH}
};
#endif

/*
 * Early initialization code.
 * This initialization must be performed just after stack setup and before
 * any other initialization.
 */
void __early_init(void) {
  stm32_clock_init();
}

#if HAL_USE_MMC_SPI
/* Board-related functions related to the SDC driver.*/
bool_t mmc_lld_is_card_inserted(MMCDriver *sdcp) {
#ifdef SDC_INS
  return !readPad(SDC_INS);
#else
  (void)sdcp;
  return TRUE;
#endif
}

bool_t mmc_lld_is_write_protected(MMCDriver *sdcp) {
  (void)sdcp;
  return FALSE;
}
#endif

#ifdef SDC_SPI
MMCDriver MMCD1;
#endif

/*
 * Board-specific initialization code.
 */
void boardInit(void) {

#ifdef SDC_SPI
  configPad(SDC_SCK, ALTERNATE_PUSHPULL);
  configPad(SDC_DI, ALTERNATE_PUSHPULL);
  configPad(SDC_DO, INPUT_PULLUP);
  
  configPad(SDC_NSS, OUTPUT_PUSHPULL);
  setPad(SDC_NSS);
  
  mmcObjectInit(&MMCD1);
#endif

#ifdef SDC_INS
  configPad(SDC_INS, INPUT_PULLUP);
#endif

#ifdef LED_1
  configPad(LED_1, OUTPUT_OPENDRAIN);
#endif

#ifdef LED_2
  configPad(LED_2, OUTPUT_OPENDRAIN);
#endif

#ifdef LED_3
  //configPad(LED_3, OUTPUT_OPENDRAIN);
  configPad(LED_3, OUTPUT_PUSHPULL);
#endif

#ifdef HAL_USE_USB
  configPad(USB_DM, INPUT);
  configPad(USB_DP, INPUT);
#endif

#ifdef SPI_1_DEV
  spiIO(SPI_1, 1);
# ifdef SPI_1_CS_1
  spiCS(SPI_1, 1, 1);
# endif
# ifdef SPI_1_CS_2
  spiCS(SPI_1, 2, 1);
# endif
# ifdef SPI_1_CS_3
  spiCS(SPI_1, 3, 1);
# endif
# ifdef SPI_1_CS_4
  spiCS(SPI_1, 4, 1);
# endif
#endif//SPI_1_DEV

#if 0
#ifdef MOTX_DIR
  motIn(X, 1);
  motOut(X, 1);
#endif

#ifdef MOTY_DIR
  motIn(Y, 1);
  motOut(Y, 1);
#endif

#ifdef MOTZ_DIR
  motIn(Z, 1);
  motOut(Z, 1);
#endif

#ifdef MOTE1_DIR
  motOut(E1, 1);
#endif

#ifdef MOTE2_DIR
  motOut(E2, 1);
#endif
#endif

}

#define STP_1_DIR PE0
#define STP_1_STP PE1
#define STP_1_ENB PE5
#define STP_1_MS3 PE2
#define STP_1_MS2 PE3
#define STP_1_MS1 PE4
#define STP_1_MIN PE6
#define STP_1_MAX PE7

#define STP_2_DIR PE8
#define STP_2_STP PE9
#define STP_2_ENB PE13
#define STP_2_MS3 PE10
#define STP_2_MS2 PE11
#define STP_2_MS1 PE12
#define STP_2_MIN PE14
#define STP_2_MAX PE15

#define STP_3_DIR PD8
#define STP_3_STP PD9
#define STP_3_ENB PD13
#define STP_3_MS3 PD10
#define STP_3_MS2 PD11
#define STP_3_MS1 PD12
#define STP_3_MIN PD14
#define STP_3_MAX PD15

#define STP_4_DIR PC6
#define STP_4_STP PC7
#define STP_4_ENB PC3
#define STP_4_MS3 PC0
#define STP_4_MS2 PC1
#define STP_4_MS1 PC2
#define STP_4_MIN PNC
#define STP_4_MAX PNC

#define SPI_1_DEV  SPID2
#define SPI_1_SCLK PB13
#define SPI_1_MISO PB14
#define SPI_1_MOSI PB15
#define SPI_1_CS_1 PB10
#define SPI_1_CS_2 PB11
#define SPI_1_CS_3 PB12

#define ADC_1_DEV ADCD1
#define ADC_1_CH_1_IN PC4
#define ADC_1_CH_1 IN14
#define ADC_1_CH_2_IN PC5
#define ADC_1_CH_2 IN15
#define ADC_1_CH_3_IN PB0
#define ADC_1_CH_3 IN8
#define ADC_1_CH_4_IN PB1
#define ADC_1_CH_4 IN9
#define ADC_1_CH_R_IN PNC
#define ADC_1_CH_R VREFINT
#define ADC_1_CH_T_IN PNC
#define ADC_1_CH_T SENSOR

#define PWM_1_DEV  PWMD2
#define PWM_1_CH_1_OUT PA0
#define PWM_1_CH_1 0
#define PWM_1_CH_2_OUT PA1
#define PWM_1_CH_2 1
#define PWM_1_CH_3_OUT PA2
#define PWM_1_CH_3 2
#define PWM_1_CH_4_OUT PA3
#define PWM_1_CH_4 3

#define GPT_1_DEV GPTD4

#define SDC_SPI SPID1
#define SDC_DI  PA7
#define SDC_DO  PA6
#define SDC_SCK PA5
#define SDC_NSS PA4
//#define SDC_INS PB5

#define ATX_PSON PC12
//#define ATX_PWOK 

#define LED_1 PC10
#define LED_2 PC11
#define LED_3 PC9

#define USB_DM PA11
#define USB_DP PA12
#define USB_DISC PA10

EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:stm32
LIBS:comm
LIBS:a4988
LIBS:n_fet
LIBS:project-cache
EELAYER 24 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 17
Title ""
Date "31 jan 2014"
Rev "0.01"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	6700 2200 6700 3200
Connection ~ 5600 2200
Wire Wire Line
	5100 2200 6700 2200
Wire Wire Line
	6200 2800 6200 2700
Connection ~ 6800 2500
Wire Wire Line
	7100 2500 6800 2500
Wire Wire Line
	7100 2500 7100 2600
Wire Wire Line
	5100 3900 5200 3900
Wire Wire Line
	5100 3500 5200 3500
Wire Wire Line
	5100 3300 5200 3300
Wire Wire Line
	5200 3700 5100 3700
Wire Wire Line
	5100 3700 5100 3600
Wire Wire Line
	6200 2300 6200 2200
Wire Wire Line
	5900 2900 5900 2800
Wire Wire Line
	6700 3200 6600 3200
Wire Wire Line
	6800 2400 6800 3800
Wire Wire Line
	6900 3600 6600 3600
Wire Wire Line
	6900 3400 6600 3400
Wire Wire Line
	6800 3800 6600 3800
Wire Wire Line
	6600 3900 6700 3900
Wire Wire Line
	6900 3500 6600 3500
Wire Wire Line
	6900 3700 6600 3700
Wire Wire Line
	6700 3300 6600 3300
Wire Wire Line
	6700 4000 6700 3300
Connection ~ 6700 3900
Wire Wire Line
	5100 3600 5200 3600
Wire Wire Line
	5100 3200 5200 3200
Wire Wire Line
	5100 3400 5200 3400
Wire Wire Line
	5100 3800 5200 3800
Wire Wire Line
	7100 3100 7100 3000
Wire Wire Line
	5600 2200 5600 2300
Connection ~ 6200 2200
Wire Wire Line
	6200 2800 5600 2800
Wire Wire Line
	5600 2800 5600 2700
Connection ~ 5900 2800
$Comp
L CP1 C301
U 1 1 52EB206F
P 5600 2500
AR Path="/52E0BE52/52EB206F" Ref="C301"  Part="1" 
AR Path="/52E0FC36/52EB206F" Ref="C401"  Part="1" 
AR Path="/52E10426/52EB206F" Ref="C501"  Part="1" 
AR Path="/52E10446/52EB206F" Ref="C601"  Part="1" 
AR Path="/52E1046F/52EB206F" Ref="C701"  Part="1" 
F 0 "C701" H 5650 2600 50  0000 L CNN
F 1 "100u 35V" H 5450 2400 50  0000 L CNN
F 2 "SMDHD/VF" H 5600 2500 60  0001 C CNN
F 3 "" H 5600 2500 60  0001 C CNN
	1    5600 2500
	-1   0    0    -1  
$EndComp
Text HLabel 5100 2200 0    60   Input ~ 0
VMM
$Comp
L GND #PWR304
U 1 1 52EB1B71
P 7100 3100
AR Path="/52E0BE52/52EB1B71" Ref="#PWR304"  Part="1" 
AR Path="/52E0FC36/52EB1B71" Ref="#PWR404"  Part="1" 
AR Path="/52E10426/52EB1B71" Ref="#PWR504"  Part="1" 
AR Path="/52E10446/52EB1B71" Ref="#PWR604"  Part="1" 
AR Path="/52E1046F/52EB1B71" Ref="#PWR704"  Part="1" 
F 0 "#PWR704" H 7100 3100 30  0001 C CNN
F 1 "GND" H 7100 3030 30  0001 C CNN
F 2 "" H 7100 3100 60  0001 C CNN
F 3 "" H 7100 3100 60  0001 C CNN
	1    7100 3100
	1    0    0    -1  
$EndComp
Text HLabel 5100 3900 0    60   Input ~ 0
DIR
Text HLabel 5100 3800 0    60   Input ~ 0
STP
Text HLabel 5100 3500 0    60   Input ~ 0
MS3
Text HLabel 5100 3400 0    60   Input ~ 0
MS2
Text HLabel 5100 3300 0    60   Input ~ 0
MS1
Text HLabel 5100 3200 0    60   Input ~ 0
~ENB
$Comp
L GND #PWR301
U 1 1 52E9F403
P 5900 2900
AR Path="/52E0BE52/52E9F403" Ref="#PWR301"  Part="1" 
AR Path="/52E0FC36/52E9F403" Ref="#PWR401"  Part="1" 
AR Path="/52E10426/52E9F403" Ref="#PWR501"  Part="1" 
AR Path="/52E10446/52E9F403" Ref="#PWR601"  Part="1" 
AR Path="/52E1046F/52E9F403" Ref="#PWR701"  Part="1" 
F 0 "#PWR701" H 5900 2900 30  0001 C CNN
F 1 "GND" H 5900 2830 30  0001 C CNN
F 2 "" H 5900 2900 60  0001 C CNN
F 3 "" H 5900 2900 60  0001 C CNN
	1    5900 2900
	1    0    0    -1  
$EndComp
$Comp
L C C302
U 1 1 52E9F3F6
P 6200 2500
AR Path="/52E0BE52/52E9F3F6" Ref="C302"  Part="1" 
AR Path="/52E0FC36/52E9F3F6" Ref="C402"  Part="1" 
AR Path="/52E10426/52E9F3F6" Ref="C502"  Part="1" 
AR Path="/52E10446/52E9F3F6" Ref="C602"  Part="1" 
AR Path="/52E1046F/52E9F3F6" Ref="C702"  Part="1" 
F 0 "C702" H 6250 2600 50  0000 L CNN
F 1 "100n" H 6250 2400 50  0000 L CNN
F 2 "SM0603" H 6200 2500 60  0001 C CNN
F 3 "" H 6200 2500 60  0001 C CNN
	1    6200 2500
	-1   0    0    -1  
$EndComp
$Comp
L C C303
U 1 1 52E9F3C3
P 7100 2800
AR Path="/52E0BE52/52E9F3C3" Ref="C303"  Part="1" 
AR Path="/52E0FC36/52E9F3C3" Ref="C403"  Part="1" 
AR Path="/52E10426/52E9F3C3" Ref="C503"  Part="1" 
AR Path="/52E10446/52E9F3C3" Ref="C603"  Part="1" 
AR Path="/52E1046F/52E9F3C3" Ref="C703"  Part="1" 
F 0 "C703" H 7150 2900 50  0000 L CNN
F 1 "100n" H 7150 2700 50  0000 L CNN
F 2 "SM0603" H 7100 2800 60  0001 C CNN
F 3 "" H 7100 2800 60  0001 C CNN
	1    7100 2800
	-1   0    0    -1  
$EndComp
$Comp
L +3.3V #PWR303
U 1 1 52E9F2F9
P 6800 2400
AR Path="/52E0BE52/52E9F2F9" Ref="#PWR303"  Part="1" 
AR Path="/52E0FC36/52E9F2F9" Ref="#PWR403"  Part="1" 
AR Path="/52E10426/52E9F2F9" Ref="#PWR503"  Part="1" 
AR Path="/52E10446/52E9F2F9" Ref="#PWR603"  Part="1" 
AR Path="/52E1046F/52E9F2F9" Ref="#PWR703"  Part="1" 
F 0 "#PWR703" H 6800 2360 30  0001 C CNN
F 1 "+3.3V" H 6800 2510 30  0000 C CNN
F 2 "" H 6800 2400 60  0001 C CNN
F 3 "" H 6800 2400 60  0001 C CNN
	1    6800 2400
	1    0    0    -1  
$EndComp
$Comp
L CONN_4 P301
U 1 1 52E9F2CC
P 7250 3550
AR Path="/52E0BE52/52E9F2CC" Ref="P301"  Part="1" 
AR Path="/52E0FC36/52E9F2CC" Ref="P401"  Part="1" 
AR Path="/52E10426/52E9F2CC" Ref="P501"  Part="1" 
AR Path="/52E10446/52E9F2CC" Ref="P601"  Part="1" 
AR Path="/52E1046F/52E9F2CC" Ref="P701"  Part="1" 
F 0 "P701" V 7200 3550 50  0000 C CNN
F 1 "MOT" V 7300 3550 50  0000 C CNN
F 2 "SIL-4" H 7250 3550 60  0001 C CNN
F 3 "" H 7250 3550 60  0001 C CNN
	1    7250 3550
	1    0    0    1   
$EndComp
$Comp
L GND #PWR302
U 1 1 52E9F256
P 6700 4000
AR Path="/52E0BE52/52E9F256" Ref="#PWR302"  Part="1" 
AR Path="/52E0FC36/52E9F256" Ref="#PWR402"  Part="1" 
AR Path="/52E10426/52E9F256" Ref="#PWR502"  Part="1" 
AR Path="/52E10446/52E9F256" Ref="#PWR602"  Part="1" 
AR Path="/52E1046F/52E9F256" Ref="#PWR702"  Part="1" 
F 0 "#PWR702" H 6700 4000 30  0001 C CNN
F 1 "GND" H 6700 3930 30  0001 C CNN
F 2 "" H 6700 4000 60  0001 C CNN
F 3 "" H 6700 4000 60  0001 C CNN
	1    6700 4000
	1    0    0    -1  
$EndComp
$Comp
L STEPSTICK J301
U 1 1 52E9F22D
P 5900 3550
AR Path="/52E0BE52/52E9F22D" Ref="J301"  Part="1" 
AR Path="/52E0FC36/52E9F22D" Ref="J401"  Part="1" 
AR Path="/52E10426/52E9F22D" Ref="J501"  Part="1" 
AR Path="/52E10446/52E9F22D" Ref="J601"  Part="1" 
AR Path="/52E1046F/52E9F22D" Ref="J701"  Part="1" 
F 0 "J701" H 5900 4050 60  0000 C CNN
F 1 "STEPSTICK" H 5900 3050 60  0000 C CNN
F 2 "STEPSTICK" H 5900 3550 60  0001 C CNN
F 3 "" H 5900 3550 60  0001 C CNN
	1    5900 3550
	1    0    0    -1  
$EndComp
$EndSCHEMATC

update=Сб 01 фев 2014 14:02:29
version=1
last_client=pcbnew
[general]
version=1
[cvpcb]
version=1
NetIExt=net
[cvpcb/libraries]
EquName1=devcms
[eeschema]
version=1
LibDir=
NetFmtName=PcbnewAdvanced
RptD_X=0
RptD_Y=100
RptLab=1
LabSize=60
[eeschema/libraries]
LibName1=power
LibName2=device
LibName3=transistors
LibName4=conn
LibName5=linear
LibName6=regul
LibName7=74xx
LibName8=cmos4000
LibName9=adc-dac
LibName10=memory
LibName11=xilinx
LibName12=special
LibName13=microcontrollers
LibName14=dsp
LibName15=microchip
LibName16=analog_switches
LibName17=motorola
LibName18=texas
LibName19=intel
LibName20=audio
LibName21=interface
LibName22=digital-audio
LibName23=philips
LibName24=display
LibName25=cypress
LibName26=siliconi
LibName27=opto
LibName28=atmel
LibName29=contrib
LibName30=valves
LibName31=stm32
LibName32=comm
LibName33=a4988
LibName34=n_fet
[pcbnew]
version=1
LastNetListRead=project.net
UseCmpFile=0
PadDrill="    0,000000"
PadDrillOvalY="    0,000000"
PadSizeH="    1,524000"
PadSizeV="    0,524000"
PcbTextSizeV="    2,032000"
PcbTextSizeH="    1,524000"
PcbTextThickness="    0,304800"
ModuleTextSizeV="    1,524000"
ModuleTextSizeH="    1,524000"
ModuleTextSizeThickness="    0,304800"
SolderMaskClearance="    0,254000"
SolderMaskMinWidth="    0,000000"
DrawSegmentWidth="    0,381000"
BoardOutlineThickness="    0,381000"
ModuleOutlineThickness="    0,381000"
[pcbnew/libraries]
LibDir=
LibName1=sockets
LibName2=connect
LibName3=discret
LibName4=pin_array
LibName5=divers
LibName6=smd_capacitors
LibName7=smd_resistors
LibName8=smd_crystal&oscillator
LibName9=smd_transistors
LibName10=libcms
LibName11=display
LibName12=led
LibName13=dip_sockets
LibName14=pga_sockets
LibName15=valves
LibName16=stepstick
LibName17=cr2032_battery
LibName18=miniusb_5pin
LibName19=to263
LibName20=sd_slot

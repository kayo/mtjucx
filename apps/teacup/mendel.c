/** \file
	\brief Main file - this is where it all starts, and ends
*/

/** \mainpage Teacup Reprap Firmware
	\section intro_sec Introduction
		Teacup Reprap Firmware (originally named FiveD on Arduino) is a firmware package for numerous reprap electronics sets.

		Please see README for a full introduction and long-winded waffle about this project
	\section install_sec	Installation
		\subsection step1 Step 1: Download
			\code git clone git://github.com/triffid/Teacup_Firmware \endcode
		\subsection step2 Step 2: configure
			\code cp config.[yourboardhere].h config.h \endcode
			Edit config.h to suit your machone
			Edit Makefile to select the correct chip and programming settings
		\subsection step3 Step 3: Compile
			\code make \endcode
			\code make program \endcode
		\subsection step4 Step 4: Test!
			\code ./func.sh mendel_reset
			./func.sh mendel_talk
			M115
			ctrl+d \endcode
*/

#define APP teacup

#include "config.h"

#include "serial.h"
#include "dda_queue.h"
#include "dda.h"
#include "gcode_parse.h"
#include "timer.h"
#include "temp.h"
#include "sermsg.h"
#include "watchdog.h"
#include "debug.h"
#include "sersendf.h"
#include "heater.h"
//#include "analog.h"
#include "pinio.h"
#include "arduino.h"
#include "clock.h"
//#include "intercom.h"
#include "simulator.h"

/// Startup code, run when we come out of reset
void init(void) {
	// set up watchdog
	wd_init();

	// set up serial
	serial_init();

	// set up G-code parsing
	gcode_init();

	// set up inputs and outputs
	mot_init();

	// set up timers
	timer_init();

	// read PID settings from EEPROM
	heater_init();

	// set up dda
	dda_init();

	// start up analog read interrupt loop,
	// if any of the temp sensors in your config.h use analog interface
	//analog_init();

	// set up temperature inputs
	temp_init();

	// enable interrupts
	//sei();

	// reset watchdog
	wd_reset();

  // prepare the power supply
  power_init();

	// say hi to host
	serial_writestr_P(PSTR("start\nok\n"));

}

static WORKING_AREA(tcWorkArea, 1024);

static msg_t tcThreadFn(void *arg){
	(void)arg;
	
	chRegSetThreadName("teacup");
	
	// main loop
	for(; !chThdShouldTerminate(); ){
		// if queue is full, no point in reading chars- host will just have to wait
		if((serial_rxchars() != 0) && (queue_full() == 0)){
			uint8_t c = serial_popchar();
			gcode_parse_char(c);
		}
		
    clock();
	}
	
	return 0;
}

static Thread *tcThread = NULL;

shellCommandDefine(teacup){
	(void)argc;
	(void)argv;
	
	if(tcThread){	
		return -1;
	}
		
  init();
  
  tcThread = chThdCreateStatic(tcWorkArea, sizeof(tcWorkArea), NORMALPRIO, tcThreadFn, NULL);
	
	return 0;
}

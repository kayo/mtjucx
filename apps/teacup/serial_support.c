#include "serial.h"

/// send a whole block
void serial_writeblock(void *data, int datalen)
{
        int i;

        for (i = 0; i < datalen; i++)
                serial_writechar(((uint8_t *) data)[i]);
}

/// send a string- look for null byte instead of expecting a length
void serial_writestr(uint8_t *data)
{
        uint8_t i = 0, r;
        // yes, this is *supposed* to be assignment rather than comparison, so we break when r is assigned zero
        while ((r = data[i++]))
                serial_writechar(r);
}

/**
        Write block from FLASH

        Extensions to output flash memory pointers. This prevents the data to
        become part of the .data segment instead of the .code segment. That means
        less memory is consumed for multi-character writes.

        For single character writes (i.e. '\n' instead of "\n"), using
        serial_writechar() directly is the better choice.
*/
void serial_writeblock_P(PGM_P data, int datalen)
{
        int i;

        for (i = 0; i < datalen; i++)
                serial_writechar(pgm_read_byte(&data[i]));
}

/// Write string from FLASH
void serial_writestr_P(PGM_P data)
{
        uint8_t r, i = 0;
        // yes, this is *supposed* to be assignment rather than comparison, so we break when r is assigned zero
        while ((r = pgm_read_byte(&data[i++])))
                serial_writechar(r);
}

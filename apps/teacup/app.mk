teacup = TEACUP

TEACUPINC = $(APPSDIR)/teacup
#TEACUPDIR = $(EXTDIR)/Teacup_Firmware
TEACUPDIR = $(TEACUPINC)

TEACUPSRC = \
  $(patsubst %,$(TEACUPDIR)/%.c,dda dda_lookahead dda_maths dda_queue gcode_parse gcode_process home debug clock sermsg crc timer) \
  $(patsubst %,$(TEACUPINC)/%.c,mendel pinio serial_support temp heater avr_emul sersendf)

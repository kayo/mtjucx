#ifndef _SERIAL_H
#define _SERIAL_H

#include "simulator.h"

#include "arduino.h"
#include <avr/pgmspace.h>

#define serial_init()
#define serial_rxchars() 1
#define serial_popchar() tty_getc()
#define serial_writechar(c) tty_putchar_b(c)

// read/write many characters
// uint8_t serial_recvblock(uint8_t *block, int blocksize);
void serial_writeblock(void *data, int datalen);

void serial_writestr(uint8_t *data);

// write from flash
void serial_writeblock_P(PGM_P data, int datalen);
void serial_writestr_P(PGM_P data);

#endif

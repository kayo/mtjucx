#ifndef _MEMORY_BARRIER_H_
#define _MEMORY_BARRIER_H_

#define MEMORY_BARRIER()
#define CLI_SEI_BUG_MEMORY_BARRIER()

#define ATOMIC_START
#define ATOMIC_END
//#define ATOMIC_START chSysLockFromIsr()
//#define ATOMIC_END chSysUnlockFromIsr()
//#define ATOMIC_START chSysLock()
//#define ATOMIC_END chSysUnlock()

#endif//_MEMORY_BARRIER_H_

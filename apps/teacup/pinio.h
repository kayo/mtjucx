#include "api.h"
#include "local.h"

void power_init(void);
void power_on(void);
void power_off(void);

#define DUMMY do{}while(0)

#define endstops_on() DUMMY
#define endstops_off() DUMMY

#define stepper_enable() DUMMY
#define stepper_disable() DUMMY

extern BaseObject* mot_x;
extern BaseObject* mot_y;
extern BaseObject* mot_z;
extern BaseObject* mot_e;

void mot_init(void);

#define x_enable() stpEnb(mot_x, 1)
#define x_disable() stpEnb(mot_x, 0)
#define x_direction(dir) stpDir(mot_x, dir)
#define x_step() stpStp(mot_x, 1)
#define x_min() stpMin(mot_x)
//#define x_max() 1

#define y_enable() stpEnb(mot_y, 1)
#define y_disable() stpEnb(mot_y, 0)
#define y_direction(dir) stpDir(mot_y, dir)
#define y_step() stpStp(mot_y, 1)
#define y_min() stpMin(mot_y)
//#define y_may() 1

#define z_enable() stpEnb(mot_z, 1)
#define z_disable() stpEnb(mot_z, 0)
#define z_direction(dir) stpDir(mot_z, dir)
#define z_step() stpStp(mot_z, 1)
#define z_min() stpMin(mot_z)
//#define z_maz() 1

#define e_enable() stpEnb(mot_e, 1)
#define e_disable() stpEnb(mot_e, 0)
#define e_direction(dir) stpDir(mot_e, dir)
#define e_step() stpStp(mot_e, 1)

#define unstep() ({ stpStp(mot_x, 0); stpStp(mot_y, 0); stpStp(mot_z, 0); stpStp(mot_e, 0); })

extern volatile uint8_t psu_timeout;

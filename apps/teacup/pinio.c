#include <stdint.h>

#include "pinio.h"

#include "config.h"

volatile uint8_t psu_timeout;

struct {
  BaseObject *obj;
  int atx_pson;
} ctrl;

void power_init(void){
  ctrl.obj = objectFind("ctl");
  ctrl.atx_pson = objectParamFind(ctrl.obj, "atx:power");
}

void power_on(void){
  unsigned st = 1;
  objectParamSet(ctrl.obj, ctrl.atx_pson, &st);
}

void power_off(void){
  unsigned st = 0;
  objectParamSet(ctrl.obj, ctrl.atx_pson, &st);
}

BaseObject* mot_x;
BaseObject* mot_y;
BaseObject* mot_z;
BaseObject* mot_e;

void mot_init(void){
  mot_x = objectFind("stp:1");
  mot_y = objectFind("stp:2");
  mot_z = objectFind("stp:3");
  mot_e = objectFind("stp:4");
}

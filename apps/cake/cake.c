#include "ch.h"
#include "hal.h"
#include "chprintf.h"

#include "api.h"
#include "shell.h"
#include "local.h"
#include "gcode.h"

typedef struct {
  BaseSequentialStream *sd;
} Cake;

static msg_t mThread(void *arg){
  Cake *m = arg;
	
	GCodeObject gco;
	GCodeEntry gce;
  
	gcodeObjectInit(&gco);
	
  chRegSetThreadName("cake");
  
  chprintf(m->sd, "start\nok\n");
	
	for(; !chThdShouldTerminate(); ){
		gcodeStartParse(&gco, &gce);
		
		switch(gcodeParseStream(&gco, m->sd)){
		case gcodeMissing:
			chThdSleepMilliseconds(500);
			break;
		case gcodeTransferError:
			chprintf(m->sd, "rs N%d\r\n", gce.lnum);
			break;
		case gcodeSuccess:
			gcodeEntryPrint(m->sd, &gce);
			break;
		default:
			break;
		}
	}
  
  chHeapFree(m);

  return 0;
}

shellCommandDefine(cake){
  (void)argc;
  (void)argv;
  
  Cake *m = chHeapAlloc(NULL, sizeof(Cake));
  
  m->sd = (BaseSequentialStream*)&SDU2;
  
  chThdCreateFromHeap(NULL, THD_WA_SIZE(1024), NORMALPRIO, mThread, m);
  
  return 0;
}
